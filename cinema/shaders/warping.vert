#version 330
layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 normalPosition;
layout(location = 2) in vec2 uvTexturePosition;

//out vec2 texCoord;
out vec3 vVertex, vReflect;
//out vec2 texCoord;
uniform mat4 projection_matrix, view_matrix, model_matrix, normal_matrix;




void main()
  {
	//Calculate Vertex Position on Screen

	gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vertexPosition, 1.0);
//	gl_Position.x /= 1.77;
	vVertex = vertexPosition.xyz;
//	vec3 incident_vector = (view_matrix * model_matrix * vec4(vertexPosition, 1.0)).xyz;
	vec3 incident_vector = (model_matrix * vec4(vertexPosition, 1.0)).xyz;
    vReflect = reflect(normalize(incident_vector), normalize((normal_matrix * vec4(normalPosition, 1.0)).xyz));
//    vReflect = incident_vector;
//	texCoord = uvTexturePosition;
  }