#version 330
#extension GL_NV_shadow_samplers_cube : enable


uniform sampler2D TextureMap;
uniform samplerCube CubeMap;

//in vec2 texCoord;
in vec3 vVertex, vReflect;
in vec2 texCoord;
out vec4 color;



void main()
{
//    color.rgb = textureCube(CubeMap, texCoord).rgb;
    color.rgb = textureCube(CubeMap, vReflect).rgb;
//    color.rgb = textureCube(CubeMap, vVertex).rgb;
    color.a = 1.0;

}