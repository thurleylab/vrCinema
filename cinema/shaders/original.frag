#version 330

uniform int sides;
uniform float phi;
uniform sampler2D TextureMap;

in vec2 texCoord;
out vec4 color;

const float pi = 3.14159265;

//const float phi = i * 2 * pi / sides;

void main()
{
    /**************************************/
    /****CALCULATIONS: REVERSE ORDER!!!****/
    /**************************************/

    vec2 warp_coord = texCoord - 0.5;  // moving to center
    warp_coord /= 0.5;  // scaling (x&y)
    vec2 warp_coord_temp = warp_coord;
    warp_coord_temp.s *= 1.77;

    warp_coord.s = warp_coord_temp.s * cos(phi) - warp_coord_temp.t * sin(phi); // rotation
    warp_coord.t = warp_coord_temp.t * cos(phi) + warp_coord_temp.s * sin(phi); // rotation
    warp_coord.t -= 0.5;  // shift along y-axes
    warp_coord.s = ((warp_coord.s) / tan(pi/sides)) / (1 + warp_coord.t * 2);  // warping (x-size, angle)

    warp_coord = warp_coord + 0.5;  // reverting 'moving to center' line

    // only apply a color to the pixel if warp_coord is inside the texture (0 <= warp_coord <= 1)
    if ( (warp_coord.s >= 0.0  && warp_coord.s <= 1.0) && (warp_coord.t >= 0.0 && warp_coord.t <= 1.0) ) {
    	color.rgb = texture2D(TextureMap, warp_coord).rgb;
    	color.a = 1.;
    } else {
        discard;
    }
}