#version 330
#extension GL_NV_shadow_samplers_cube : enable

uniform sampler2D TextureMap;
in vec2 texCoord;
out vec4 final_color;

void main()
{
    vec3 texture_coeff = texture2D(TextureMap, texCoord).rgb;
    final_color = vec4(texture_coeff, 1.0);
}