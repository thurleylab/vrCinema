import serial

from serial.tools.list_ports import comports


class ArduinoDevice(object):
    """
    NOTE for LINUX users! Include user in the 'dialout' group and restart to
    access USB devices.
    """

    def __init__(self, port=None, baud=9600):
        self.device = serial.Serial(port, baud, timeout=.1)

    @staticmethod
    def serial_ports():
        """
        Lists available serial ports

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
        """
        return comports()

    def read(self):
        return self.device.readline()

    def write(self, data):
        return self.device.write(data)

    def close(self):
        self.device.close()
