import os
import sys

from ..utils import EventBusClient


class ProPixxController(object):

    def __init__(self, verbose=True):
        self.event_bus = EventBusClient(events=["turn_projection_on_off", "shutdown"])
        self.event_bus.register("turn_projection_on_off", self.turn_projection_on_off)
        self.event_bus.register("shutdown", self.on_shutdown)

        self.keep_alive = True

        # FIXME patch to let propixx work for python 3.5
        sys.path.append('c:\\users\\sirotalab\\miniconda3\\envs\\py35\\lib\\site-packages\\pypixxlib')
        from pypixxlib.propixx import PROPixx
        
        self.propixx_device = PROPixx()

        if verbose: 
            print("OK (%s): Propixx (projector)" % os.getpid())

        self.run()

    def run(self):
        while self.keep_alive:
            self.event_bus.check_messages()

    def turn_projection_on_off(self, msg_data):
        new_led_state = bool(msg_data[0])
        self.propixx_device.setLampLED(new_led_state)

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: Propixx (projector)")

        self.keep_alive = False
        self.propixx_device.setLampLED(True)
