import os
import time
import numpy as np
# import matplotlib.pyplot as plt
# import matplotlib.animation as animation

import asyncio
import datetime
import random
import websockets

from ..utils import EventBusClient


class InfoWindowController(object):

    def __init__(self, config):
        self.keep_alive = True
        self.event_bus = EventBusClient(events=["shutdown", "feed", "position_subject"])
        self.event_bus.register("shutdown", self.on_shutdown)
        self.event_bus.register("feed", self.on_hotspot)
        self.event_bus.register("position_subject", self.subject_pos_update)

        self.start_time = time.time()
        self.visited_hotspots = 0
        self.subject_position = [0, 0, 0]
        self.subject_rotation = [0, 0, 0]
        self.is_moved = False

        start_server = websockets.serve(self.time, "127.0.0.1", 5678)

        print("OK (%s): info window" % os.getpid())

        asyncio.get_event_loop().run_until_complete(start_server)
        asyncio.get_event_loop().run_forever()

    async def time(self, websocket, path):
        while self.keep_alive:
            self.event_bus.check_messages()

            if self.is_moved:
                x = self.subject_position[0]
                y = self.subject_position[2]

                await websocket.send("%s, %s" % (x, y))

                self.is_moved = False

        asyncio.get_event_loop().stop()

    def on_hotspot(self, msg_data):
        self.visited_hotspots += 1

    def subject_pos_update(self, msg_data):
        pos = self.subject_position
        delta = np.sqrt( (pos[0] - msg_data[1])**2 + (pos[2] - msg_data[3])**2 )

        if delta > 10**-2:
            self.subject_position = [msg_data[1], msg_data[2], msg_data[3]]
            self.subject_rotation = [msg_data[4], msg_data[5], msg_data[6]]

            self.is_moved = True

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: info window")
        self.keep_alive = False

#
#
# class TrajectoryWindowController(object):
#
#     def __init__(self, config):
#         """
#         Creates a window with position data (uses matplotlib)
#
#         :param x_virt:          shared memory Double virtual X coordinate
#         :param y_virt:          shared memory Double virtual Y coordinate
#         :param config:          window configuration dict
#         """
#         def data_gen():
#             while self.keep_alive:
#                 yield self.subject_position[0], -self.subject_position[2]
#
#             else:
#                 self.ani.event_source.stop()
#                 plt.close(fig)
#
#         self.keep_alive = True
#         self.event_bus = EventBusClient(events=["shutdown", "feed", "position_subject"])
#         self.event_bus.register("shutdown", self.on_shutdown)
#         self.event_bus.register("feed", self.on_hotspot)
#         self.event_bus.register("position_subject", self.subject_pos_update)
#         self.start_time = time.time()
#         self.visited_hotspots = 0
#         self.subject_position = [0, 0, 0]
#         self.subject_rotation = [0, 0, 0]
#
#         fig, ax = plt.subplots(figsize=(config["size"]["x"], config["size"]["y"]))
#         fig.tight_layout(pad=0.5)
#
#         line, = ax.plot([], [], '.', alpha=0.4)
#
#         ax.set_ylim(config["ylim"][0], config["ylim"][1])
#         ax.set_xlim(config["xlim"][0], config["xlim"][1])
#         ax.grid()
#
#         xdata, ydata = [0.], [0.]
#
#         def run(data):
#             self.event_bus.check_messages()
#
#             timedelta = time.time() - self.start_time
#             title = "%s Rewards: %d" % (time.strftime("%H:%M:%S", time.gmtime(timedelta)), self.visited_hotspots)
#             fig.canvas.set_window_title(title)
#
#             x, y = data
#
#             if np.abs(xdata[-1] - x) > 10**(-3) or np.abs(ydata[-1] - y) > 10**(-3):
#                 xdata.append(x)
#                 ydata.append(y)
#                 line.set_data(xdata, ydata)
#
#             return line,
#
#         location = config["location"]
#         plt.get_current_fig_manager().window.wm_geometry("+%s+%s" % (str(location["x"]), str(location["y"])))
#
#         self.ani = animation.FuncAnimation(fig, run, data_gen, blit=True, interval=50, repeat=False)
#         print("OK (%s): trajectory window" % os.getpid())
#
#         plt.show()
#
#     def on_shutdown(self, msg_data):
#         print("SHUTDOWN: trajectory window")
#         self.keep_alive = False
#
#     def on_hotspot(self, msg_data):
#         self.visited_hotspots += 1
#
#     def subject_pos_update(self, msg_data):
#         self.subject_position = [msg_data[1], msg_data[2], msg_data[3]]
#         self.subject_rotation = [msg_data[4], msg_data[5], msg_data[6]]
#
#
# class RatCaveTrajectoryController(object):
#
#     def __init__(self, config):
#         """
#         Creates a window with position data (uses matplotlib)
#
#         :param config:          window configuration dict
#         """
#         # def data_gen():
#         #     while self.keep_alive:
#         #         yield self.subject_position[0], -self.subject_position[2]
#         #
#         #     else:
#         #         self.ani.event_source.stop()
#         #         plt.close(fig)
#
#         self.keep_alive = True
#         self.event_bus = EventBusClient(events=["shutdown", "feed", "position_subject"])
#         self.event_bus.register("shutdown", self.on_shutdown)
#         self.event_bus.register("feed", self.on_hotspot)
#         self.event_bus.register("position_subject", self.subject_pos_update)
#
#         self.start_time = time.time()
#         self.visited_hotspots = 0
#         self.subject_position = [0, 0, 0]
#         self.subject_rotation = [0, 0, 0]
#
#         self.fig, self.ax = plt.subplots(1, 1)
#         #self.fig, self.ax = plt.subplots(figsize=(config["size"]["x"], config["size"]["y"]))
#         #self.fig.tight_layout(pad=0.5)
#
#         #line, = self.ax.plot([], [], '.', alpha=0.4)
#
#         self.ax.set_xlim(config["xlim"][0], config["xlim"][1])
#         self.ax.set_ylim(config["ylim"][0], config["ylim"][1])
#         #self.ax.grid()
#
#         plt.show(False)
#         plt.draw()
#
#         self.background = self.fig.canvas.copy_from_bbox(self.ax.bbox)
#         self.points = self.ax.scatter(0, 0, 'o', s=10)[0]
#         self.fig.canvas.draw()
#         self.last_x, self.last_y = 0, 0
#
#         print("OK (%s): trajectory window" % os.getpid())
#
#         self.run()
#
#         #
#         # def run(data):
#         #     self.event_bus.check_messages()
#         #
#         #     timedelta = time.time() - self.start_time
#         #     title = "%s Rewards: %d" % (time.strftime("%H:%M:%S", time.gmtime(timedelta)), self.visited_hotspots)
#         #     fig.canvas.set_window_title(title)
#         #
#         #     x, y = data
#         #
#         #     if np.abs(xdata[-1] - x) > 10**(-3) or np.abs(ydata[-1] - y) > 10**(-3):
#         #         xdata.append(x)
#         #         ydata.append(y)
#         #         line.set_data(xdata, ydata)
#         #
#         #     return line,
#         #
#         # location = config["location"]
#         # plt.get_current_fig_manager().window.wm_geometry("+%s+%s" % (str(location["x"]), str(location["y"])))
#         #
#         # self.ani = animation.FuncAnimation(fig, run, data_gen, blit=True, interval=50, repeat=False)
#         # print("OK (%s): trajectory window" % os.getpid())
#         #
#         # plt.show()
#
#     def run(self):
#         while self.keep_alive:
#             self.event_bus.check_messages()
#
#             timedelta = time.time() - self.start_time
#             title = "%s Rewards: %d" % (time.strftime("%H:%M:%S", time.gmtime(timedelta)), self.visited_hotspots)
#             #self.fig.canvas.set_window_title(title)
#
#             # update the xy data
#             #x, y = float(self.subject_position[0]), -float(self.subject_position[2])
#             x, y = np.random.rand(), np.random.rand()
#             # if np.abs(self.last_x - x) > 10 ** (-3) or np.abs(self.last_y - y) > 10 ** (-3):
#             self.last_x, self.last_y = x, y
#             self.points.set_data(x, y)
#
#             # restore background
#             self.fig.canvas.restore_region(self.background)
#
#             # redraw just the points
#             self.ax.draw_artist(self.points)
#
#             # fill in the axes rectangle
#             self.fig.canvas.blit(self.ax.bbox)
#
#         plt.close(self.fig)
#
#     def on_shutdown(self, msg_data):
#         print("SHUTDOWN: trajectory window")
#         self.keep_alive = False
#
#     def on_hotspot(self, msg_data):
#         self.visited_hotspots += 1
#
#     def subject_pos_update(self, msg_data):
#         self.subject_position = [msg_data[1], msg_data[2], msg_data[3]]
#         self.subject_rotation = [msg_data[4], msg_data[5], msg_data[6]]
