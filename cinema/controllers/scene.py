import os
import numpy as np

import pyglet
from ..utils import EventBusClient


_ROOT = os.path.abspath(os.path.dirname(__file__))


class SceneController(object):

    def __init__(self, scene_builder, scene_cfg, screen_cfg):
        self.event_bus = EventBusClient(events=["move", "shutdown"])
        self.event_bus.register("move", self.on_move)
        self.event_bus.register("move", self.check_hotspot)
        self.event_bus.register("shutdown", self.on_shutdown)

        self.scene = scene_builder(scene_cfg)
        self.scene.yaw = scene_cfg["yaw"]

        display = pyglet.window.get_platform().get_default_display()
        screens = display.get_screens()
        self.window = pyglet.window.Window(
            fullscreen=screen_cfg["fullscreen"],
            screen=screens[screen_cfg["screen_index"]]
        )

        if not screen_cfg["fullscreen"]:
            self.window.set_size(screen_cfg["width"], screen_cfg["height"])
            self.window.set_location(screen_cfg["location"]["x"], screen_cfg["location"]["y"])

        # events
        self.window.event(self.on_close)
        self.window.event(self.on_key_press)
        self.window.event(self.on_draw)
        self.fps_label = pyglet.window.FPSDisplay(self.window)

        pyglet.clock.schedule(self.check_messages)

        print("OK (%s): scene controller" % str(os.getpid()))

        self.run()

    def run(self):
        pyglet.app.run()

    """
    @property
    def yaw(self):
        return (self.scene.meshes.rotation.z * np.pi) / 180.0
    """
    def to_real(self, x0, y0, yaw):
        x = x0 * np.cos(-yaw) - y0 * np.sin(-yaw)
        y = x0 * np.sin(-yaw) + y0 * np.cos(-yaw)
        return x, y

    def check_messages(self, dt):
        self.event_bus.check_messages()

    def check_hotspot(self, msg_data):
        timestamp, x, y, rot, delta = msg_data

        for spot in self.scene.hotspots:
            #x1, y1 = self.to_real(x, y)
            #if spot.is_inside(x1, y1) and spot.available():
            if spot.is_inside(x, y) and spot.available():
                for sp1 in self.scene.hotspots:
                    sp1.check_in()

                self.event_bus.send({"hotspot": (timestamp, spot.sid, x, y)})

    def on_move(self, msg_data):
        timestamp, x, y, yaw, delta = msg_data
        pos_delta = np.array([delta[0], delta[1], 0.])

        distal_cues = []
        if hasattr(self.scene, 'distal_cues'):
            distal_cues = self.scene.distal_cues

        self.scene.camera.position.xyz = (x, y, self.scene.camera.position.z)
        self.scene.yaw = yaw
        #self.scene.meshes.rotation.xyz = (0., 0., rot*100)
        #self.scene.meshes.update()

        for mesh in distal_cues + [self.scene.light]:
            mesh.position.xyz = np.array(mesh.position.xyz) + pos_delta

    def on_shutdown(self, msg_data):
        pyglet.clock.schedule_once(self.do_safe_close, 0.1)

    def on_draw(self):
        self.scene.draw_360()
        self.fps_label.draw()

    def on_key_press(self, symbol, modifiers):
        self.event_bus.send({"key_press": [symbol]})

    def on_close(self):
        print("SHUTDOWN: scene controller")
        self.event_bus.send({"shutdown": []})

    def do_safe_close(self, dt):
        self.window.close()


class RatcaveSceneController(object):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, *args, **kwargs):
        # FIXME issue with pyglet run as a separate process
        # https://github.com/los-cocos/cocos/issues/281
        import pyglet.gl as gl
        import ratcave as rc

        self.event_bus = EventBusClient(events=["position_subject", "position_arena", "shutdown"])
        self.event_bus.register("shutdown", self.on_shutdown)
        self.event_bus.register("position_subject", self.subject_pos_update)
        self.event_bus.register("position_arena", self.arena_pos_update)

        self.subject_position = [0, 0, 0]
        self.subject_rotation = [0, 0, 0]
        self.arena_body_position = [0, 0, 0]
        self.arena_body_quaternion = [0, 0, 0, 0]

        # Load projector's Camera object, created from the calib_projector ratcave_utils CLI tool.
        config = gl.Config(double_buffer=True, stereo=True)
        display = pyglet.window.get_platform().get_default_display()
        screen = display.get_screens()[screen_cfg['screen_index']]
        #self.window = pyglet.window.Window(fullscreen=True, screen=screen, vsync=False, config=config)
        self.window = pyglet.window.Window(fullscreen=True, screen=screen)

        self.fbo = rc.FBO(rc.TextureCube(width=4096, height=4096))
        self.shader3d = rc.resources.cube_shader

        # setting up the projection scene (arena)
        arena = rc.WavefrontReader(scene_cfg['arena_filename'].encode()).get_mesh('Arena')
        arena.rotation = arena.rotation.to_quaternion()
        arena.textures.append(self.fbo.texture)
        self.arena = arena

        camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=40.974, aspect=1.777778, z_near=.005, z_far=20.))
        camera.position.xyz = screen_cfg['projector_position']
        camera.rotation.xyz = screen_cfg['projector_rotation']

        scene = rc.Scene(meshes=[self.arena], bgColor=(0., 0., 0.))
        scene.gl_states.states = scene.gl_states.states[:-1]
        scene.camera = camera
        scene.light.position.xyz = camera.position.xyz
        self.scene = scene

        # building VR scene
        vr_scene = scene_builder(scene_cfg)
        vr_scene.light.position.xyz = self.scene.camera.position.xyz
        vr_scene.light.position.y -= 2.
        self.vr_scene = vr_scene

        for msh in self.vr_scene.meshes.children:
            msh.original_position = msh.position.xyz

        # events
        self.window.event(self.on_close)
        self.window.event(self.on_draw)
        self.window.event(self.on_key_press)
        self.fps_label = pyglet.window.FPSDisplay(self.window)

        pyglet.clock.schedule(self.update)

        print("OK (%s): scene controller" % str(os.getpid()))

        self.run()

    def run(self):
        pyglet.app.run()

    # daemons

    def check_messages(self):
        self.event_bus.check_messages()

    def update(self, dt):
        self.check_messages()

        self.arena.position.xyz = self.arena_body_position
        self.arena.rotation.xyzw = self.arena_body_quaternion

        self.vr_scene.root.position.xyz = self.arena.position.xyz
        self.vr_scene.camera.position.xyz = self.subject_position
        self.scene.camera.uniforms['playerPos'] = self.subject_position

    def arena_pos_update(self, msg_data):
        self.arena_body_position = [msg_data[1], msg_data[2], msg_data[3]]
        self.arena_body_quaternion = [msg_data[4], msg_data[5], msg_data[6], msg_data[7]]

    def subject_pos_update(self, msg_data):
        self.subject_position = [msg_data[1], msg_data[2], msg_data[3]]
        self.subject_rotation = [msg_data[4], msg_data[5], msg_data[6]]

    # events
    def on_key_press(self, symbol, modifiers):
        self.event_bus.send({"key_press": [symbol]})

    def on_shutdown(self, msg_data):
        if hasattr(self, 'timers'):
            for t in self.timers:
                t.cancel()
                print('TIMER: %s cancelled' % str(t))

        pyglet.clock.schedule_once(self.do_safe_close, 0.1)

    def on_close(self):
        print("SHUTDOWN: scene controller")
        self.event_bus.send({"shutdown": []})
        self.on_shutdown("")

    def on_draw(self):
        with self.shader3d:
            with self.fbo:
                self.window.clear()
                self.vr_scene.camera.projection.match_aspect_to_viewport()
                self.vr_scene.draw360_to_texture(self.fbo.texture)

            self.scene.camera.projection.match_aspect_to_viewport()
            self.scene.draw()

    # actions

    def optitrack_to_VR(self, x, y, z):
        # position from OptiTrack coordinates to VR coordinates
        phi = np.pi * 4.3 / 180.0

        # TODO do infer translation / rotation from the arena file and do matrix multiplication
        x1 = x * np.cos(phi) - z * np.sin(phi)
        z1 = x * np.sin(phi) + z * np.cos(phi)
        y1 = y

        return x1, y1, z1  # subject position in VR coordinates

    def VR_to_optitrack(self, x, y, z):
        # position from VR coordinates to OptiTrack coordinates
        phi = np.pi * 4.3 / 180.0

        x1 = x * np.cos(-phi) - z * np.sin(-phi)
        z1 = x * np.sin(-phi) + z * np.cos(-phi)
        y1 = y

        return x1, y1, z1  # subject position in OptiTrack coordinates

    def cut_to_boundaries(self, x, y, z):
        # limit by the boundaries
        # TODO do infer boundaries from the arena file
        b_x = [-0.37, 0.22]
        b_y = [-10.0, 10.0]
        b_z = [-0.59, 1.02]

        def limit_to_boundary(value, lower_lim, upper_lim):
            if value < lower_lim:
                return lower_lim
            if value > upper_lim:
                return upper_lim
            return value

        x1 = limit_to_boundary(x, b_x[0], b_x[1])
        y1 = limit_to_boundary(y, b_y[0], b_y[1])
        z1 = limit_to_boundary(z, b_z[0], b_z[1])

        return x1, y1, z1

    def do_safe_close(self, dt):
        self.window.close()
