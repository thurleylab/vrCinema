import time
import os
from ..utils import EventBusClient
from ..idisk import DataWriter


class LoggingController(object):

    update_frequency = 100  # Hz

    def __init__(self, where, sources_path, exp_base, settings_path, writer_cls=DataWriter):
        self.event_bus = EventBusClient(events=["move", "collision", "hotspot", "ext_data", "shutdown"])
        self.event_bus.register("move", self.write_position)
        self.event_bus.register("collision", self.write_collision)
        self.event_bus.register("hotspot", self.write_hotspot)
        self.event_bus.register("ext_data", self.write_external)
        self.event_bus.register("shutdown", self.on_shutdown)

        self.keep_alive = True

        self.ext_data_collector = []
        self.current_second = int(divmod(time.time(), 1)[0])

        self.writer = writer_cls(where, {})

        # dump config + maze obj/mtl files
        for filename in os.listdir(sources_path):
            if filename.endswith("%s.obj" % exp_base) or filename.endswith("%s.mtl" % exp_base):
                self.writer.dump_file(os.path.join(sources_path, filename))

        self.writer.dump_file(settings_path)

        print("OK (%s): logging controller" % str(os.getpid()))

        self.run()

    def run(self):
        timestamp = time.time()

        while self.keep_alive:
            self.event_bus.check_messages()  # listening on events

            time_since_last = time.time() - timestamp
            if (1. / LoggingController.update_frequency) > time_since_last:
                time.sleep((1. / LoggingController.update_frequency) - time_since_last)
            timestamp = time.time()

    def write_position(self, msg_data):
        timestamp, x, y, rot, delta = msg_data
        self.writer.log("position", timestamp, x, y, 0.0, rot)

    def write_collision(self, msg_data):
        timestamp, x, y, delta = msg_data
        self.writer.log("collision", timestamp, x, y, 0.0)

    def write_hotspot(self, msg_data):
        timestamp, spot_id, x, y = msg_data
        self.writer.log("hotspot", timestamp, spot_id, x, y, 0.0)

    def write_external(self, msg_data):
        timestamp, value = msg_data
        second, partial = divmod(timestamp, 1)

        if int(second) == self.current_second:
            self.ext_data_collector.append("%.6f:%.2f" % (partial, value))

        else:
            self.writer.log("external", self.current_second, " ".join(self.ext_data_collector))

            self.current_second = int(second)
            self.ext_data_collector = []

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: logging controller")
        self.keep_alive = False


class RatcaveLoggingController(LoggingController):

    def run(self):
        self.event_bus.add_event("position_subject")
        self.event_bus.register("position_subject", self.log_position)
        self.event_bus.add_event("position_arena")
        self.event_bus.register("position_arena", self.log_arena_position)
        self.event_bus.add_event("feed")
        self.event_bus.register("feed", self.log_reward)
        self.event_bus.add_event("vr_event")
        self.event_bus.register("vr_event", self.log_vr_event)

        super(RatcaveLoggingController, self).run()

    def log_position(self, msg_data):
        timestamp, x, y, z, xr, yr, zr = msg_data
        self.writer.log("position", timestamp, x, y, z, xr, yr, zr)

    def log_arena_position(self, msg_data):
        timestamp, x, y, z, xr, yr, zr, w = msg_data
        self.writer.log("arena", timestamp, x, y, z, xr, yr, zr, w)

    def log_reward(self, msg_data):
        timestamp, spot_id, x, y = msg_data
        self.writer.log("reward", timestamp, spot_id, x, y)

    def log_vr_event(self, msg_data):
        self.writer.log("vr_event", *msg_data)
