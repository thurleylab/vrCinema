import time
import os

from ..utils import EventBusClient
from ..iserial import ArduinoDevice


class SerialController(object):

    def __init__(self, port, baud, syringe, displacement, delay, verbose=False):
        self.event_bus = EventBusClient(events=["hotspot", "shutdown"])
        self.event_bus.register("hotspot", self.give_reward)
        self.event_bus.register("shutdown", self.on_shutdown)

        self.keep_alive = True
        self.port = port
        self.baud = baud
        self.syringe = syringe
        self.displacement = displacement
        self.delay = delay

        self._last_ext_value = None
        self.is_port_at_origin = True
        self.port_return_time = None

        if verbose: print("OK (%s): arduino at %s:%s" % (os.getpid(), port, baud))

        self.run()

    def run(self):
        while self.keep_alive or not self.is_port_at_origin:
            self.event_bus.check_messages()

            if self.port_return_time and self.port_return_time < time.time():
                self.return_holder()
                self.is_port_at_origin = True
                self.port_return_time = None

            try:
                value = float(self.device.read())  # incoming data from device
                if not self._last_ext_value == value:
                    self._last_ext_value = value
                    self.event_bus.send({"ext_data": (time.time(), self._last_ext_value)})

            except (ValueError, TypeError):
                pass

    def give_reward(self, msg_data):
        if self.is_port_at_origin:
            command = b"1:%d&2:%d" % (self.syringe, self.displacement)
        else:
            command = b"1:%d" % self.syringe

        self.device.write(command)

        self.is_port_at_origin = False
        self.port_return_time = time.time() + self.delay

    def return_holder(self):
        self.device.write(b'2:%d' % (-self.displacement))

    def on_external(self, *args):
        # e.g. do smth about arduino feedback
        pass

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: arduino at %s:%s" % (self.port, self.baud))
        self.keep_alive = False


class ArenaActuatorController(object):

    def __init__(self, port, baud, verbose=False):
        self.event_bus = EventBusClient(events=["arena_move", "key_press", "shutdown"])
        self.event_bus.register("arena_move", self.on_arena_move)
        self.event_bus.register("key_press", self.on_key_press)
        self.event_bus.register("shutdown", self.on_shutdown)

        self.keep_alive = True
        self.port = port
        self.baud = baud

        self.device = ArduinoDevice(port, baud or 9600)

        if verbose: print("OK (%s): Actuators (serial) at %s:%s" % (os.getpid(), port, baud))

        self.run()

    def run(self):
        #self.device.write("B".encode())  # move arena back to the start if not there

        while self.keep_alive:
            self.event_bus.check_messages()

    def on_arena_move(self, msg_data):
        self.device.write(msg_data[0].encode())

    def on_key_press(self, msg_data):
        if msg_data[0] == 102:  # forward move, key 'F'
            self.device.write('f'.encode())

        if msg_data[0] == 98:   # backward move, key 'B'
            self.device.write('b'.encode())

    def on_external(self, *args):
        # e.g. do smth about arduino feedback
        pass

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: actuators (serial) at %s:%s" % (self.port, self.baud))

        self.device.write("M".encode())  # move arena back to the center
        self.device.close()
        self.keep_alive = False


class FeederController(object):

    def __init__(self, port, baud, verbose=False):
        self.event_bus = EventBusClient(events=["feed", "shutdown"])
        self.event_bus.register("feed", self.feed)
        self.event_bus.register("shutdown", self.on_shutdown)

        self.keep_alive = True
        self.port = port
        self.baud = baud

        self.device = ArduinoDevice(port, baud or 9600)

        if verbose: print("OK (%s): Feeder (serial) at %s:%s" % (os.getpid(), port, baud))

        self.run()

    def run(self):
        while self.keep_alive:
            self.event_bus.check_messages()

    def feed(self, msg_data):
        self.device.write('f'.encode())

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: Feeder (serial) at %s:%s" % (self.port, self.baud))

        self.device.close()
        self.keep_alive = False