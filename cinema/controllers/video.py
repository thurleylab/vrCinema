import cv2
import os
import time
from ..utils import EventBusClient


class VideoController(object):

    def __init__(self, config):
        self.event_bus = EventBusClient(events=["shutdown"])
        self.event_bus.register("shutdown", self.on_shutdown)

        self.freq = config["record"]["fps"]
        self.keep_alive = True
        self.config = config

        self.cam = cv2.VideoCapture(config["device_id"])

        self.cam.set(3, config["resolution"]["x"])
        self.cam.set(4, config["resolution"]["y"])

        self.window = cv2.namedWindow("tracking")
        cv2.moveWindow("tracking", config["location"]["x"], config["location"]["y"])

        record = config["record"]

        if record["enabled"]:
            ret, img = self.cam.read()

            #codec = record["codec"].encode('ascii', 'ignore')
            fourcc = cv2.VideoWriter_fourcc(*record["codec"])
            where = os.path.join(record["where"], "tracking.avi")
            self.out = cv2.VideoWriter(where, fourcc, record["fps"], (img.shape[1], img.shape[0]))

        print("OK (%s): video controller, device %s" % (os.getpid(), str(config["device_id"])))

        self.run()

    def run(self):
        timestamp = time.time()

        while self.keep_alive:
            self.event_bus.check_messages()

            ret_val, img = self.cam.read()
            if self.config["mirror"]:
                img = cv2.flip(img, 1)

            if self.config["record"]["enabled"]:
                self.out.write(img)
            cv2.imshow("tracking", img)

            if cv2.waitKey(1) == 27:
                self.event_bus.send({"shutdown": []})

            # timer to keep required frequency
            time_since_last = time.time() - timestamp
            if (1. / self.freq) > time_since_last:
                time.sleep((1. / self.freq) - time_since_last)
            timestamp = time.time()

        if self.config["record"]["enabled"]:
            self.out.release()
            print("Video file released")

        self.cam.release()
        print("Camera released")
        cv2.destroyAllWindows()
        print("Destroying all windows now..")

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: video controller")
        self.keep_alive = False