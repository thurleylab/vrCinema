import os
import time
import pyglet
from ..utils import EventBusClient

_ROOT = os.path.abspath(os.path.dirname(__file__))
sounds_path = os.path.join(_ROOT, '..', 'sounds')
click_sound = os.path.join(sounds_path, 'click.wav')
umgawa_sound = os.path.join(sounds_path, 'umgawa.wav')


class SoundController(object):

    update_frequency = 50  # Hz

    def __init__(self, path, play_count=4, delay=0.3):
        self.event_bus = EventBusClient(events=["hotspot", "shutdown"])
        self.event_bus.register("hotspot", self.on_hotspot)
        self.event_bus.register("shutdown", self.on_shutdown)

        self.keep_alive = True

        filename = path
        if not path:
            filename = click_sound

        self.sound = pyglet.media.load(filename, streaming=False)
        self.play_count = play_count
        self.delay = delay

        self.counter = 0

        print("OK (%s): sound controller" % os.getpid())

        self.run()

    def run(self):
        timestamp = time.time()

        while self.keep_alive:
            self.event_bus.check_messages()

            time_since_last = time.time() - timestamp
            if (1. / SoundController.update_frequency) > time_since_last:
                time.sleep((1. / SoundController.update_frequency) - time_since_last)
            timestamp = time.time()

    def on_hotspot(self, *args):
        self.counter = self.play_count

        self.play()

    def play(self, dt=0):
        while self.counter > 0:
            self.sound.play()
            self.counter -= 1

            # this does not work somehow
            #self.clock.schedule_once(self.on_hotspot, self.delay)

            time.sleep(self.delay)

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: sound controller")
        self.keep_alive = False


class DoubleSoundController(object):

    update_frequency = 50  # Hz

    def __init__(self, path_sound_file_hotspot=click_sound, path_sound_file_feed=umgawa_sound):
        self.event_bus = EventBusClient(events=["hotspot", "feed", "shutdown"])
        self.event_bus.register("hotspot", self.on_hotspot)
        self.event_bus.register("feed", self.on_feed)
        self.event_bus.register("shutdown", self.on_shutdown)

        self.keep_alive = True

        self.sound_hotspot = pyglet.media.load(path_sound_file_hotspot, streaming=False)
        self.sound_feed = pyglet.media.load(path_sound_file_feed, streaming=False)

        print("OK (%s): sound controller" % os.getpid())

        self.run()

    def run(self):
        timestamp = time.time()

        while self.keep_alive:
            self.event_bus.check_messages()

            time_since_last = time.time() - timestamp
            if (1. / SoundController.update_frequency) > time_since_last:
                time.sleep((1. / SoundController.update_frequency) - time_since_last)
            timestamp = time.time()

    def on_hotspot(self, *args):
        self.sound_hotspot.play()

    def on_feed(self, *args):
        self.sound_feed.play()

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: sound controller")
        self.keep_alive = False