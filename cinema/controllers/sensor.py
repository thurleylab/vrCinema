import usb
import time
import os
from sensors.mouse import Mouse
from ..utils import EventBusClient


class SensorController(object):

    def __init__(self, dev_kwargs, frequency, speed, x, y, verbose=False, invert=False):
        self.event_bus = EventBusClient(events=["shutdown"])
        self.event_bus.register("shutdown", self.on_shutdown)

        self.dev_kwargs = dev_kwargs
        self.keep_alive = True

        self.sensor = Mouse.list_connected(**dev_kwargs)[0]

        try:
            self.sensor.detach()
        except usb.core.USBError:  # on Linux may not have sufficient permissions
            raise IOError("You are probably on Linux. Ensure you"
                          "have rights enabled to manage USB devices"
                          "or create appropriate udev rules")

        self.x = x
        self.y = y
        self.delay = 1. / frequency
        self.koeff = -1.0 if invert else 1.0
        self.speed = speed

        print("OK (%s): sensor %s" % (os.getpid(), str(self.dev_kwargs)))

        self.run()

    def run(self):
        while self.keep_alive:
            self.event_bus.check_messages()

            x1, y1 = self.sensor.get_position_change()
            self.x.value += self.koeff * x1 * self.speed
            self.y.value += self.koeff * y1 * self.speed

            time.sleep(self.delay)

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: sensor %s" % str(self.dev_kwargs))
        self.keep_alive = False