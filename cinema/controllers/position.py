import time
import os
import numpy as np
import datetime

from ..utils import EventBusClient


class PositionController(object):

    update_frequency = 100  # Hz

    def __init__(self, dx1, dy1, dx2, dy2, x_virt, y_virt, yaw_virt, gain, step_size, yaw, border, boundaries, method):
        self.event_bus = EventBusClient(events=["key_press", "shutdown"])
        self.event_bus.register("key_press", self.on_key_press)
        self.event_bus.register("shutdown", self.on_shutdown)

        self.keep_alive = True
        self.method = method

        self.actions = {
            65362: (dx2, step_size),            # up
            65364: (dx2, -step_size),           # down
            65361: (dx1, -step_size) if not self.method == 'polar' else (dy2, step_size),  # left
            65363: (dx1, step_size) if not self.method == 'polar' else (dy2, -step_size),  # right
        }

        self.dx = dx1
        self.dy = dx2
        self.dyaw = dy2

        self.x_virt = x_virt
        self.y_virt = y_virt
        self.yaw_virt = yaw_virt
        self.gain = gain
        
        self.yaw_virt.value = yaw
        self.bdr = border
        self.boundaries = boundaries

        print("OK (%s): position controller" % str(os.getpid()))

        self.run()

    def run(self):
        timestamp = time.time()

        while self.keep_alive:
            self.event_bus.check_messages()
            self.position_update()

            time_since_last = time.time() - timestamp
            if (1. / PositionController.update_frequency) > time_since_last:
                time.sleep((1. / PositionController.update_frequency) - time_since_last)
            timestamp = time.time()

    def is_inside(self, x0, y0):
        def is_in(x, y, sq, bdr):
            return sq[0][0] + bdr <= x <= sq[1][0] - bdr and sq[0][1] + bdr <= y <= sq[1][1] - bdr

        for square in self.boundaries.values():
            if is_in(x0, y0, square, self.bdr):
                return True

        return False
    
    def position_update(self):  # based on shared memory dx, dy
        delta_in = {
            'x': float(self.dx.value * self.gain),
            'y': float(self.dy.value * self.gain),
            'yaw': float(self.dyaw.value * self.gain * 50)
        }
        delta_actual = delta_in.copy()

        timestamp = time.time()

        # reset sensor inputs
        self.dx.value = 0.0
        self.dy.value = 0.0
        self.dyaw.value = 0.0

        if not (abs(delta_in['x']) > 10**(-5) or abs(delta_in['y']) > 10**(-5) or abs(delta_in['yaw']) > 10**(-5)):
            return  # no movement

        # on move
        yaw = self.yaw_virt.value * np.pi / 180.

        if self.method == 'polar':
            self.yaw_virt.value = (self.yaw_virt.value - delta_actual['yaw']) % 360.  # in deg

            delta_actual['x'] = np.sin(self.yaw_virt.value * np.pi / 180.) * delta_in['x']
            delta_actual['y'] = -np.cos(self.yaw_virt.value * np.pi / 180.) * delta_in['x']

        else:
            delta_actual['x'] = -1 * (np.cos(yaw) * delta_in['x'] + np.sin(yaw) * delta_in['y'])
            delta_actual['y'] = -np.sin(yaw) * delta_in['x'] + np.cos(yaw) * delta_in['y']
            delta_actual['yaw'] = 0.0

        # TODO: fix friction
        friction_base = abs(delta_in['x']) + abs(delta_in['y'])

        if not self.is_inside(self.x_virt.value + delta_actual['x'], self.y_virt.value + delta_actual['y']) and friction_base > 0:
            if not self.is_inside(self.x_virt.value + delta_actual['x'], self.y_virt.value):
                delta_actual['y'] *= abs(delta_actual['y']) / friction_base  # * by friction
                delta_actual['x'] = 0.0

            if not self.is_inside(self.x_virt.value, self.y_virt.value + delta_actual['y']):
                delta_actual['x'] *= abs(delta_actual['x']) / friction_base  # * by friction
                delta_actual['y'] = 0.0

        #print("Calculated: %s %s" % (delta_actual['x'], delta_actual['y']))

        self.x_virt.value += delta_actual['x']
        self.y_virt.value += delta_actual['y']

        move_args = (timestamp, self.x_virt.value, self.y_virt.value, self.yaw_virt.value, (delta_actual['x'], delta_actual['y']))
        self.event_bus.send({"move": move_args})

        # on collision
        if abs(delta_actual['x']) < abs(delta_in['x']) or abs(delta_actual['y']) < abs(delta_in['y']):
            imaginary_pos = (self.x_virt.value + delta_in['x'], self.y_virt.value + delta_in['y'])
            collision_args = (timestamp, imaginary_pos[0], imaginary_pos[1], tuple(delta_in.values()))
            self.event_bus.send({"collision": collision_args})

    def on_key_press(self, msg_data):
        action = int(msg_data[0])
        if action in self.actions.keys():
            var, step = self.actions[action]
            var.value += step

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: position controller")
        self.keep_alive = False


class MotivePositionController(object):

    update_frequency = 100  # Hz

    def __init__(self, scene_cfg):
        self.event_bus = EventBusClient(events=["shutdown"])
        self.event_bus.register("shutdown", self.on_shutdown)

        # import only for Motive controller
        from natnetclient import NatClient

        self.client = NatClient()
        self.arena_body = self.client.rigid_bodies['Arena']
        self.subject = self.client.rigid_bodies[scene_cfg['body']]

        self.keep_alive = True
        self.last_loop_time = time.time()

        print("OK (%s): motive position controller" % str(os.getpid()))

        self.run()

    def run(self):
        timestamp = time.time()

        while self.keep_alive:
            self.event_bus.check_messages()  # listening on events
            self.broadcast_position()        # broadcast motive position info

            time_since_last = time.time() - timestamp
            if (1. / MotivePositionController.update_frequency) > time_since_last:
                time.sleep((1. / MotivePositionController.update_frequency) - time_since_last)
            timestamp = time.time()

            # uncomment to print the real update frequency
            # print(1. / (timestamp - self.last_loop_time))
            self.last_loop_time = timestamp

    def broadcast_position(self):
        actual = time.time()

        x, y, z = self.subject.position
        xr, yr, zr = self.subject.rotation
        self.event_bus.send({"position_subject": (actual, x, y, z, xr, yr, zr)})

        x, y, z = self.arena_body.position
        xr, yr, zr, w = self.arena_body.quaternion
        self.event_bus.send({"position_arena": (actual, x, y, z, xr, yr, zr, w)})

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: motive position controller")
        self.keep_alive = False


class FakePositionController(object):

    update_frequency = 100  # Hz
    subject_default = [0.0, 0.45, 0.8, 0.0, 85.0, 2.0]
    arena_default = [0.0, 0.8, 0.16, 0.0, 0.0, 0.0, -1]

    def __init__(self, subject_default=subject_default, arena_default=arena_default):
        self.event_bus = EventBusClient(events=["shutdown"])
        self.event_bus.register("shutdown", self.on_shutdown)

        self.subject_actual = FakePositionController.subject_default
        self.arena_actual = arena_default

        self.keep_alive = True
        self.last_loop_time = time.time()

        print("OK (%s): FAKE position controller" % str(os.getpid()))

        self.run()

    def run(self):
        timestamp = time.time()

        while self.keep_alive:
            self.event_bus.check_messages()  # listening on events
            self.broadcast_position()        # broadcast motive position info

            time_since_last = time.time() - timestamp
            if (1. / MotivePositionController.update_frequency) > time_since_last:
                time.sleep((1. / MotivePositionController.update_frequency) - time_since_last)
            timestamp = time.time()

            # uncomment to print the real update frequency
            # print(1. / (timestamp - self.last_loop_time))
            self.last_loop_time = timestamp

    def broadcast_position(self):
        actual = time.time()

        x, y, z, xr, yr, zr = self.subject_actual
        self.event_bus.send({"position_subject": (actual, x, y, z, xr, yr, zr)})

        x, y, z, xr, yr, zr, w = self.arena_actual
        self.event_bus.send({"position_arena": (actual, x, y, z, xr, yr, zr, w)})

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: motive position controller")
        self.keep_alive = False


class MotivePositionControllerWithSync(object):

    update_frequency = 100  # Hz

    def __init__(self, scene_cfg):
        self.event_bus = EventBusClient(events=["shutdown"])
        self.event_bus.register("shutdown", self.on_shutdown)

        # sync with OE FIXME
        from ..inetwork import OpenEphysSocket
        hostname = "192.168.0.3"  #"10.153.170.88"
        port = 5556
        self.socket = OpenEphysSocket(hostname, port, True)
        self.socket.start_record(scene_cfg['rec_dir'])

        # import only for Motive controller
        from natnetclient import NatClient

        self.client = NatClient()
        self.arena_body = self.client.rigid_bodies['Arena']
        self.subject = self.client.rigid_bodies[scene_cfg['body']]

        self.keep_alive = True
        self.last_loop_time = time.time()

        print("OK (%s): motive position controller" % str(os.getpid()))

        self.run()

    def run(self):
        timestamp = time.time()
        t_sync = time.time()
        freq = 1

        while self.keep_alive:
            self.event_bus.check_messages()  # listening on events
            self.broadcast_position()        # broadcast motive position info

            time_since_last = time.time() - timestamp
            time_since_last_sync = time.time() - t_sync

            if (1. / freq) < time_since_last_sync:
                x, y, z = self.subject.position
                args = (time.time(), x, y, z)
                self.socket.send_message("time: %s x: %.8f y: %.8f z: %.8f" % args)
                t_sync = time.time()

            if (1. / MotivePositionController.update_frequency) > time_since_last:
                time.sleep((1. / MotivePositionController.update_frequency) - time_since_last)

            timestamp = time.time()

            # uncomment to print the real update frequency
            # print(1. / (timestamp - self.last_loop_time))
            self.last_loop_time = timestamp

    def broadcast_position(self):
        actual = time.time()

        x, y, z = self.subject.position
        xr, yr, zr = self.subject.rotation
        self.event_bus.send({"position_subject": (actual, x, y, z, xr, yr, zr)})

        x, y, z = self.arena_body.position
        xr, yr, zr, w = self.arena_body.quaternion
        self.event_bus.send({"position_arena": (actual, x, y, z, xr, yr, zr, w)})

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: motive position controller")
        self.keep_alive = False

        self.socket.stop_record()
