import time
import os
import datetime
from ..inetwork import OpenEphysSocket
from ..utils import EventBusClient


class AcquisitionSyncController(object):

    def __init__(self, hostname, port, freq, rec_dir, x, y, verbose=False):
        """
        Updates acquisition system with actual x, y (memory-shared) coordinates
        via network interface.

        :param hostname:        hostname of the acquisition system listener
        :param port:            port of the acquisition system listener
        :param freq:            communication frequency
        :param rec_dir:         folder to save recorded data on the remote
        :param x:               x coordinate
        :param y:               y coordinate
        :param verbose:         print messages to stdout
        """
        self.event_bus = EventBusClient(events=["shutdown"])
        self.event_bus.register("shutdown", self.on_shutdown)

        self.x = x
        self.y = y
        self.freq = freq

        self.socket = OpenEphysSocket(hostname, port, verbose)

        #socket.start_aqcuisition()  # will make OpenEphys crash, skip now

        previous = datetime.datetime.now()
        self.socket.start_record(rec_dir)
        self.socket.send_message("time: %s x: %s y: %s" % (str(previous), str(x.value), str(y.value)))

        self.keep_alive = True

        print("OK (%s): acquisition sync" % os.getpid())

        self.run()

    def run(self):
        timestamp = time.time()

        while self.keep_alive:
            self.event_bus.check_messages()

            current = datetime.datetime.now()
            self.socket.send_message("time: %s x: %s y: %s" % (str(current), str(self.x.value), str(self.y.value)))

            time_since_last = time.time() - timestamp
            if (1. / self.freq) > time_since_last:
                time.sleep((1. / self.freq) - time_since_last)
            timestamp = time.time()

    def on_shutdown(self, msg_data):
        self.socket.stop_record()

        #socket.stop_aqcuisition()  # will make OpenEphys crash, skip now

        print("SHUTDOWN: acquisition sync controller")
        self.keep_alive = False


class AcquisitionSocketSyncController(object):

    def __init__(self, hostname, port, freq, rec_dir, verbose=False):
        """
        Updates acquisition system with actual x, y (memory-shared) coordinates
        via network interface.

        :param hostname:        hostname of the acquisition system listener
        :param port:            port of the acquisition system listener
        :param freq:            communication frequency
        :param rec_dir:         folder to save recorded data on the remote
        :param verbose:         print messages to stdout
        """
        self.event_bus = EventBusClient(events=["shutdown", "position_subject"])
        self.event_bus.register("shutdown", self.on_shutdown)
        self.event_bus.register("position_subject", self.on_move)

        self.freq = freq
        self.last_position_time = datetime.datetime.now()
        self.subject_position = [0, 0, 0]

        self.socket = OpenEphysSocket(hostname, port, verbose)

        #socket.start_aqcuisition()  # will make OpenEphys crash, skip now
        self.socket.start_record(rec_dir)

        self.socket.send_message("start time %s at the VR computer" % str(datetime.datetime.now()))

        self.keep_alive = True

        print("OK (%s): acquisition sync" % os.getpid())

        self.run()

    def run(self):
        timestamp = time.time()

        while self.keep_alive:
            self.event_bus.check_messages()

            time_since_last = time.time() - timestamp
            if (1. / self.freq) < time_since_last:
                pos = self.subject_position
                args = (self.last_position_time, pos[0], pos[1], pos[2])
                self.socket.send_message("time: %s x: %.8f y: %.8f z: %.8f" % args)

                timestamp = time.time()

    def on_move(self, msg_data):
        self.last_position_time = msg_data[0]
        self.subject_position = [msg_data[1], msg_data[2], msg_data[3]]

    def on_shutdown(self, msg_data):
        self.socket.stop_record()
        #socket.stop_aqcuisition()  # will make OpenEphys crash, skip now

        print("SHUTDOWN: acquisition sync controller")
        self.keep_alive = False