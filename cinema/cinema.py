import ratcave as rc
import math
import numpy as np
import os
import time

import pyglet
import pyglet.gl as gl

_ROOT = os.path.abspath(os.path.dirname(__file__))

vertshader = open(os.path.join(_ROOT, 'shaders', 'warping.vert')).read()
fragshader = open(os.path.join(_ROOT, 'shaders', 'warping.frag')).read()
flattexture = open(os.path.join(_ROOT, 'shaders', 'flatTexture.frag')).read()
combshader = open(os.path.join(_ROOT, 'shaders', 'combShader.vert')).read()


class SphericalScene(rc.Scene):

    render_freq = 1. / 100
    gl_states = (gl.GL_DEPTH_TEST, gl.GL_POINT_SMOOTH, gl.GL_TEXTURE_CUBE_MAP, gl.GL_TEXTURE_2D)

    def __init__(self, sides=16, **kwargs):
        super(SphericalScene, self).__init__(**kwargs)

        self.sides = sides
        self.texShader = rc.Shader(vertshader, fragshader)
        self.meshShader = rc.Shader(combshader, flattexture)

        self.camera.projection.fov_y = 90  # height
        self.camera.projection.aspect = 1.
        self.camera.projection.z_near = .0001
        self.camera.projection.update()

        self.fbo = rc.FBO(rc.TextureCube())

        reader = rc.WavefrontReader(os.path.join(_ROOT, 'blender', 'projection.obj'))
        self.sphere = reader.get_mesh('projection_Sphere.006', position=(0, 0, 4.3), scale=1.0)
        #self.sphere = reader.get_mesh('Cone', position=(0, 0, 4.0), scale=1.0)

        self.sphere.rotation.y = 90
        #self.sphere.texture = self.fbo.texture
        self.sphere.textures.append(self.fbo.texture)

        self.sceneSphere = rc.Scene(meshes=[self.sphere])
        #self.sceneSphere.gl_states = self.sceneSphere.gl_states[:-1]
        self.sceneSphere.camera.rotation.y = 180
        self.sceneSphere.camera.position.xyz = 0, 0, 0
        self.sceneSphere.camera.projection.aspect = 1.77
        self.sceneSphere.camera.projection.z_far = 20.
        self.sceneSphere.camera.projection.fov_y = 25
        self.sceneSphere.camera.projection.update()

        self.yaw = 0.0

    def draw360_to_texture(self, cubetexture):
        """
        Draw each visible mesh in the scene from the perspective of the scene's camera and lit by its light, and
        applies it to each face of cubetexture, which should be currently bound to an FBO.
        """

        assert self.camera.projection.aspect == 1. and self.camera.projection.fov_y == 90  # todo: fix aspect property, which currently reads from viewport.
        assert type(cubetexture) == rc.TextureCube, "Must render to TextureCube"

        #self.ang += 0.1  # (self.ang % 180) - 180
        rot_offset = rc.RotationEulerDegrees(x=0, y=0, z=self.yaw).to_matrix()

        for face, rotation in enumerate([[180, -90, 0], [180, 90, 0], [90, 0, 0], [-90, 0, 0], [180, 0, 0], [0, 0, 180]]):
        #for face, rotation in enumerate([[180, -90, 0], [180, 90, 0], [90, 0, (self.ang % 180) - 180], [-90, 0, 0], [180, 0, 0], [0, 0, 180]]):
            self.camera.rotation.xyz = rotation
            new_matrix = np.dot(rot_offset, self.camera.rotation.to_matrix())
            self.camera.rotation = rc.RotationEulerDegrees.from_matrix(new_matrix)

            cubetexture.attach_to_fbo(face)
            self.draw(clear=True)

    def draw360_cubemap(self, cubetexture):
        """
        Draw each visible mesh in the scene from the perspective of the scene's camera and lit by its light, and
        applies it to each face of cubetexture, which should be currently bound to an FBO.
        """
        assert type(cubetexture) == rc.TextureCube, "Must render to TextureCube"

        for face, color in enumerate([(1., 0., 0.), (0., 1., 0.), (0., 0., 1.), (1., 1., 0.), (1., 0., 1.), (1., 1., 1.)]):
            self.bgColor = color
            cubetexture.attach_to_fbo(face)
            self.clear()

    def draw_360(self):
        self.clear()

        with self.meshShader, self.fbo:
            self.draw360_to_texture(self.fbo.texture)

        with self.texShader:
            self.sceneSphere.draw()
