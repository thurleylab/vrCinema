import asyncio
import random
import websockets
import numpy as np


async def time(websocket, path):
    while True:
        x = np.random.rand()
        y = np.random.rand()

        await websocket.send("%s, %s" % (x, y))
        await asyncio.sleep(random.random() * 1)

start_server = websockets.serve(time, "127.0.0.1", 5678)

asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()