import unittest
import time
import json
import os
import shutil
import cinema

from cinema.idisk import DataWriter


class TestDataWriter(unittest.TestCase):
    """
    Run tests from main project folder.
    """

    def setUp(self):
        self.where = "/tmp/foo"

        with open(os.path.join(os.path.dirname(cinema.__file__), "default.json")) as f:
            self.config = json.load(f)

        self.to_dump = {}
        self.to_dump.update(self.config["vr"]["scene"])
        self.to_dump.update(self.config["vr"]["subjects"])

        self.writer = DataWriter(self.where, self.to_dump)

    def tearDown(self):
        if os.path.exists(self.where):
            shutil.rmtree(self.where)

    def test_init_files(self):
        for name in DataWriter.filenames.values():
            assert(os.path.exists(os.path.join(self.where, name)))

        # TODO test headers

    def test_write_position(self):
        self.writer.write_position(time.time(), 50., 51., 52.)

        with open(os.path.join(self.where, DataWriter.filenames["positions"]), "r") as f:
            for line in f.readlines():
                if not line.startswith("#"):
                    data = line.split(" ")

                    assert (abs(float(data[1]) - 50.0) < 0.00001)
                    assert (abs(float(data[2]) - 51.0) < 0.00001)
                    assert (abs(float(data[3]) - 52.0) < 0.00001)

    def test_write_collision(self):
        self.writer.write_collision(time.time(), 40., 41., 42.)

        with open(os.path.join(self.where, DataWriter.filenames["collisions"]), "r") as f:
            for line in f.readlines():
                if not line.startswith("#"):
                    data = line.split(" ")

                    assert (abs(float(data[1]) - 40.0) < 0.00001)
                    assert (abs(float(data[2]) - 41.0) < 0.00001)
                    assert (abs(float(data[3]) - 42.0) < 0.00001)

    def test_write_hotspot(self):
        self.writer.write_hotspot(time.time(), 5, 30., 31., 32.)

        with open(os.path.join(self.where, DataWriter.filenames["collisions"]), "r") as f:
            for line in f.readlines():
                if not line.startswith("#"):
                    data = line.split(" ")

                    assert (abs(float(data[1]) - 40.0) < 0.00001)
                    assert (abs(float(data[2]) - 41.0) < 0.00001)
                    assert (abs(float(data[3]) - 42.0) < 0.00001)