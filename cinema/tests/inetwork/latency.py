from __future__ import print_function, unicode_literals

import sys
import zmq
import random
import string
import time
import numpy as np


def client(hostname='localhost', port=5556):
    freqs = [50, 100, 200]  # in Hz
    request_count = 100
    results = {}
    some_bytes = ''.join(random.choice(string.ascii_uppercase) for _ in range(100))

    print("Sending %d requests at.." % request_count)

    with zmq.Context() as ctx:
        with ctx.socket(zmq.REQ) as sock:  # blocking type of request socket

            def request(data):
                t1 = time.time()
                sock.send_string(some_bytes)
                response = sock.recv_string()
                t2 = time.time()

                if t2 < t1 + delay:
                    time.sleep(t1 + delay - t2)

                return t2 - t1

            sock.RCVTIMEO = 1000  # timeout in milliseconds
            sock.connect('tcp://%s:%d' % (hostname, port))

            for freq in freqs:
                print("%s Hz.. " % freq)
                delay = 1.0 / freq  # in seconds

                test_begin = time.time()
                time_diffs = [request(some_bytes) for x in range(request_count)]  # testing for 3 seconds
                test_end = time.time()

                results[freq] = {
                    "testing_time": 1000.*(test_end - test_begin),
                    "latency_mean": 1000.*(np.array(time_diffs).mean())
                }

    for freq, stats in results.items():
        print("%d Hz: testing time - %f ms, average latency - %f ms" % (freq, stats["testing_time"], stats["latency_mean"]))


if __name__ == '__main__':
    client(sys.argv[1], int(sys.argv[2]))
