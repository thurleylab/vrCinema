import os
import sys
import json

from sensors.mouse import Mouse
from serial.tools.list_ports import comports


"""
devices.json contains identification info for currently
installed optical mouse sensors.
"""
_ROOT = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(_ROOT, 'devices.json')) as f:
    devices = json.load(f)

with open(os.path.join(_ROOT, 'default.json')) as f:
    default_config = json.load(f)


class Autodetect(object):
    """
    Autodetect devices and return their configurations.
    """
    devices = devices

    @staticmethod
    def sensor_cfg():
        """
        for linux include this to udev configuration /etc/udev/rules.d/mouse-sensors.rules
        SUBSYSTEM=="usb", ATTR{idVendor}=="0fcf", ATTR{idProduct}=="1008", MODE="666"

        to distinguish 2 identical devices on Windows: try infi.devicemanager
        pip install infi.devicemanager
        https://github.com/Infinidat/infi.devicemanager

        :return: config dict for sensors if found, {} otherwise
        """
        sensors = {}
        cfg = default_config["sensors"]

        for dev in Autodetect.devices:
            filtered = Mouse.list_connected(idProduct=dev['product_id'], idVendor=dev['vendor_id'])
            for mouse in filtered:
                index = "%s:%s" % (mouse.usb_device.bus, mouse.usb_device.address)
                if index not in sensors.keys():
                    sensors[index] = mouse

        if len(sensors) > 1:
            devices = list(sensors.values())
            cfg['use_sensors'] = True
            cfg['sensor_west'] = {
                'bus': devices[0].usb_device.bus,
                'address': devices[0].usb_device.address,
            }
            cfg['sensor_north'] = {
                'bus': devices[1].usb_device.bus,
                'address': devices[1].usb_device.address,
            }

        return cfg

    @staticmethod
    def arduino_cfg():
        cfg = default_config["arduino"]

        port = None
        cfg['port'] = port
        cfg['connect'] = False

        try:
            if sys.platform.startswith('win'):
                port = [x for x in comports() if x.description.lower().find('arduino') > -1][0].device

            elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):

                # old version of pySerial
                if len(comports()) > 0 and type(comports()[0]) == tuple:
                    port = [x for x in comports() if x[0].find('ACM') > -1][0][0]

                # newer version of pySerial
                else:
                    port = [x for x in comports() if x.device.find('ACM') > -1][0].device

            if port:
                cfg['port'] = port
                cfg['connect'] = True

        except IndexError:
            pass

        return cfg

    @staticmethod
    def arduino_port_autodetect():
        port = None

        try:
            if sys.platform.startswith('win'):
                port = [x for x in comports() if x.description.lower().find('arduino') > -1][0].device

            elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):

                # old version of pySerial
                if len(comports()) > 0 and type(comports()[0]) == tuple:
                    port = [x for x in comports() if x[0].find('ACM') > -1][0][0]

                # newer version of pySerial
                else:
                    port = [x for x in comports() if x.device.find('ACM') > -1][0].device

            return port

        except IndexError:
            pass  # no device found
