import datetime

from os import path
from shutil import copyfile


class DataWriter(object):

    filenames = {
        "position": "positions.traj",
        "collision": "collisions.traj",
        "hotspot": "hotspots.traj",
        "reward": "rewards.traj",
        "external": "external.traj",
        "arena": "arena.traj",
        "vr_event": "vr_events.traj"
    }

    def __init__(self, where, settings_dict):

        def init_file(file_path):
            with open(file_path, "a") as f:
                for key, value in settings_dict.items():
                    f.write("# %s='%s'\n" % (key, value))

                f.write("# start_time='%s'\n" % str(init_time))

        init_time = datetime.datetime.now()

        self.where = where
        self.settings_dict = settings_dict

        for name in self.filenames.values():
            file_path = path.join(self.where, name)
            if not path.isfile(file_path):
                init_file(file_path)

    def log(self, data_type, *args):
        with open(path.join(self.where, self.filenames[data_type]), "a") as f:
            f.write(" ".join([repr(x) for x in args]) + "\n")

    def dump_file(self, source_path):
        copyfile(source_path, path.join(self.where, path.basename(source_path)))
