from __future__ import print_function

from multiprocessing import Process  # freeze_support maybe needed on Win
from zmq.devices.basedevice import ProcessDevice
import zmq
from multiprocessing.sharedctypes import Value
from os import path
from .utils import EventBusClient

import os
import time
import datetime
import json


class DispatcherMini(object):
    """
    Manage separate processes for scene rendering,
    mouse sensors tracking and acquisition system
    communication.

    :param 
        x_virt      - shared memory Double virtual X coordinate
        y_virt      - shared memory Double virtual Y coordinate
        config      - configuration dict
        save_to     - path to a common directory where to store tracking / logs
    """

    @staticmethod
    def read_settings(config_path):
        from .autodetect import Autodetect

        with open(config_path) as f:
            config = json.load(f)

        settings = {
            'sensor_cfg': config["sensors"] if not config["sensors"]["autodetect"] else Autodetect.sensor_cfg(),
            'arduino_cfg': config["arduino"], # if not config["arduino"]["autodetect"] else Autodetect.arduino_cfg(),
            'ephys_cfg': config["ephys"],
            'video_cfg': config["videotracking"],
            'position_cfg': config["position"],
            'logging_cfg': config["logging"],
            'sound_cfg': config["sound"],
            'vr_cfg': config["vr"],
            'infowindow_cfg': config["infowindow"],
            'event_bus_cfg': config["event_bus"],
            'save_to': config["save_to"]
        }

        if settings["arduino_cfg"]["autodetect"]:
            port = Autodetect.arduino_port_autodetect()
            if port:
                settings["arduino_cfg"]["port"] = port

        return settings

    @staticmethod
    def start_ratcave(controllers, event_bus_cfg):
        forwarder_device = ProcessDevice(zmq.FORWARDER, zmq.SUB, zmq.PUB)
        forwarder_device.bind_in("tcp://127.0.0.1:%d" % event_bus_cfg["bind_in"])
        forwarder_device.bind_out("tcp://127.0.0.1:%d" % event_bus_cfg["bind_out"])
        forwarder_device.setsockopt_in(zmq.SUBSCRIBE, b"")

        processes = []

        for name, ctrl_data in controllers.items():
            p = Process(target=ctrl_data[0], args=ctrl_data[1])
            processes.append(p)

        forwarder_device.start()

        for proc in processes:
            proc.start()

        def kill_all(msg_data):
            print("Killing zombie processes..")

            time.sleep(0.5)
            for proc in processes:
                proc.terminate()

        event_bus = EventBusClient(events=["shutdown"])
        event_bus.register("shutdown", kill_all)

        all_dead = False
        while not all_dead:
            event_bus.check_messages()

            all_dead = True
            for proc in processes:
                if proc.is_alive():
                    all_dead = False

            # info = ["%s: %s" % (p.pid, str(p.is_alive())) for p in processes]
            # print(info)
            time.sleep(0.5)

        print("All nodes dead, kill the event bus now..")
        forwarder_device.join(timeout=0.1)


    @staticmethod
    def start(scene_builder, controllers, source_files_path, settings):
        # alloc shared memory
        x1 = Value('d', 0.0, lock=False)  # first sensor X coordinate
        y1 = Value('d', 0.0, lock=False)  # first sensor Y coordinate
        x2 = Value('d', 0.0, lock=False)  # second sensor X coordinate
        y2 = Value('d', 0.0, lock=False)  # second sensor Y coordinate
    
        x_virt = Value('d', 0.0, lock=False)  # virtual X coordinate
        y_virt = Value('d', 0.0, lock=False)  # virtual Y coordinate
        yaw_virt = Value('d', 0.0, lock=False)  # virtual camera rotation
    
        # create common directory for tracking / logs
        if settings["logging_cfg"]["enabled"] or settings['video_cfg']["record"]["enabled"]:
            subject = settings['vr_cfg']["subjects"]["animal"]
            session = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
            where = path.join(settings['save_to'], subject, session)
    
            if not path.exists(where):
                os.makedirs(where)
    
            settings["logging_cfg"]["where"] = where
            settings['video_cfg']["record"]["where"] = where
            settings['ephys_cfg']["rec_dir"] = os.path.join(settings['ephys_cfg']["rec_dir"], subject)
    
        forwarder_device = ProcessDevice(zmq.FORWARDER, zmq.SUB, zmq.PUB)
        forwarder_device.bind_in("tcp://127.0.0.1:%d" % settings["event_bus_cfg"]["bind_in"])
        forwarder_device.bind_out("tcp://127.0.0.1:%d" % settings["event_bus_cfg"]["bind_out"])
        forwarder_device.setsockopt_in(zmq.SUBSCRIBE, b"")
    
        processes = []

        # launch sensors tracking
        cfg = settings['sensor_cfg']
        if cfg['enabled']:
            args = (cfg['sensor_west'], cfg['frequency'], cfg['speed_west'], x1, y1, cfg['verbose'], False)
            processes.append(Process(target=controllers["sensor"], args=args))
    
            args = (cfg['sensor_north'], cfg['frequency'], cfg['speed_north'], x2, y2, cfg['verbose'], True)
            processes.append(Process(target=controllers["sensor"], args=args))
    
        # launch arduino communication
        cfg = settings['arduino_cfg']
        if cfg['connect']:
            args = (cfg['port'], cfg['baud'], cfg['syringe'], cfg['displacement'], cfg['delay'], cfg['verbose'])
            processes.append(Process(target=controllers["serial"], args=args))
    
        # launch position controller
        cfg = settings['position_cfg']
        args = (x1, y1, x2, y2, x_virt, y_virt, yaw_virt, cfg["gain"], cfg["keyboard_step"],
                settings['vr_cfg']["scene"]["yaw"], cfg["border"], cfg["boundaries"], cfg["method"])
        processes.append(Process(target=controllers["position"], args=args))
    
        # launch logging
        cfg = settings['logging_cfg']
        if cfg['enabled']:
            args = (cfg['where'], source_files_path)
            processes.append(Process(target=controllers["log"], args=args))

        # launch video recording
        cfg = settings['video_cfg']
        if cfg['enabled']:
            processes.append(Process(target=controllers["video"], args=(cfg,)))

        # launch open ephys communication
        cfg = settings["ephys_cfg"]
        if cfg['connect']:
            args = (cfg['hostname'], cfg['port'], cfg['frequency'], cfg['rec_dir'], x_virt, y_virt, cfg['verbose'])
            processes.append(Process(target=controllers["acquisition"], args=args))

        # launch trajectory window
        cfg = settings["infowindow_cfg"]
        if cfg["enabled"]:
            processes.append(Process(target=controllers["trajectory"], args=(x_virt, y_virt, cfg)))

        # launch sound controller
        cfg = settings['sound_cfg']
        if cfg['enabled']:
            args = (cfg['filepath'], cfg['count'], cfg['delay'])
            processes.append(Process(target=controllers["sound"], args=args))

        # launch VR projection
        args = (scene_builder, settings['vr_cfg']["scene"], settings['vr_cfg']["screen"])
        processes.append(Process(target=controllers["scene"], args=args))

        forwarder_device.start()
        for proc in processes:
            proc.start()

        all_dead = False
        while not all_dead:
            all_dead = True
            for proc in processes:
                if proc.is_alive():
                    all_dead = False

            time.sleep(0.5)

        print("All nodes dead, kill the event bus now..")
        forwarder_device.join(timeout=0.1)

