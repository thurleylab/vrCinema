import numpy as np
import zmq


def if_enabled(func):
    def call_if_enabled(self, *args, **kwargs):
        if self.enabled:
            return func(self, *args, **kwargs)
    return call_if_enabled


def build_borders_from_squares(yaw, boundaries):
    def to_real(x0, y0):
        yaw_rad = np.pi * yaw / 180.
        x = x0 * np.cos(-yaw_rad) - y0 * np.sin(-yaw_rad)
        y = x0 * np.sin(-yaw_rad) + y0 * np.cos(-yaw_rad)
        return x, y

    def is_inside(x0, y0):
        is_in = lambda x, y, sq: sq[0][0] <= x <= sq[1][0] and sq[0][1] <= y <= sq[1][1]
        x1, y1 = to_real(x0, y0)

        for square in boundaries.values():
            if is_in(x1, y1, square):
                return True

        return False
    return is_inside


class BaseSpot(object):

    def __init__(self, sid, r, init_state=False):
        self.sid = sid  # could be uuid.uuid4().hex[:10]
        self.r = r

        self._visited = init_state

    def __repr__(self):
        is_hidden = "" if self.available() else "(hidden) "
        return "Spot %s%s at (%2f, %2f)" % (is_hidden, self.sid, self.x, self.y)

    def is_inside(self, x0, y0):
        return np.sqrt((x0 - self.x) ** 2 + (y0 - self.y) ** 2) < self.r

    def available(self):
        return not self._visited

    def check_in(self):
        self._visited = True

    def reset(self):
        self._visited = False

    @property
    def x(self):
        raise NotImplementedError

    @x.setter
    def x(self, value):
        raise NotImplementedError

    @property
    def y(self):
        raise NotImplementedError

    @y.setter
    def y(self, value):
        raise NotImplementedError


class HotSpot(BaseSpot):

    def __init__(self, sid, x, y, r, init_state=False):
        super(HotSpot, self).__init__(sid, r, init_state)

        self._x = x
        self._y = y

    def __repr__(self):
        is_hidden = "" if self.available() else "(hidden) "
        return "Spot %s%s at (%2f, %2f)" % (is_hidden, self.sid, self.x, self.y)

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, value):
        self._x = value

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, value):
        self._y = value


class Beacon(BaseSpot):

    def __init__(self, sid, r, beacon):
        super(Beacon, self).__init__(sid, r)

        self.beacon = beacon

    @property
    def x(self):
        return self.beacon.position[0]

    @property
    def y(self):
        return self.beacon.position[1]

    def check_in(self):
        super(Beacon, self).check_in()

        self.beacon.visible = False

    def reset(self):
        super(Beacon, self).reset()

        self.beacon.visible = True


class MessageBus:
    """
    taken and modified from
    https://newcircle.com/s/post/1743/2015/06/17/tutorial-the-observer-pattern-in-python
    """

    def __init__(self, events):
        # maps event names to subscribers
        # str -> dict
        self.events = {event: [] for event in events}

    def get_subscribers(self, event):
        return self.events[event]

    def register(self, event, who, callback):
        self.get_subscribers(event).append((who, callback))

    def unregister(self, event, who):
        subscribers = self.get_subscribers(event)
        for i, pair in self.get_subscribers(event):
            if pair[0] == who:
                subscribers.pop(i)
                break

    def dispatch(self, event, *args):
        for subscriber, callback in self.get_subscribers(event):
            callback(*args)


class EventBus(object):

    def __init__(self):
        try:
            context = zmq.Context()
            # Socket facing clients
            frontend = context.socket(zmq.SUB)
            frontend.bind("tcp://*:5559")
            frontend.setsockopt_string(zmq.SUBSCRIBE, b"")

            # Socket facing services
            backend = context.socket(zmq.PUB)
            backend.bind("tcp://*:5560")

            print("zmq device is UP")
            zmq.device(zmq.FORWARDER, frontend, backend)

        except Exception as e:
            print(e)
            print("bringing DOWN zmq device")

        finally:
            frontend.close()
            backend.close()
            context.term()


class EventBusClient(object):

    def __init__(self, events, port_push="5559", port_pull="5560"):
        self.context = zmq.Context()

        #self.push_socket = self.context.socket(zmq.PUSH)
        self.push_socket = self.context.socket(zmq.PUB)
        #self.push_socket.setsockopt(zmq.SNDHWM, 2)
        #self.push_socket.setsockopt(zmq.SNDBUF, 2*1024)
        #self.push_socket.bind("tcp://localhost:%s" % port_push)
        self.push_socket.connect("tcp://localhost:%s" % port_push)

        #self.pull_socket = self.context.socket(zmq.PULL)
        self.pull_socket = self.context.socket(zmq.SUB)
        #self.pull_socket.setsockopt(zmq.RCVHWM, 2)
        #self.pull_socket.setsockopt(zmq.RCVBUF, 2*1024)
        #self.pull_socket.set_hwm(2)
        self.pull_socket.connect("tcp://localhost:%s" % port_pull)
        self.pull_socket.setsockopt(zmq.SUBSCRIBE, b"")

        self.events = {event: [] for event in events}

    def add_event(self, event_name):
        if not event_name in self.events:
            self.events[event_name] = []
        else:
            raise ValueError("Event with this name already exist")

    def get_subscribers(self, event):
        return self.events[event]

    def register(self, event, callback):
        self.get_subscribers(event).append(callback)

    def unregister(self, event, callback):
        subscribers = self.get_subscribers(event)
        for func in self.get_subscribers(event):
            if func == callback:
                subscribers.pop(func)
                break

    def dispatch(self, event, *args):
        for callback in self.get_subscribers(event):
            callback(*args)

    def send(self, json_data):
        self.push_socket.send_json(json_data)

    def check_messages(self):
        message_counter = 0

        try:
            while True:
                json_data = self.pull_socket.recv_json(flags=zmq.NOBLOCK)
                message_counter += 1

                for event, args in json_data.items():
                    if event in self.events:
                        self.dispatch(event, args)

                if message_counter > 20:  # avoid total blocking on high frequency spam
                    break

        except zmq.Again as e:
            pass  # no message
