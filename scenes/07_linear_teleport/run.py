from __future__ import absolute_import

import sys
import os
from threading import Timer

import pyglet
pyglet.options['debug_gl'] = False

import random
import cinema
import numpy as np
from cinema.dispatch import DispatcherMini
from cinema.utils import Beacon, build_borders_from_squares

from cinema.controllers.sensor import SensorController
from cinema.controllers.log import LoggingController
from cinema.controllers.position import PositionController
from cinema.controllers.scene import SceneController
from cinema.controllers.serial import SerialController
from cinema.controllers.video import VideoController
from cinema.controllers.acquisition import AcquisitionSyncController
from cinema.controllers.trajectory import TrajectoryWindowController
from cinema.controllers.sound import SoundController

_ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(_ROOT)


class LocalPositionController(PositionController):

    def run(self):
        self.event_bus.add_event("hotspot")
        self.event_bus.register("hotspot", self.schedule_position_reset)

        super(LocalPositionController, self).run()

    def schedule_position_reset(self, *args):
        t = Timer(2.0, self.position_reset)
        t.start()

    def position_reset(self):
        self.x_virt.value = 0.0
        self.y_virt.value = 0.0
        self.yaw_virt.value = random.choice([90])


class SelfMovingBeacon(Beacon):

    def check_in(self):
        Beacon.check_in(self)

        pyglet.clock.schedule_once(self.position_reset, 2.0)

    def position_reset(self, dt):
        curr_pos = self.beacon.position

        self.beacon.position.xyz = (3. + 2. * np.random.rand(1)[0], curr_pos[1], curr_pos[2])
        self.reset()


def scene_builder(scene_config):
    import ratcave as rc
    from cinema.cinema import SphericalScene

    reader = rc.WavefrontReader('track.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}

    beacon = [mesh for key, mesh in meshes.items() if key.lower().find('beacon') > -1][0]
    hotspots = [SelfMovingBeacon(1, 0.5, beacon)]
    hotspots[0].check_in()

    root = rc.EmptyEntity()
    for mesh in meshes.values():
        root.add_child(mesh)

    scene = SphericalScene(sides=scene_config["sides"], meshes=root, bgColor=(1. / 255., 1. / 255., 1. / 255.))

    #scene.meshes.rotation.xyz = (0., 0., scene_config["yaw"])
    scene.camera.position.xyz = (scene_config['x0'], scene_config['y0'], 0.1)
    scene.camera.rotation.xyz = (90., 0., 90.)
    scene.camera.projection.z_far = 10.0
    scene.hotspots = hotspots

    return scene


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        print("WARNING! Using default settings..")
        path = os.path.join(os.path.dirname(cinema.__file__), "default.json")

    settings = DispatcherMini.read_settings(path)

    controllers = {
        "sensor": SensorController,
        "log": LoggingController,
        "position": LocalPositionController,
        "scene": SceneController,
        "serial": SerialController,
        "video": VideoController,
        #"video": None,
        "acquisition": AcquisitionSyncController,
        "trajectory": TrajectoryWindowController,
        "sound": SoundController
        #"sound": None
    }

    DispatcherMini.start(scene_builder, controllers, _ROOT, settings)
