from __future__ import absolute_import

import sys
import os
import json
import datetime, time
import numpy as np
import ratcave as rc


ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(ROOT)

from scipy.stats import norm
from threading import Timer
from cinema.dispatch import DispatcherMini
from cinema.controllers.acquisition import AcquisitionSocketSyncController
from cinema.controllers.video import VideoController
from cinema.controllers.position import MotivePositionController
from cinema.controllers.log import RatcaveLoggingController
from cinema.controllers.scene import RatcaveSceneController
from cinema.controllers.serial import FeederController
from cinema.controllers.trajectory import InfoWindowController
from cinema.controllers.sound import DoubleSoundController, SoundController


# TODO refactor scene_builder to controllers

def vr_scene_builder(scene_cfg):
    reader = rc.WavefrontReader('Y.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}
    root = rc.EmptyEntity()
    root.rotation = root.rotation.to_quaternion()

    for name, mesh in meshes.items():
        root.add_child(mesh)

    vr_scene = rc.Scene(meshes=root, bgColor=(0., 0., 0.))
    vr_scene.camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=90, aspect=1., z_near=.005, z_far=20.))
    vr_scene.root = root
    vr_scene.meshes_dict = meshes

    return vr_scene


class RatcaveVFGSceneController(RatcaveSceneController):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, *args, **kwargs):
        self.scene_cfg = scene_cfg
        self.rot = None
        self.original_position = None
        self.calib_mesh_A = None
        self.calib_mesh_B = None
        self.calib_mesh_C = None
        self.do_activate_YZ = False
        self.do_activate_XZ = False
        self.current_state = None
        self.hotspots = None
        self.feed_possible = False

        self.rot_shift_x = 0.0
        self.rot_shift_z = 0.0
        self.rot_world_x = 0.0
        self.rot_world_z = 0.0
        self.alpha_start = 0.0
        self.rot_angle_min = 0.0
        self.rot_angle_max = 0.0
        self.last_delta_z = 0.0

        self.cdf_length = scene_cfg['cdf_length']
        self.feed_interval = scene_cfg['feed_interval']
        self.timestamp = time.time()
        self.arena_y_comp = -0.055
        self.timers = []

        self.reward_delay = scene_cfg["reward_delay"]           # time an animal waits in the reward zone to get reward
        self.refractory_delay = scene_cfg["refractory_delay"]   # time in sec to let an animal eat the reward
        self.reward_radius = scene_cfg["reward_radius"]         # reward zone radius from the beacon center
        self.in_hotspot_since = 0
        self.on_refractory_since = 0

        super(RatcaveVFGSceneController, self).__init__(scene_builder, scene_cfg, screen_cfg, *args, **kwargs)

    def run(self):
        # FIXME compensating virtual coordinates to Arena orientation
        self.scene.camera.position.x += .02

        for msh in self.vr_scene.meshes.children:
            msh.original_position = np.array(msh.position.xyz)

        self.calib_mesh_A = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('A_calibration') > -1][0]
        self.calib_mesh_B = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('B_calibration') > -1][0]
        self.calib_mesh_C = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('C_calibration') > -1][0]

        self.active_beacons = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('beacon') > -1]
        self.magic_sphere = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('magic_sphere') > -1][0]
        self.magic_sphere.visible = False
        self.null_object = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('null_object') > -1][0]
        self.null_object.visible = False

        self.arrows = {}
        for room in ['x', 'y', 'z']:
            self.arrows[room] = {
                'left': [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.lower().find(room + '_arrow_left') > -1][0],
                'right': [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.lower().find(room + '_arrow_right') > -1][0]
            }
            for key, mesh in self.arrows[room].items():
                mesh.room = room
                mesh.direction = key
                mesh.visible = False

        self.set_xy()
        self.shift_meshes_to_pivot()
        self.show_arrows()
        self.event_bus.send({"vr_event": (time.time(), self.current_state)})

        # scheduling
        t1 = Timer(self.scene_cfg['timers']['stop_experiment'], self.stop_experiment)
        # t2 = Timer(self.scene_cfg['timers']['switch_to_YZ'], self.init_YZ)
        # t3 = Timer(self.scene_cfg['timers']['switch_to_XZ'], self.init_XZ)
        #t4 = Timer(self.feed_interval, self.do_feed)

        self.timers = [t1]
        for timer in self.timers:
            timer.start()

        # FIXME error with OpenGL rendering - requires position data first
        time.sleep(2)
        super(RatcaveVFGSceneController, self).run()

    def get_active_arrows(self):
        arrows = []
        for room in ['x', 'y', 'z']:
            for direction in ['left', 'right']:
                if self.arrows[room][direction].visible:
                    arrows.append(self.arrows[room][direction])
        return arrows

    def update(self, dt):
        self.check_messages()

        # transformation of "Arena" rigid body center point in the OptiTrack coordinate system to the virtual arena
        # (where the image is projected) center point defined in arena.obj coordinate system
        self.arena.position.xyz = np.array(self.arena_body_position) + np.array((-0.005, -0.36, 0.09))
        self.arena.rotation.xyzw = np.array(self.arena_body_quaternion) + np.array((0, 0.012, 0, 0))

        # Align the VR scene coordinate system with the virtual arena (arena.obj) coordinate system
        self.vr_scene.root.position.xyz = self.arena.position.xyz
        x1, y1, z1, w1, = self.arena.rotation.xyzw
        self.vr_scene.root.rotation.xyzw = (x1, y1 + self.arena_y_comp, z1, w1)

        if self.original_position is None:
            self.original_position = np.array(self.vr_scene.root.position.xyz)

        # animal position
        # bring subject position from OptiTrack coordinates to VR coordinates
        s_pos = self.subject_position

        phi = np.pi * 4.3 / 180.0

        x = s_pos[0] * np.cos(phi) - s_pos[2] * np.sin(phi)
        z = s_pos[0] * np.sin(phi) + s_pos[2] * np.cos(phi)
        y = s_pos[1]

        new_pos = [x, y, z]  # subject position in VR coordinates

        x1, y1, z1, = self.subject_position
        self.vr_scene.camera.position.xyz = [x1, y1, z1]
        self.scene.camera.uniforms['playerPos'] = [x1, y1, z1]
        self.vr_scene.light.position.xyz = (x1, self.vr_scene.light.position.xyz[1], z1)  # light moves with the animal

        # rotation part

        # from -0.5 to 0.5 depending on animal Z pos  FIXME use new_pos instead of camera?
        delta = (self.vr_scene.camera.position.z - self.null_object.original_position[2] - 0.25) / 1.6  # arena length
        delta_norm = norm.cdf((-delta) * self.cdf_length)
        to_add = self.alpha_start - delta_norm * (self.rot_angle_max - self.rot_angle_min)
        angle = np.deg2rad(to_add)

        if self.rot is None:
            self.rot = self.vr_scene.root.rotation.to_euler()
            self.rot_original = self.vr_scene.root.rotation.to_euler()

        self.rot.y = self.rot_original.y + angle
        x1, y1, z1, w1, = self.rot.to_quaternion().xyzw
        self.vr_scene.root.rotation.xyzw = (x1, y1, z1, w1)

        self.vr_scene.root.position.x = self.original_position[0] + self.rot_world_x
        self.vr_scene.root.position.z = self.original_position[2] + self.rot_world_z

        # reward if needed
        if self.feed_possible and (self.last_delta_z + 0.2) * (delta + 0.2) < 0:
            self.do_feed()
            self.feed_possible = False

        self.last_delta_z = delta

        # if self.do_activate_YZ and self.vr_scene.camera.position.xyz[2] < -0.45:  # criteria for XY -> YZ
        #     self.set_yz()
        #     self.do_activate_YZ = False
        #     self.event_bus.send({"vr_event": (time.time(), 'XY -> YZ')})
        #
        # if self.do_activate_XZ and self.vr_scene.camera.position.xyz[2] > 0.5:   # criteria for YZ -> XZ
        #     self.set_xz()
        #     self.do_activate_XZ = False
        #     self.event_bus.send({"vr_event": (time.time(), 'YZ -> XZ')})

        if self.hotspots is None:
            self.hotspots = [
                (self.arena.position.x - 0.26, self.arena.position.z - 0.65),  # top left
                (self.arena.position.x + 0.28, self.arena.position.z - 0.65),  # top right
                (self.arena.position.x - 0.26, self.arena.position.z + 0.68),  # bottom left
                (self.arena.position.x + 0.28, self.arena.position.z + 0.68)   # bottom right
            ]

        # check arrows
        if not self.on_refractory_since:
            at_some_spot = False
            for i, spot in enumerate(self.hotspots):  # i = 0, 1 top spots, 2, 3 - bottom spots
                dist = np.sqrt(np.square(new_pos[0] - spot[0]) + np.square(new_pos[2] - spot[1]))

                state_index = 1 if i == 0 or i == 1 else 0
                if dist < self.reward_radius and self.arrows[self.current_state[state_index]]['right'].visible:  # entered the hotspot
                    at_some_spot = True

                    direction = 'right' if i == 0 or i == 3 else 'left'
                    self.record_hotspot(self.current_state[state_index], direction)

            if not at_some_spot and self.in_hotspot_since:  # exit the hotspot
                self.in_hotspot_since = 0

    def set_xy(self):
        self.rot_shift_x = -self.calib_mesh_A.original_position[0]  # 0.3
        self.rot_shift_z = -self.calib_mesh_A.original_position[2]  # 0.0
        self.rot_angle_min = 0.0
        self.rot_angle_max = 60.0
        self.alpha_start = 0.0
        self.rot_world_x = self.calib_mesh_A.original_position[0]   # -0.3
        self.rot_world_z = self.calib_mesh_A.original_position[2] + 0.02
        self.current_state = 'xy'

    def set_yx(self):
        self.rot_shift_x = -self.calib_mesh_A.original_position[0]  # 0.3
        self.rot_shift_z = -self.calib_mesh_A.original_position[2]  # 0.0
        self.rot_angle_min = 60.0
        self.rot_angle_max = 0.0
        self.alpha_start = 120.0
        self.rot_world_x = 0.38
        self.rot_world_z = -0.02
        self.current_state = 'yx'

    def set_yz(self):
        self.rot_shift_x = -self.calib_mesh_B.original_position[0]  # 0.0
        self.rot_shift_z = -self.calib_mesh_B.original_position[2]  # 0.4
        self.rot_angle_min = 0.0
        self.rot_angle_max = 60.0
        self.alpha_start = 120.0
        self.rot_world_x = -0.34
        self.rot_world_z = 0.04
        self.current_state = 'yz'

    def set_zy(self):
        self.rot_shift_x = -self.calib_mesh_B.original_position[0]  # 0.0
        self.rot_shift_z = -self.calib_mesh_B.original_position[2]  # 0.4
        self.rot_angle_min = 60.0
        self.rot_angle_max = 0.0
        self.alpha_start = -120.0
        self.rot_world_x = 0.38  # half of arena width
        self.rot_world_z = -0.02
        self.current_state = 'zy'

    def set_xz(self):
        self.rot_shift_x = -self.calib_mesh_C.original_position[0]
        self.rot_shift_z = -self.calib_mesh_C.original_position[2]
        self.rot_angle_min = 60.0
        self.rot_angle_max = 0.0
        self.alpha_start = 0.0
        self.rot_world_x = 0.38  # half of arena width
        self.rot_world_z = -0.02
        self.current_state = 'xz'

    def set_zx(self):
        self.rot_shift_x = -self.calib_mesh_C.original_position[0]
        self.rot_shift_z = -self.calib_mesh_C.original_position[2]
        self.rot_angle_min = 0.0
        self.rot_angle_max = 60.0
        self.alpha_start = -120.0
        self.rot_world_x = -0.34
        self.rot_world_z = 0.04
        self.current_state = 'zx'

    def shift_meshes_to_pivot(self):
        # move ALL meshes to pivot point
        for msh in self.vr_scene.meshes.children:
            msh.position.x = msh.original_position[0] + self.rot_shift_x
            msh.position.z = msh.original_position[2] + self.rot_shift_z

    # events

    def record_hotspot(self, room, direction):
        # activate a hotspot
        if not self.in_hotspot_since:
            self.in_hotspot_since = time.time()
            # do something when animal entered reward zone

        # enough time in hostspot: change direction, (switch) rooms, activate refractory
        if time.time() - self.in_hotspot_since > self.reward_delay:
            old_state = str(self.current_state)
            self.hide_arrows()
            self.change_direction(room, direction)
            if not old_state == self.current_state:  # click when new rooms active
                x, y = self.vr_scene.camera.position.x, self.vr_scene.camera.position.y
                self.event_bus.send({"hotspot": (time.time(), '%s_%s' % (room, direction), float(x), float(y))})

                self.feed_possible = True

            self.event_bus.send({"vr_event": (time.time(), self.current_state)})

            # refractory period
            self.on_refractory_since = time.time()

            Timer(self.refractory_delay, self.cancel_refractory).start()  # independent of reward for now

    def show_arrows(self):
        if self.scene_cfg['alternate']:
            room = self.current_state[0]
        else:
            room = self.current_state[0] if np.random.rand() > 0.5 else self.current_state[1]

        self.arrows[room]['left'].visible = True
        self.arrows[room]['right'].visible = True

    def hide_arrows(self):
        for room in ['x', 'y', 'z']:
            self.arrows[room]['left'].visible = False
            self.arrows[room]['right'].visible = False

    def change_direction(self, room, direction):
        if self.current_state == 'xy':
            if room == 'x' and direction == 'right':
                self.set_xz()
            elif room == 'y' and direction == 'left':
                self.set_zy()

        if self.current_state == 'yx':
            if room == 'y' and direction == 'left':
                self.set_yz()
            elif room == 'x' and direction == 'right':
                self.set_zx()

        if self.current_state == 'yz':
            if room == 'y' and direction == 'right':
                self.set_yx()
            elif room == 'z' and direction == 'left':
                self.set_xz()

        if self.current_state == 'zy':
            if room == 'y' and direction == 'right':
                self.set_xy()
            elif room == 'z' and direction == 'left':
                self.set_zx()

        if self.current_state == 'zx':
            if room == 'z' and direction == 'right':
                self.set_zy()
            elif room == 'x' and direction == 'left':
                self.set_yx()

        if self.current_state == 'xz':
            if room == 'x' and direction == 'left':
                self.set_xy()
            elif room == 'z' and direction == 'right':
                self.set_yz()

        self.shift_meshes_to_pivot()

    def cancel_refractory(self):
        self.show_arrows()
        self.on_refractory_since = 0

    def init_YZ(self):
        self.do_activate_YZ = True

    def init_XZ(self):
        self.do_activate_XZ = True

    def do_feed(self):
        x, y = self.vr_scene.camera.position.x, self.vr_scene.camera.position.y
        self.event_bus.send({"feed": (str(time.time()), 'A', str(x), str(y))})

    def make_all_invisible(self):
        for mesh in self.vr_scene.root.children:
            mesh.visible = False

    def make_all_visible(self):
        for mesh in self.vr_scene.root.children:
            mesh.visible = True

    def stop_experiment(self):
        self.on_close()


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        raise ValueError("Please provide settings file as a first argument")

    with open(path) as f:
        config = json.load(f)

    # create common directory for tracking / logs
    subject = config["vr"]["subjects"]["animal"]
    config['ephys']["rec_dir"] = os.path.join(config['ephys']["rec_dir"], subject)

    if config["logging"]["enabled"]:
        session = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        where = os.path.join(config['save_to'], subject, session)

        if not os.path.exists(where):
            os.makedirs(where)

        config["videotracking"]["record"]["where"] = where
        config["logging"]["where"] = where

    controllers = {
        "scene": (RatcaveVFGSceneController, (vr_scene_builder, config['vr']["scene"], config['vr']["screen"])),
        "motive": (MotivePositionController, (config['vr']["scene"],)),
    }

    cfg = config["logging"]
    if cfg["enabled"]:
        args = (cfg["where"], ROOT, "Y", path)
        controllers["log"] = (RatcaveLoggingController, args)

    cfg = config["ephys"]
    if cfg["connect"]:
        args = (cfg["hostname"], cfg["port"], cfg["frequency"], cfg["rec_dir"])
        controllers["ephys"] = (AcquisitionSocketSyncController, args)

    cfg = config["videotracking"]
    if cfg["enabled"]:
        controllers["videotracking"] = (VideoController, (cfg,))

    cfg = config["infowindow"]
    if cfg["enabled"]:
        controllers["infowindow"] = (InfoWindowController, (cfg,))

    cfg = config["sound"]
    if cfg["enabled"]:
        controllers["sound"] = (SoundController, ('', 1))

    cfg = config["feeder"]
    if cfg["connect"]:
        args = (cfg["port"], cfg["baud"], cfg["verbose"])
        controllers["feeder"] = (FeederController, args)

    DispatcherMini.start_ratcave(controllers, config['event_bus'])
