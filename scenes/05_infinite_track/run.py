from __future__ import absolute_import

import sys
import os

import pyglet
pyglet.options['debug_gl'] = False

_ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(_ROOT)

import cinema

from cinema.alias import AliasBase
from cinema.dispatch import Dispatcher
from cinema.controller import WindowController, IOController, LoggingController, SoundController
from cinema.utils import MessageBus
from scene import SceneBuilder
from controller import LocalSceneController, LocalPositionController


# TODO plans:
# models: space (yaw, borders), scene (things, meshes), drawer (draw_360), hotspots etc.
# state: position, position delta, serial io etc.
# controllers: move scene controller to generic


class LinearTrackAlias(AliasBase):

    def __init__(self, dx, dy, x_virt, y_virt, data_in, data_out, stop_trigger, config):
        super(LinearTrackAlias, self).__init__(SceneBuilder, LocalSceneController, WindowController, x_virt, y_virt, stop_trigger, config)

        self.message_bus.register("move", self.scene_ctrl, self.scene_ctrl.check_center_crossing)
        self.message_bus.register("on_draw", self.scene_ctrl, self.scene_ctrl.update_hotspot_rotation)


class LinearTrack(object):

    def __init__(self, dx, dy, x_virt, y_virt, data_in, data_out, stop_trigger, config):

        # MODELS

        self.message_bus = MessageBus(["move", "collision", "hotspot", "key_pressed", "ext_data", "on_draw"])

        self.dx = dx                # X position change (sensors, keyboard)
        self.dy = dy                # Y position change (sensors, keyboard)
        self.data_in = data_in      # arduino input
        self.data_out = data_out    # arduino output
        self.x_virt = x_virt        # X position in VR
        self.y_virt = y_virt        # Y position in VR

        width = config["scene"]["width"]
        border = config["scene"]["border"]
        boundaries = {'square1': [(-4.0 + border, -width / 2 + border), (4.0 - border, width / 2 - border)]}
        self.scene = SceneBuilder.create_scene(x_virt.value, y_virt.value, config)

        # CONTROLLERS

        # logging
        self.logging_ctrl = LoggingController(config, _ROOT)

        # scene actions
        self.scene_ctrl = LocalSceneController(self.message_bus, self.scene, boundaries)

        # positioning
        self.position_ctrl = LocalPositionController(config["scene"]["yaw"], self.message_bus, dx, dy, x_virt, y_virt,
                                    config["sensors"]["gain"], config["keyboard"]["step"], self.scene_ctrl.is_inside)

        # window
        self.window_ctrl = WindowController(self.message_bus, self.scene, stop_trigger, config["screen"])

        # arduino communication
        self.io_ctrl = IOController(self.message_bus, data_in, data_out, config["reward"])

        # rewarding sounds
        scfg = config["sound"]
        self.sound_ctrl = SoundController(scfg["filepath"], scfg["count"], scfg["delay"])

        # bind controllers to events
        self.message_bus.register("ext_data", self.logging_ctrl, self.logging_ctrl.write_external)
        self.message_bus.register("collision", self.logging_ctrl, self.logging_ctrl.write_collision)
        self.message_bus.register("hotspot", self.logging_ctrl, self.logging_ctrl.write_hotspot)
        self.message_bus.register("hotspot", self.io_ctrl, self.io_ctrl.give_reward)
        self.message_bus.register("hotspot", self.sound_ctrl, self.sound_ctrl.on_reward)
        self.message_bus.register("move", self.logging_ctrl, self.logging_ctrl.write_position)
        self.message_bus.register("move", self.scene_ctrl, self.scene_ctrl.on_move)
        self.message_bus.register("move", self.scene_ctrl, self.scene_ctrl.check_hotspot)
        self.message_bus.register("move", self.scene_ctrl, self.scene_ctrl.check_center_crossing)
        self.message_bus.register("key_pressed", self.position_ctrl, self.position_ctrl.on_key_press)
        self.message_bus.register("on_draw", self.scene_ctrl, self.scene_ctrl.update_hotspot_rotation)
        # self.message_bus.register("ext_data", self, self.print_ext)

        pyglet.app.run()


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        print("WARNING! Using default settings..")
        path = os.path.join(os.path.dirname(cinema.__file__), "default.json")

    Dispatcher.from_file(LinearTrack, LinearTrackAlias, path).launch()
