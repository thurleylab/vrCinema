from cinema.controller import PositionController, SceneController
import numpy as np


class LocalPositionController(PositionController):

    # TODO: split scene into "space" (yaw, borders), "things" (meshes, hotspots)
    # TODO: and (ideally) "drawer" (rendering logic).

    def __init__(self, yaw, *args, **kwargs):
        self.yaw = yaw

        super(LocalPositionController, self).__init__(*args, **kwargs)

    def position_update(self, dt):
        """ Teleport """
        x = self.x_virt.value
        y = self.y_virt.value
        yaw = (self.yaw * np.pi) / 180.0

        R = np.sqrt(x ** 2 + y ** 2)

        if R > 3.0:
            # bringing to the horizontal orientation
            x1 = x * np.cos(-yaw) - y * np.sin(-yaw)
            y1 = x * np.sin(-yaw) + y * np.cos(-yaw)

            # then teleport is just a move along X axis
            x2 = x1 + 6.0 if x1 < 0 else x1 - 6.0
            y2 = y1

            # rotate back by yaw angle
            x3 = x2 * np.cos(yaw) - y2 * np.sin(yaw)
            y3 = x2 * np.sin(yaw) + y2 * np.cos(yaw)

            self.dx.value -= (x3 - x) / self.gain
            self.dy.value += (y3 - y) / self.gain

        super(LocalPositionController, self).position_update(dt)


class LocalSceneController(SceneController):

    def check_center_crossing(self, timestamp, x, y, delta):
        x0 = x - delta[0]
        y0 = y - delta[1]

        if self.to_real(x, y)[0] * self.to_real(x0, y0)[0] < 0 and np.sqrt(delta[0]**2 + delta[1]**2) > 3:
            for spot in self.scene.hotspots:
                spot.reset()

    def update_hotspot_rotation(self, *args):
        all_spots = [mesh for mesh in self.scene.meshes.children if mesh.name.lower().find('beacon') > -1]

        for spot in all_spots:
            spot.rotation.z += 0.2
            spot.rotation.y += 0.4
            spot.rotation.x += 0.6
