from cinema.utils import Beacon


class SceneBuilder(object):

    @staticmethod
    def create_scene(x0, y0, config):
        import ratcave as rc
        from cinema.cinema import SphericalScene

        width = config["scene"]["width"]
        reader = rc.WavefrontReader('infinite_track.obj')
        meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}

        walls_north = [mesh for key, mesh in meshes.items() if key.lower().find('wall_north') > -1]
        walls_south = [mesh for key, mesh in meshes.items() if key.lower().find('wall_south') > -1]

        for wall in walls_north:
            wall.position.y = width / 2
        for wall in walls_south:
            wall.position.y = -width / 2

        beacon = [mesh for key, mesh in meshes.items() if key.lower().find('beacon_middle') > -1][0]
        hotspots = [Beacon(1, 0.5, beacon)]
        hotspots[0].check_in()

        root = rc.EmptyEntity()
        for mesh in meshes.values():
            root.add_child(mesh)

        scene = SphericalScene(sides=config["scene"]["sides"], meshes=root, bgColor=(1. / 255., 1. / 255., 1. / 255.))

        scene.meshes.rotation.xyz = (0., 0., config["scene"]["yaw"])
        scene.camera.position.xyz = (x0, y0, 0.2)
        scene.camera.rotation.xyz = (90., 90., 0.)
        scene.camera.projection.z_far = 10.0

        scene.hotspots = hotspots

        return scene