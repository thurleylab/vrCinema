from __future__ import absolute_import

import sys
import os
import json
import datetime, time
import pyglet
import numpy as np
import ratcave as rc

ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(ROOT)

from threading import Timer
from cinema.dispatch import DispatcherMini
from cinema.controllers.acquisition import AcquisitionSocketSyncController
from cinema.controllers.video import VideoController
from cinema.controllers.serial import ArenaActuatorController
from controllers import MotivePositionController, LocalLoggingController, RatcaveSceneController


def vr_scene_builder(scene_cfg):
    reader = rc.WavefrontReader('180.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}
    root = rc.EmptyEntity()
    root.rotation = root.rotation.to_quaternion()

    for name, mesh in meshes.items():
        root.add_child(mesh)

    vr_scene = rc.Scene(meshes=root, bgColor=(0., 0., 0.))
    vr_scene.camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=90, aspect=1., z_near=.005, z_far=20.))
    vr_scene.root = root
    vr_scene.meshes_dict = meshes

    return vr_scene


class BaseSceneController(RatcaveSceneController):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, *args, **kwargs):
        super(BaseSceneController, self).__init__(scene_builder, scene_cfg, screen_cfg, *args, **kwargs)

    def run(self):
        # FIXME compensating virtual coordinates to Arena orientation
        self.scene.camera.position.x += .01

        self.make_1x_visible()

        # FIXME error with OpenGL rendering - requires position data first
        time.sleep(2)
        super(BaseSceneController, self).run()

    def update(self, dt):
        # FIXME move these algebra to the base class
        #super(BaseSceneController, self).update(dt)

        self.check_messages()

        self.arena.position.xyz = self.arena_body_position
        self.arena.rotation.xyzw = self.arena_body_quaternion
        self.vr_scene.root.position.xyz = self.arena.position.xyz

        # limit the boundaries - in VR coordinates
        b_x = [-0.37, 0.22]
        b_y = [-0.59, 1.02]

        # bring subject position from OptiTrack coordinates to VR coordinates
        s_pos = self.subject_position

        phi = np.pi * 4.3 / 180.0

        x = s_pos[0] * np.cos(phi) - s_pos[2] * np.sin(phi)
        z = s_pos[0] * np.sin(phi) + s_pos[2] * np.cos(phi)
        y = s_pos[1]

        new_pos = [x, y, z]  # subject position in VR coordinates

        # cut position change at the boundaries in the VR coordinate system
        if x < b_x[0]:
            new_pos[0] = b_x[0]
        if x > b_x[1]:
            new_pos[0] = b_x[1]
        if z < b_y[0]:
            new_pos[2] = b_y[0]
        if z > b_y[1]:
            new_pos[2] = b_y[1]

        # move back (possibly limited) subject position to the OptiTrack coordinate system
        x1 = new_pos[0] * np.cos(-phi) - new_pos[2] * np.sin(-phi)
        z1 = new_pos[0] * np.sin(-phi) + new_pos[2] * np.cos(-phi)
        y1 = new_pos[1]

        # assign the (possibly limited) subject position for the projection
        self.vr_scene.camera.position.xyz = [x1, y1, z1]
        self.scene.camera.uniforms['playerPos'] = [x1, y1, z1]

        x1, y1, z1, w1, = self.arena.rotation.xyzw
        self.vr_scene.root.rotation.xyzw = (x1, y1 + self.arena_y_comp, z1, w1)

        self.do_operations()
        
    # events

    def do_operations(self):
        pass  # used in particular implementations

    def make_1x_visible(self):
        for mesh in self.vr_scene.root.children:
            mesh.visible = False

        the_meshes = [mesh for key, mesh in self.vr_scene.meshes_dict.items() if key.lower().find('south') > -1]

        for mesh in the_meshes:
            mesh.visible = True

    def make_2x_visible(self):
        for mesh in self.vr_scene.root.children:
            mesh.visible = False

        the_meshes = [mesh for key, mesh in self.vr_scene.meshes_dict.items() if key.lower().find('north') > -1]

        for mesh in the_meshes:
            mesh.visible = True

    def make_all_invisible(self):
        for mesh in self.vr_scene.root.children:
            mesh.visible = False

    def start_arena_move(self):
        self.move_arena_flag = True

    def stop_arena_move(self):
        self.move_arena_flag = False

    def move_arena(self):
        args = (self.next_move_direction,)
        self.event_bus.send({"arena_move": args})

        self.next_move_direction = "F" if self.next_move_direction == "B" else "B"

    def stop_experiment(self):
        self.on_close()


class ArenaMovingBetweenScenes(BaseSceneController):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, *args, **kwargs):
        self.timers = []

        # schedule experiment events
        t1 = Timer(scene_cfg['timers'][0], self.make_all_invisible)
        t2 = Timer(scene_cfg['timers'][1], self.start_arena_move)
        t3 = Timer(scene_cfg['timers'][2], self.make_2x_visible)
        t4 = Timer(scene_cfg['timers'][3], self.stop_arena_move)
        t5 = Timer(scene_cfg['timers'][4], self.stop_experiment)

        self.timers = [t1, t2, t3, t4, t5]
        for timer in self.timers:
            timer.start()

        self.move_arena_flag = False
        self.move_freq = scene_cfg["move_freq"]
        self.next_move_direction = "F"
        self.timestamp = time.time()
        self.arena_y_comp = -0.055

        super(BaseSceneController, self).__init__(scene_builder, scene_cfg, screen_cfg, *args, **kwargs)

    def do_operations(self):
        if self.move_arena_flag:
            time_since_last = time.time() - self.timestamp
            if (1. / self.move_freq) < time_since_last:
                self.move_arena()
                self.timestamp = time.time()


class ArenaAndSceneMoving(BaseSceneController):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, *args, **kwargs):
        self.timers = []

        # schedule experiment end
        t1 = Timer(scene_cfg['timers'][0], self.stop_experiment)

        self.timers = [t1]
        for timer in self.timers:
            timer.start()

        # arena light ON / OFF cycle
        self.is_light_on = True
        self.light_on_time = scene_cfg['light_on_time']    # in seconds
        self.light_off_time = scene_cfg['light_off_time']  # in seconds
        self.timestamp = time.time()

        # arena moving direction
        self.next_move_direction = "F"

        self.arena_y_comp = -0.055

        super(BaseSceneController, self).__init__(scene_builder, scene_cfg, screen_cfg, *args, **kwargs)

    def do_operations(self):
        time_since_last = time.time() - self.timestamp
        if self.is_light_on and time_since_last > self.light_on_time:
            # turn lights OFF and move the arena (F or B)
            self.make_all_invisible()
            self.move_arena()
            self.is_light_on = False
            self.timestamp = time.time()

        time_since_last = time.time() - self.timestamp
        if not self.is_light_on and time_since_last > self.light_off_time:
            # change the 1x -> 2x arena and turn lights ON
            self.make_2x_visible() if self.next_move_direction == "B" else self.make_1x_visible()
            self.is_light_on = True
            self.timestamp = time.time()


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        raise ValueError("Please provide settings file as a first argument")

    with open(path) as f:
        config = json.load(f)

    # create common directory for tracking / logs
    if config["logging"]["enabled"]:
        subject = config["vr"]["subjects"]["animal"]
        session = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        where = os.path.join(config['save_to'], subject, session)

        if not os.path.exists(where):
            os.makedirs(where)

        config["videotracking"]["record"]["where"] = where
        config["logging"]["where"] = where
        config['ephys']["rec_dir"] = os.path.join(config['ephys']["rec_dir"], subject)

    scene_ctrl = ArenaMovingBetweenScenes if config['vr']["scene"]["mode"] == 1 else ArenaAndSceneMoving
    controllers = {
        "scene": (scene_ctrl, (vr_scene_builder, config['vr']["scene"], config['vr']["screen"])),
        "motive": (MotivePositionController, (config['vr']["scene"],)),
    }

    cfg = config["logging"]
    if cfg["enabled"]:
        args = (cfg["where"], ROOT, "180")
        controllers["log"] = (LocalLoggingController, args)

    cfg = config["ephys"]
    if cfg["connect"]:
        args = (cfg["hostname"], cfg["port"], cfg["frequency"], cfg["rec_dir"])
        controllers["ephys"] = (AcquisitionSocketSyncController, args)

    cfg = config["arduino"]
    if cfg["connect"]:
        args = (cfg["port"], cfg["baud"], cfg["verbose"])
        controllers["serial"] = (ArenaActuatorController, args)

    cfg = config["videotracking"]
    if cfg["enabled"]:
        controllers["videotracking"] = (VideoController, (cfg,))

    DispatcherMini.start_ratcave(controllers, config['event_bus'])




