from __future__ import absolute_import

import sys
import os
import json
import datetime, time
import ratcave as rc
import pyglet
import numpy as np

ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(ROOT)

from threading import Timer
from cinema.dispatch import DispatcherMini
from cinema.controllers.acquisition import AcquisitionSocketSyncController
from cinema.controllers.video import VideoController
from cinema.controllers.serial import ArenaActuatorController
from cinema.controllers.position import MotivePositionController, MotivePositionControllerWithSync
from cinema.controllers.log import RatcaveLoggingController
from cinema.controllers.scene import RatcaveSceneController
from cinema.controllers.trajectory import InfoWindowController
from cinema.controllers.serial import FeederController
from cinema.controllers.propixx import ProPixxController

from cinema.iserial import ArduinoDevice


def vr_scene_builder(scene_cfg):
    reader = rc.WavefrontReader('VShiftD.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}
    root = rc.EmptyEntity()
    root.rotation = root.rotation.to_quaternion()

    for name, mesh in meshes.items():
        root.add_child(mesh)

    vr_scene = rc.Scene(meshes=root, bgColor=(0., 0., 0.))
    vr_scene.camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=90, aspect=1., z_near=.005, z_far=20.))
    vr_scene.root = root
    vr_scene.meshes_dict = meshes

    return vr_scene


class RatcaveVShiftSceneController(RatcaveSceneController):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, *args, **kwargs):
        self.subject_distance = 0
        self.last_fed_at = 0
        self.is_dark = False
        self.feed_interval = scene_cfg['feed_interval']
        self.timestamp = time.time()
        self.experiment_start = time.time()
        self.arena_y_comp = 0 #-0.055
        self.vr_arena_lock = scene_cfg["is_locked"]  # lock VR to arena
        self.arena_pos_initial = []
        self.is_arena_moved = False
        self.move_direction = 0  # 0 - off, 1 - goes up to 1, -1 - goes down to 0
        self.move_start = time.time()
        self.arena_visual_shift = [0, 0, 0]  # shift if visual is on
        self.move_type = scene_cfg["move_type"]  # visual projection or physical arena
        self.move_duration = scene_cfg['move_duration']  # in seconds
        self.move_interval = scene_cfg["move_interval"]
        self.next_move_direction = "M" if self.move_type == "visual" else "D"  # move arena to initial position
        self.off_x = scene_cfg['off_x']
        self.off_z = scene_cfg['off_z']
        self.x_move_visual = scene_cfg['x_move_visual']
        self.z_move_visual = scene_cfg['z_move_visual']

        self.last_frame_time = time.time()

        self.timers = []

        # schedule experiment end
        t0 = Timer(2, self.move_arena)  # move to original (far or center) position
        t1 = Timer(scene_cfg['timers']['switch_VR_arena_lock'], self.switch_arena_lock)
        t2 = Timer(scene_cfg['timers']['stop_experiment'], self.stop_experiment)
        t3 = Timer(scene_cfg['timers']['turn_projection_off'], self.turn_projection_off)
        t4 = Timer(self.feed_interval, self.do_feed)

        self.timers = [t0, t1, t2, t3, t4]
        for timer in self.timers:
            timer.start()

        # -------------- integrate device controllers here ----------

        sys.path.append('c:\\users\\sirotalab\\miniconda3\\envs\\py35\\lib\\site-packages\\pypixxlib')
        from pypixxlib.propixx import PROPixx

        self.propixx_device = PROPixx()
        self.actuators = ArduinoDevice('COM14', 115200)
        self.feeder = ArduinoDevice('COM12', 9600)

        super(RatcaveVShiftSceneController, self).__init__(scene_builder, scene_cfg, screen_cfg, *args, **kwargs)

    def run(self):
        # FIXME compensating virtual coordinates to Arena orientation
        self.scene.camera.position.x += .02

        self.move_meshes(self.off_x, self.off_z)

        #self.event_bus.send({"turn_projection_on_off": (True,)})
        self.propixx_device.setLampLED(True)

        # FIXME error with OpenGL rendering - requires position data first
        time.sleep(2)
        super(RatcaveVShiftSceneController, self).run()

    def update(self, dt):
        self.check_messages()

        # transformation of "Arena" rigid body center point in the OptiTrack coordinate system to the virtual arena
        # (where the image is projected) center point defined in arena.obj coordinate system
        self.arena.position.xyz = np.array(self.arena_body_position) + np.array((0.02, 0, 0))
        self.arena.rotation.xyzw = np.array(self.arena_body_quaternion) #+ np.array((0, 0.012, 0, 0))

        if not self.arena_pos_initial:
            self.arena_pos_initial = list(self.arena.position.xyz)

        # Align the VR scene coordinate system with the virtual arena (arena.obj) coordinate system
        if self.vr_arena_lock:
            self.vr_scene.root.position.xyz = self.arena.position.xyz
        else:
            #shifted_coords = self.VR_to_optitrack(*self.arena_visual_shift)
            #print(self.arena_visual_shift, shifted_coords)
            self.vr_scene.root.position.xyz = np.array(self.arena_pos_initial) + np.array(self.arena_visual_shift)

        # lock to arena first few seconds to move to original position
        if time.time() - self.experiment_start < 10:
            self.vr_scene.root.position.xyz = np.array(self.arena.position.xyz) + np.array((0, 0.001, 0))
            self.arena_pos_initial = list(self.arena.position.xyz)

        x1, y1, z1, w1, = self.arena.rotation.xyzw
        self.vr_scene.root.rotation.xyzw = (x1, y1 + self.arena_y_comp, z1, w1)

        # assign subject position
        x1, y1, z1, = self.subject_position

        # assign the (possibly limited) subject position for the projection
        self.vr_scene.camera.position.xyz = [x1, y1, z1]
        self.scene.camera.uniforms['playerPos'] = [x1, y1, z1]
        self.vr_scene.light.position.xyz = (x1, self.vr_scene.light.position.xyz[1], z1)  # light moves with the animal

        # event - moving arena or projection
        time_since_last = time.time() - self.timestamp
        if self.move_interval < time_since_last:
            if self.move_type == 'physical':
                self.move_arena()
            else:
                Timer(0, self.move_projection).start()

            self.timestamp = time.time()

        # ------ print FPS -----------
        fps_update_diff = time.time() - self.last_frame_time
        #print(1.0/fps_update_diff)
        self.last_frame_time = time.time()

        #  ------- feed depending on animal distance ---------

        # aggregate subject distance
        # pos = self.vr_scene.camera.position
        # delta = np.sqrt((pos[0] - x1)**2 + (pos[2] - z1)**2)
        # if delta > 10**-3 and not pos[0] == 0:  # exclude initial position at 0
        #     self.subject_distance += delta
        # if self.subject_distance - self.last_fed_at > self.feed_interval:
        #     self.do_feed()
        #     self.last_fed_at = self.subject_distance

    # actions

    def turn_projection_off(self):
        #self.event_bus.send({"turn_projection_on_off": (False,)})

        self.propixx_device.setLampLED(False)
        self.is_dark = True

        # for vSHIFT away - reduce twice to get arena move in dark
        #self.move_interval /= 2

    def do_feed(self):
        x, y = self.vr_scene.camera.position.x, self.vr_scene.camera.position.y
        self.event_bus.send({"feed": (str(time.time()), 'A', str(x), str(y))})

        self.feeder.write('f'.encode())

        # schedule next feed if feeding by time
        Timer(self.feed_interval, self.do_feed).start()

    def move_meshes(self, x, z):
        # virtual (instant) move of the VR projection
        for mesh in [mesh for x, mesh in self.vr_scene.meshes_dict.items()]:
            mesh.position.x += x
            mesh.position.z += z

    def move_arena(self):
        # physical move of the arena
        self.event_bus.send({"arena_move": (self.next_move_direction,)})

        self.actuators.write(self.next_move_direction.encode())

        #self.next_move_direction = "F" if self.next_move_direction == "B" else "B"
        self.next_move_direction = "U" if self.next_move_direction == "D" else "D"

    def move_projection(self):
        #x_off = -0.03 # for 0.3m shift and -0.06 for 0.6m
        #z_off = -0.3  # for 0.3m shift and -0.6 for 0.6m
        x_off = self.x_move_visual
        z_off = self.z_move_visual

        self.is_arena_moved = not self.is_arena_moved
        self.move_direction = 1 if self.is_arena_moved else -1
        self.move_start = time.time()
        
        while not (self.move_direction == 0): 
            actual = time.time()
            progress = (actual - self.move_start) / self.move_duration  # between 0 and 1

            move_progress = progress if self.move_direction > 0 else 1 - progress

            if move_progress > 0:
                self.arena_visual_shift[0] = move_progress * x_off  # * self.move_direction
                self.arena_visual_shift[2] = move_progress * z_off  # * self.move_direction

            if progress > 1:
                self.move_direction = 0  # stop transition

            time.sleep(0.01)  # smaller pauses block main thread

        # move_direction = -1 if round(abs(self.arena_visual_shift[2]), 3) > 0 else 1
        # t_last = time.time()
        # 
        # # stop moving criteria
        # if move_direction == 1:     # from 0 to z_off
        #     should_move = lambda x: abs(x) < abs(z_off)
        # else:                       # from z_off to 0
        #     should_move = lambda x: x * move_direction > 0
        # 
        # while should_move(self.arena_visual_shift[2]):
        #     actual = time.time()
        #     move_progress = (actual - t_last) / self.move_duration
        # 
        #     if move_progress > 0:
        #         self.arena_visual_shift[0] += move_progress * x_off * move_direction
        #         self.arena_visual_shift[2] += move_progress * z_off * move_direction
        # 
        #         t_last = actual
        # 
        #     time.sleep(0.01)  # smaller pauses block main thread

    # events

    def switch_arena_lock(self):
        self.vr_arena_lock = not self.vr_arena_lock

    def stop_experiment(self):
        self.on_close()

    def on_shutdown(self, msg_data):
        pyglet.clock.unschedule(self.move_arena)

        # turn back projector LEDs ON
        self.propixx_device.setLampLED(True)

        # close feeder
        self.feeder.close()

        # move arena back to the center
        self.actuators.write("M".encode())
        self.actuators.close()

        super(RatcaveVShiftSceneController, self).on_shutdown(msg_data)


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        raise ValueError("Please provide settings file as a first argument")

    with open(path) as f:
        config = json.load(f)

    # create common directory for tracking / logs
    if config["logging"]["enabled"]:
        subject = config["vr"]["subjects"]["animal"]
        session = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        where = os.path.join(config['save_to'], subject, session)

        if not os.path.exists(where):
            os.makedirs(where)

        config["videotracking"]["record"]["where"] = where
        config["logging"]["where"] = where
        config['ephys']["rec_dir"] = os.path.join(config['ephys']["rec_dir"], subject)
        config['vr']["scene"]["rec_dir"] = config['ephys']["rec_dir"]

    controllers = {
        "scene": (RatcaveVShiftSceneController, (vr_scene_builder, config['vr']["scene"], config['vr']["screen"])),
        "motive": (MotivePositionControllerWithSync, (config['vr']["scene"],)),
    }

    cfg = config["logging"]
    if cfg["enabled"]:
        args = (cfg["where"], ROOT, "VShiftA", path)
        controllers["log"] = (RatcaveLoggingController, args)

    cfg = config["ephys"]
    if cfg["connect"]:
        args = (cfg["hostname"], cfg["port"], cfg["frequency"], cfg["rec_dir"])
        controllers["ephys"] = (AcquisitionSocketSyncController, args)

    cfg = config["arduino"]
    if cfg["connect"]:
        args = (cfg["port"], cfg["baud"], cfg["verbose"])
        controllers["serial"] = (ArenaActuatorController, args)

    cfg = config["videotracking"]
    if cfg["enabled"]:
        controllers["videotracking"] = (VideoController, (cfg,))

    cfg = config["infowindow"]
    if cfg["enabled"]:
        controllers["infowindow"] = (InfoWindowController, (cfg,))

    cfg = config["feeder"]
    if cfg["connect"]:
        args = (cfg["port"], cfg["baud"], cfg["verbose"])
        controllers["feeder"] = (FeederController, args)

    cfg = config["vr"]["projector"]
    if cfg["enabled"]:
        controllers["propixx"] = (ProPixxController, ())

    DispatcherMini.start_ratcave(controllers, config['event_bus'])




