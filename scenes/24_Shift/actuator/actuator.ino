#include "DualG2HighPowerMotorShield.h"

// Uncomment the version corresponding with the version of your shield.
DualG2HighPowerMotorShield24v14 md;
// DualG2HighPowerMotorShield18v18 md;
// DualG2HighPowerMotorShield24v18 md;
// DualG2HighPowerMotorShield18v22 md;

char incomingByte=0;    //flag required to check whether move upward(0) of downward(1)

int dir = 1;
int whole = 5000;
int half = 2500;
int loop_var=5;

void stopIfFault()
{
  if (md.getM1Fault())
  {
    md.disableDrivers();
	  delay(1);
    Serial.println("M1 fault");
    while (1);
  }
  if (md.getM2Fault())
  {
    md.disableDrivers();
	  delay(1);
    Serial.println("M2 fault");
    while (1);
  }
}


// direction -1 or 1
// duration in ms
void move(int direct, int duration, int lim)
{
  //int lim = 500;  // maximum speed from the demo script
  int val = 0;
  int rev = 1;
  md.enableDrivers();
  delay(1);  // The drivers require a maximum of 1ms to elapse when brought out of sleep mode. 
  for (int j = 1; j >= 0; j--)
  {
    for (int i = 0; i <= lim; i++)
    {
      md.setM1Speed(direct * (val + (rev * i)));
      md.setM2Speed(direct * -(val + (rev * i)));
     
      //stopIfFault();
      if (i%50 == 0)  // every 50 ms
      {
        Serial.print("M1 current: ");
        Serial.println(md.getM1CurrentMilliamps());
      }
      delay(duration / (400 * 2));
    }

    rev = -1;
    val = lim;
  }

  md.setM1Speed(0);
  md.setM2Speed(0);
  md.disableDrivers(); // Put the MOSFET drivers into sleep mode.
}


// direction -1 or 1
// duration in ms
// speed_lim speed to use
void move_non_sin(int direct, int duration, int speed_lim)
{
  md.enableDrivers();
  delay(1);  // The drivers require a maximum of 1ms to elapse when brought out of sleep mode. 

  md.setM1Speed(direct * speed_lim);  // TODO check direct for 1/-1, speed_lim
  md.setM2Speed(-direct * speed_lim);

  delay(duration);  // TODO check duration range

  md.setM1Speed(0);
  md.setM2Speed(0);
  md.disableDrivers(); // Put the MOSFET drivers into sleep mode.
}


void setup()
{
  Serial.begin(115200);
  Serial.println("Actuators ready for command :)");
  md.init();
  md.calibrateCurrentOffsets();

  delay(10);

  // Uncomment to flip a motor's direction:
  //md.flipM1(true);
  //md.flipM2(true);
}


void loop()

  {
  if (Serial.available() > 0) {

      incomingByte = Serial.read(); // read the incoming byte:
       if(incomingByte == 'F' || incomingByte == 'f')
      {
        Serial.println("Moving Up");   
        move(dir, whole, 400);
      }
      else if(incomingByte == 'B' || incomingByte == 'b')
      {
        Serial.println("Moving Down");           
        move(dir * -1, whole, 420);                  
      }
      else if(incomingByte == 'U' || incomingByte == 'u')
      {
        Serial.println("Moving Up");   
        move_non_sin(dir, whole, 350);
      }
      else if(incomingByte == 'D' || incomingByte == 'd')
      {
        Serial.println("Moving Down");
        move_non_sin(dir * -1, whole, 350);                  
      }
      else if(incomingByte == 'C' || incomingByte == 'c' || incomingByte == 'M' || incomingByte == 'm')
      {
        Serial.println("Centering");
        move(dir * -1, whole, 420);
        move(dir, half, 385);
      }
      else if(incomingByte == 'L' || incomingByte == 'l')
      {
        for(int i=0;i < loop_var ; i++)
        {
          move(dir * -1, whole, 420);

          delay(100);

          move(dir, whole, 400);
          
          delay(100);
           
        }  
      }
  }
}

