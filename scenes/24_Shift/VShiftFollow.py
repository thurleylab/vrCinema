from __future__ import absolute_import

import sys
import os
import json
import datetime, time
import numpy as np
import ratcave as rc

ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(ROOT)

from threading import Timer
from cinema.dispatch import DispatcherMini
from cinema.controllers.acquisition import AcquisitionSocketSyncController
from cinema.controllers.video import VideoController
from cinema.controllers.serial import ArenaActuatorController
from cinema.controllers.position import MotivePositionController
from cinema.controllers.log import RatcaveLoggingController
from cinema.controllers.scene import RatcaveSceneController


def vr_scene_builder(scene_cfg):
    reader = rc.WavefrontReader('VShiftA.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}
    root = rc.EmptyEntity()
    root.rotation = root.rotation.to_quaternion()

    for name, mesh in meshes.items():
        root.add_child(mesh)

    vr_scene = rc.Scene(meshes=root, bgColor=(0., 0., 0.))
    vr_scene.camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=90, aspect=1., z_near=.005, z_far=20.))
    vr_scene.root = root
    vr_scene.meshes_dict = meshes

    return vr_scene


class RatcaveVShiftSceneController(RatcaveSceneController):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, *args, **kwargs):
        self.timers = []

        # schedule experiment end
        t1 = Timer(scene_cfg['timers'][0], self.switch_follow)
        t2 = Timer(scene_cfg['timers'][1], self.stop_experiment)

        self.timers = [t1, t2]
        for timer in self.timers:
            timer.start()

        self.move_f = "u"
        self.move_b = "d"
        self.arena_y_comp = -0.055
        self.vr_arena_lock = False  # lock VR to arena
        self.arena_pos_initial = []
        self.off_x = -0.01    # scene_cfg['off_x']
        self.off_z = 0.01  # scene_cfg['off_z']

        self.move_queue = []  # a FIFO of arena moves
        self.last_move_at_time = time.time()
        self.z_at_level = 0.0
        self.level_step = 1.4 / 11.

        super(RatcaveVShiftSceneController, self).__init__(scene_builder, scene_cfg, screen_cfg, *args, **kwargs)

    def run(self):
        # FIXME compensating virtual coordinates to Arena orientation
        self.scene.camera.position.x += .01

        # FIXME error with OpenGL rendering - requires position data first
        time.sleep(2)
        super(RatcaveVShiftSceneController, self).run()

    def update(self, dt):
        self.check_messages()

        self.arena.position.xyz = self.arena_body_position
        self.arena.rotation.xyzw = self.arena_body_quaternion

        if not self.arena_pos_initial:
            self.arena_pos_initial = list(self.arena.position.xyz)
            self.arena_pos_initial[0] += self.off_x
            self.arena_pos_initial[2] += self.off_z

        if self.vr_arena_lock:
            self.vr_scene.root.position.xyz = self.arena.position.xyz
        else:
            self.vr_scene.root.position.xyz = self.arena_pos_initial

        # limit the boundaries - in VR coordinates
        b_x = [-0.37, 0.22]
        b_y = [-0.59, 1.02]

        # bring subject position from OptiTrack coordinates to VR coordinates
        s_pos = self.subject_position

        phi = np.pi * 4.3 / 180.0

        x = s_pos[0] * np.cos(phi) - s_pos[2] * np.sin(phi)
        z = s_pos[0] * np.sin(phi) + s_pos[2] * np.cos(phi)
        y = s_pos[1]

        new_pos = [x, y, z]  # subject position in VR coordinates

        # cut position change at the boundaries in the VR coordinate system
        if x < b_x[0]:
            new_pos[0] = b_x[0]
        if x > b_x[1]:
            new_pos[0] = b_x[1]
        if z < b_y[0]:
            new_pos[2] = b_y[0]
        if z > b_y[1]:
            new_pos[2] = b_y[1]

        # check if move needed
        if z - self.z_at_level > self.level_step / 2:  # move Backward
            if len(self.move_queue) > 0 and self.move_queue[-1] == self.move_f:  # remove F and move level back
                self.move_queue.pop(-1)
                self.z_at_level -= self.level_step

            if len(self.move_queue) < 11:  # not to exceed 10 moves waiting
                self.move_queue.append(self.move_b)
                self.z_at_level += self.level_step

        if z - self.z_at_level < -self.level_step / 2:  # move Forward
            if len(self.move_queue) > 0 and self.move_queue[-1] == self.move_b:  # remove B and move level back
                self.move_queue.pop(-1)
                self.z_at_level += self.level_step

            if len(self.move_queue) < 11:  # not to exceed 10 moves waiting
                self.move_queue.append(self.move_f)
                self.z_at_level -= self.level_step

        # move back (possibly limited) subject position to the OptiTrack coordinate system
        x1 = new_pos[0] * np.cos(-phi) - new_pos[2] * np.sin(-phi)
        z1 = new_pos[0] * np.sin(-phi) + new_pos[2] * np.cos(-phi)
        y1 = new_pos[1]

        # assign the (possibly limited) subject position for the projection
        self.vr_scene.camera.position.xyz = [x1, y1, z1]
        self.scene.camera.uniforms['playerPos'] = [x1, y1, z1]
        self.vr_scene.light.position.xyz = (x1, self.vr_scene.light.position.xyz[1], z1)  # light moves with the animal

        x1, y1, z1, w1, = self.arena.rotation.xyzw
        self.vr_scene.root.rotation.xyzw = (x1, y1 + self.arena_y_comp, z1, w1)

        # arena move
        self.move_arena()

        print(self.move_queue)

    # events

    def move_arena(self):
        if len(self.move_queue) > 0 and time.time() - self.last_move_at_time > 0.4:
            move_direction = self.move_queue.pop(0)
            self.event_bus.send({"arena_move": (move_direction,)})

            self.last_move_at_time = time.time()

    def switch_follow(self):
        self.move_f, self.move_b = self.move_b, self.move_f
        self.level_step = 1.9 / 11.

    def stop_experiment(self):
        self.on_close()


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        raise ValueError("Please provide settings file as a first argument")

    with open(path) as f:
        config = json.load(f)

    # create common directory for tracking / logs
    if config["logging"]["enabled"]:
        subject = config["vr"]["subjects"]["animal"]
        session = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        where = os.path.join(config['save_to'], subject, session)

        if not os.path.exists(where):
            os.makedirs(where)

        config["videotracking"]["record"]["where"] = where
        config["logging"]["where"] = where
        config['ephys']["rec_dir"] = os.path.join(config['ephys']["rec_dir"], subject)

    controllers = {
        "scene": (RatcaveVShiftSceneController, (vr_scene_builder, config['vr']["scene"], config['vr']["screen"])),
        "motive": (MotivePositionController, (config['vr']["scene"],)),
    }

    cfg = config["logging"]
    if cfg["enabled"]:
        args = (cfg["where"], ROOT, "VShiftA")
        controllers["log"] = (RatcaveLoggingController, args)

    cfg = config["ephys"]
    if cfg["connect"]:
        args = (cfg["hostname"], cfg["port"], cfg["frequency"], cfg["rec_dir"])
        controllers["ephys"] = (AcquisitionSocketSyncController, args)

    cfg = config["arduino"]
    if cfg["connect"]:
        args = (cfg["port"], cfg["baud"], cfg["verbose"])
        controllers["serial"] = (ArenaActuatorController, args)

    cfg = config["videotracking"]
    if cfg["enabled"]:
        controllers["videotracking"] = (VideoController, (cfg,))

    DispatcherMini.start_ratcave(controllers, config['event_bus'])




