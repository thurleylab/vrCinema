from __future__ import absolute_import

import sys
import os

from cinema.dispatch import DispatcherMini
from cinema.utils import Beacon, build_borders_from_squares
import cinema
import numpy as np
import random

from cinema.controllers.sensor import SensorController
from cinema.controllers.log import LoggingController
from cinema.controllers.position import PositionController
from cinema.controllers.scene import SceneController
from cinema.controllers.serial import SerialController
from cinema.controllers.video import VideoController
from cinema.controllers.acquisition import AcquisitionSyncController
from cinema.controllers.trajectory import TrajectoryWindowController
from cinema.controllers.sound import SoundController

ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(ROOT)


class SelfMovingBeacon(Beacon):

    def check_in(self):
        super(Beacon, self).check_in()

        x, y = 0, 0
        R = np.sqrt(self.x**2 + self.y**2)

        for i in range(200):  # just to be on the safe side
            val = 3 * np.random.rand(1, 1)[0][0] - 1.5
            x, y = random.choice([(1.8, val), (-1.8, val), (val, 1.8), (val, -1.8)])

            if np.sqrt((x - self.x)**2 + (y - self.y)**2) > 2.5:
                break

        self.beacon.position.xyz = (x, y, self.beacon.position[2])
        self.reset()


def scene_builder(vr_config):
    import pyglet
    pyglet.options['debug_gl'] = False

    import ratcave as rc
    from cinema.cinema import SphericalScene

    reader = rc.WavefrontReader('arena.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}

    beacon = [mesh for key, mesh in meshes.items() if key.lower().find('beacon') > -1][0]
    hotspots = [SelfMovingBeacon(1, 0.5, beacon)]
    hotspots[0].check_in()

    torus = [mesh for key, mesh in meshes.items() if key.find('Torus') > -1][0]

    def animate(dt):
        torus.rotation.xyz = np.array(torus.rotation.xyz) + np.array((0., 2., 0.))

    pyglet.clock.schedule(animate)

    root = rc.EmptyEntity()
    for mesh in meshes.values():
        root.add_child(mesh)

    scene = SphericalScene(sides=vr_config["sides"], meshes=root, bgColor=(1. / 255., 1. / 255., 1. / 255.))

    scene.meshes.rotation.xyz = (0., 0., vr_config["yaw"])
    scene.camera.position.xyz = (vr_config['x0'], vr_config['y0'], 0.2)
    scene.camera.rotation.xyz = (90., 0., 90.)
    scene.camera.projection.z_far = 10.0
    scene.hotspots = hotspots

    return scene


def build_is_inside(vr_config):
    border = vr_config["scene"]["border"]
    squares = {'square1': [(-2.0 + border, -2.0 + border), (2.0 - border, 2.0 - border)]}

    return build_borders_from_squares(vr_config["scene"]["yaw"], squares)


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        print("WARNING! Using default settings..")
        path = os.path.join(os.path.dirname(cinema.__file__), "default.json")

    settings = DispatcherMini.read_settings(path)
    is_inside = build_is_inside(settings["vr_cfg"])

    controllers = {
        "sensor": SensorController,
        "log": LoggingController,
        "position": PositionController,
        "scene": SceneController,
        "serial": SerialController,
        "video": VideoController,
        "acquisition": AcquisitionSyncController,
        "trajectory": TrajectoryWindowController,
        "sound": SoundController
    }

    DispatcherMini.start(scene_builder, is_inside, controllers, ROOT, settings)
