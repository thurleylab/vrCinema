import itertools
from segment import Segment


class Space(object):
    """
    Virtual space of infinite size. Composed of 9 similar Segments,
    where the subject is supposed to move inside the central one.
    """
    def __init__(self):
        self.segments = {}

        size = Segment.size
        idx_to_check = list(itertools.product([- size, 0, size], [- size, 0, size]))

        for x1, z1 in idx_to_check:
            if (x1, z1) not in self.keys():
                seg = Segment((x1, 0., z1))
                self[(x1, z1)] = seg

    def __len__(self):
        return len(self.segments)

    def __getitem__(self, key):
        return self.segments[key]

    def __setitem__(self, key, segment):
        self.segments[key] = segment

    def __delitem__(self, key):
        del self.segments[key]

    def __iter__(self):
        for i, seg in self.items():
            yield seg

    def __contains__(self, key):
        return key in self.segments.keys()

    def keys(self):
        return self.segments.keys()

    def items(self):
        for i, val in self.segments.items():
            yield (i, val)

    @property
    def unit_size(self):
        return Segment.size