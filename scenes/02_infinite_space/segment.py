import ratcave as rc

reader = rc.WavefrontReader(rc.resources.obj_primitives)


class Segment(rc.mesh.EmptyMesh):

    items  = ['Sphere', 'Torus', 'Cylinder']
    size   = 4  # should be int

    def __init__(self, position):
        super(Segment, self).__init__(position=position)

        # set the floor
        floor = reader.get_mesh('Plane', position=(0., -0.05, 0.), rotation=(-90, 0., 0.), scale=Segment.size/2)
        #floor.texture = rc.Texture.from_image(rc.resources.img_colorgrid)
        floor.texture = rc.Texture.from_image('img/sand.jpg')
        self.add_children(floor)

        # create reward place
        item = reader.get_mesh('Cylinder', position=(1., 0.2, -1.), scale=0.2)
        self.add_children(item)

    def get_reward_points(self, include_hidden=False):
        def is_reward_point(mesh):
            return hasattr(mesh, 'name') and mesh.name == 'Cylinder'

        if include_hidden:
            return filter(is_reward_point, self)
        return filter(lambda mesh: is_reward_point(mesh) and mesh.visible, self)

    def __str__(self):
        str_list = [str(e) for e in self.children]
        return "Segment with [" + ", ".join(str_list) + "]"

    def __repr__(self):
        return str(self)
