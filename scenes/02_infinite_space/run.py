from __future__ import absolute_import

import numpy as np
import os, sys

import cinema
import pyglet
from cinema.controller import PositionController, HotSpotController, WindowController, IOController, \
    LoggingController, KeyboardController
from cinema.utils import MessageBus, HotSpot
from cinema.dispatch import Dispatcher


PACKAGE_PARENT = '..'
_ROOT = os.path.abspath(os.path.dirname(__file__))
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))


def is_inside(x0, y0):
    return True


class ArduinoCtrl(IOController):

    def __init__(self, message_bus, data_in, data_out):
        super(ArduinoCtrl, self).__init__(message_bus, data_in, data_out)
        self.last_value = 0.0

        pyglet.clock.schedule_interval(self.check_external, 1./10)

    def check_external(self, *args):
        if self.data_in.value > 500.0 and self.last_value < 500:
            self.message_bus.dispatch("external", True)

        if self.data_in.value < 500.0 and self.last_value > 500:
            self.message_bus.dispatch("external", False)


class InfiniteSpace(object):

    def __init__(self, dx, dy, x_virt, y_virt, data_in, data_out, stop_trigger, config):

        # shared state
        self.message_bus = MessageBus(["move", "segment_end", "hotspot", "external", "key_pressed"])

        self.dx = dx                # X position change (sensors, keyboard)
        self.dy = dy                # Y position change (sensors, keyboard)
        self.data_in = data_in      # arduino input
        self.data_out = data_out    # arduino output
        self.x_virt = x_virt        # X position in VR
        self.y_virt = y_virt        # Y position in VR

        # logging
        self.logging_ctrl = LoggingController(config, _ROOT)

        # positioning
        self.position_ctrl = PositionController(self.message_bus, dx, dy, x_virt, y_virt, config["sensors"], is_inside)

        # window
        self.scene = self.build_scene(x_virt.value, y_virt.value, config["scene"])
        self.window_ctrl = WindowController(self.message_bus, self.scene, stop_trigger, config["screen"])

        # keyboard
        self.keyboard_ctrl = KeyboardController(self.message_bus, dx, dy, config["keyboard"]["step"])

        # hotspots
        self.hotspot_ctrl = HotSpotController(self.message_bus, self.build_hotspots())

        # arduino communication
        self.io_ctrl = ArduinoCtrl(self.message_bus, data_in, data_out)

        # bind controllers to events
        self.message_bus.register("hotspot", self.logging_ctrl, self.logging_ctrl.write_hotspot)
        self.message_bus.register("hotspot", self.io_ctrl, self.io_ctrl.give_reward)
        self.message_bus.register("hotspot", self, self.on_hotspot)
        self.message_bus.register("move", self.logging_ctrl, self.logging_ctrl.write_position)
        self.message_bus.register("move", self.hotspot_ctrl, self.hotspot_ctrl.check_hotspot)
        self.message_bus.register("move", self, self.check_segment_end)
        self.message_bus.register("move", self.window_ctrl, self.window_ctrl.on_move)
        self.message_bus.register("move", self, self.objects_move)
        self.message_bus.register("segment_end", self, self.make_visible)
        self.message_bus.register("external", self, self.on_external)
        self.message_bus.register("key_pressed", self.keyboard_ctrl, self.keyboard_ctrl.on_key_press)

        pyglet.app.run()

    def build_scene(self, x_init, y_init, scene_cfg):
        import ratcave as rc
        from cinema.cinema import SphericalScene
        from space import Space
        from panorama import Panorama

        reader = rc.WavefrontReader(rc.resources.obj_primitives)

        self.space = Space()  # proximal cues
        self.panorama = Panorama()  # distal cues

        # monkey indicator
        self.monkey = reader.get_mesh('Monkey', position=(0., 0.5, -1.), scale=0.2)
        self.monkey.visible = True

        scene = SphericalScene(meshes=[seg for seg in self.space] + [self.panorama] + [self.monkey],
                               bgColor=(38. / 255., 78. / 255., 119. / 255.))

        # starting scene position
        scene.root.rotation = (0., scene_cfg["yaw"], 0.)
        scene.root.position += np.array([0.0, 0.0, 0.0])

        scene.light.position = (x_init, 1.0, y_init)
        scene.camera.position = (x_init, 0., y_init)

        return scene

    def build_hotspots(self):
        hotspots = []
        for i, mesh in enumerate(self.space[(0, 0)].get_reward_points()):
            r = np.sqrt((mesh.min_xyz[0] - mesh.position[0])**2 + (mesh.min_xyz[2] - mesh.position[2])**2)
            hspot = HotSpot(i, mesh.position[0], mesh.position[2], r)
            hspot.related_mesh = mesh

            hotspots.append(hspot)

        return hotspots

    def check_segment_end(self, timestamp, x, y, delta):
        size = self.space.unit_size
        segment_end = False

        if x // (size/2) > 0:
            self.x_virt.value = x - size
            segment_end = True
        if x // (size/2) < -1:
            self.x_virt.value = x + size
            segment_end = True
        if y // (size/2) > 0:
            self.y_virt.value = y - size
            segment_end = True
        if y // (size/2) < -1:
            self.y_virt.value = y + size
            segment_end = True

        if segment_end:
            self.message_bus.dispatch("segment_end")

    def objects_move(self, timestamp, x, y, delta):
        pos_delta = np.array([delta[0], 0., delta[1]])

        self.panorama.position = np.array(self.panorama.position) + pos_delta
        self.monkey.position = np.array(self.monkey.position) + pos_delta

    def make_visible(self, *args):
        for mesh in self.space[(0, 0)].get_reward_points(include_hidden=True):
            mesh.visible = True

        self.hotspot_ctrl.reset_all()

    def on_external(self, is_high):
        self.monkey.visible = is_high

    def on_hotspot(self, timestamp, spot, x, y):
        print("on hotspot", x, y, spot.related_mesh.visible)
        spot.related_mesh.visible = False


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        print("WARNING! Using default settings..")
        path = os.path.join(os.path.dirname(cinema.__file__), "default.json")

    Dispatcher.from_file(InfiniteSpace, path).launch()
