import ratcave as rc
import numpy as np

from segment import Segment

reader = rc.WavefrontReader(rc.resources.obj_primitives)


class Panorama(rc.mesh.EmptyMesh):

    def __init__(self, position=(0., 0., 0.)):
        super(Panorama, self).__init__(position=position)

        distance = 0.75 * Segment.size

        positions = [(0., 0.5, -distance), (distance, 0.5, 0.), (0., 0.5, distance), (-distance, 0.5, 0.)]
        rotations = [(0., 0., -90.), (90., 0., -90.), (180., 0., -90.), (-90., 0., -90.)]

        for i, val in enumerate(zip(positions, rotations)):
            side = reader.get_mesh('Plane', position=val[0], rotation=val[1], scale=distance)
            side.texture = rc.Texture.from_image('img/beach360_1024x1024_' + str(i + 1) + '.jpg')
            self.add_children(side)

    @property
    def position(self):
        """xyz local position"""
        return self.x, self.y, self.z

    @position.setter
    def position(self, value):  # TODO strange behavior when updating self.position
        delta = np.array(value) - np.array(self.position)

        for side in self.children:
            side.position = np.array(side.position) + delta
