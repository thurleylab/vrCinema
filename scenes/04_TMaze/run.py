from __future__ import absolute_import

import sys
import os

import cinema
import pyglet
import time
import random
import numpy as np

from cinema.dispatch import Dispatcher
from cinema.controller import PositionController, HotSpotController, WindowController, IOController
from cinema.controller import LoggingController, KeyboardController
from cinema.utils import MessageBus, HotSpot, if_enabled
from cinema.idisk import DataWriter

_ROOT = os.path.abspath(os.path.dirname(__file__))


class ExtendedDataWriter(DataWriter):

    filenames = {
        "position": "positions.traj",
        "collision": "collisions.traj",
        "hotspot": "hotspots.traj",
        "results": "results.csv",
    }


class LocalLoggingController(LoggingController):

    @if_enabled
    def write_result(self, timestamp, location, result):
        self.writer.log("results", timestamp, location, result)


class LocalKeyboardController(KeyboardController):

    def on_key_press(self, symbol):
        if symbol == pyglet.window.key.SPACE:
            self.message_bus.dispatch("reset", time.time())
        else:
            super(LocalKeyboardController, self).on_key_press(symbol)


class LocalPositionController(PositionController):

    def reset_position(self, *args):
        delta = {
            'x': -self.x_virt.value,
            'y': -self.y_virt.value
        }

        self.x_virt.value = 0.0
        self.y_virt.value = 0.0

        move_args = (time.time(), self.x_virt.value, self.y_virt.value, (delta['x'], delta['y']))
        self.message_bus.dispatch("move", *move_args)


class Arena(object):

    bdr = 0.01

    def __init__(self, dx, dy, x_virt, y_virt, data_in, data_out, stop_trigger, config):

        self.reward_locations = []  # [1, 1, -1, -1, 1, ... ]
        self.reward_results = []  # [True, False, True, ... ]
        self.paths = {}

        self.yaw = config["scene"]["yaw"] * np.pi / 180.0
        self.border = config["scene"]["border"]
        self.space = {
            'square1': [(-0.7 + self.border, -1.0 + self.border), (0.7 - self.border, -0.8 - self.border)],
            'square2': [(-0.1 + self.border, -0.9 + self.border), (0.1 - self.border, 0.1 - self.border)]
        }
        spx1, spy1 = self.to_real(0.7, -0.9)
        spx2, spy2 = self.to_real(-0.7, -0.9)
        self.hotspots = [
            HotSpot(1, -spx1, spy1, 0.15),
            HotSpot(2, -spx2, spy2, 0.15)
        ]

        # shared state
        self.message_bus = MessageBus(["move", "collision", "hotspot", "key_pressed", "reset"])

        self.dx = dx                # X position change (sensors, keyboard)
        self.dy = dy                # Y position change (sensors, keyboard)
        self.data_in = data_in      # arduino input
        self.data_out = data_out    # arduino output
        self.x_virt = x_virt        # X position in VR
        self.y_virt = y_virt        # Y position in VR

        # logging
        self.logging_ctrl = LocalLoggingController(config, _ROOT, ExtendedDataWriter)

        # positioning
        self.position_ctrl = LocalPositionController(self.message_bus, dx, dy, x_virt, y_virt, config["sensors"], self.is_inside)

        # window
        scene = self.build_scene(x_virt.value, y_virt.value, config["scene"])
        self.arrange_reward_position()
        self.window_ctrl = WindowController(self.message_bus, scene, stop_trigger, config["screen"])

        # keyboard
        self.keyboard_ctrl = LocalKeyboardController(self.message_bus, dx, dy, config["keyboard"]["step"])

        # hotspots
        self.hotspot_ctrl = HotSpotController(self.message_bus, self.hotspots)

        # arduino communication
        self.io_ctrl = IOController(self.message_bus, data_in, data_out, config["reward"]["pulse"])

        # bind controllers to events
        self.message_bus.register("collision", self.logging_ctrl, self.logging_ctrl.write_collision)
        self.message_bus.register("hotspot", self.logging_ctrl, self.logging_ctrl.write_hotspot)
        self.message_bus.register("hotspot", self, self.process_result)
        self.message_bus.register("move", self.logging_ctrl, self.logging_ctrl.write_position)
        self.message_bus.register("move", self.window_ctrl, self.window_ctrl.on_move)
        self.message_bus.register("move", self.hotspot_ctrl, self.hotspot_ctrl.check_hotspot)
        self.message_bus.register("key_pressed", self.keyboard_ctrl, self.keyboard_ctrl.on_key_press)
        self.message_bus.register("reset", self.hotspot_ctrl, self.hotspot_ctrl.reset_all)
        self.message_bus.register("reset", self.position_ctrl, self.position_ctrl.reset_position)
        self.message_bus.register("reset", self, self.arrange_reward_position)

        pyglet.app.run()

    def to_real(self, x0, y0):
        x = x0 * np.cos(self.yaw) - y0 * np.sin(self.yaw)
        y = x0 * np.sin(self.yaw) + y0 * np.cos(self.yaw)
        return x, y

    def is_inside(self, x0, y0):
        is_in = lambda x, y, sq: sq[0][0] <= x <= sq[1][0] and sq[0][1] <= y <= sq[1][1]
        x1, y1 = self.to_real(x0, y0)

        for square in self.space.values():
            if is_in(x1, y1, square):
                return True

        return False

    def build_scene(self, x_init, y_init, scene_cfg):
        import ratcave as rc
        from cinema.cinema import SphericalScene

        reader = rc.WavefrontReader('maze')
        meshes = reader.get_meshes(reader.mesh_names)

        self.paths = {
            'path_flat': [mesh for key, mesh in meshes.items() if key.find('path_flat') > -1][0],
            'path_stripes': [mesh for key, mesh in meshes.items() if key.find('path_stripes') > -1][0]
        }

        scene = SphericalScene(sides=scene_cfg["sides"], meshes=meshes.values(), bgColor=(1. / 255., 1. / 255., 1. / 255.))

        # starting scene position
        scene.root.rotation = (0., scene_cfg["yaw"], 0.)

        scene.light.position = (x_init, 1.0, y_init)
        scene.camera.position = (x_init, 0.02, y_init)
        scene.camera.zFar = 10.0

        return scene

    def arrange_reward_position(self, *args):
        i = len(self.reward_results) - 1

        if i + 1 < 2:
            position = random.choice([1, -1])
        else:
            if self.reward_results[i - 1] and self.reward_results[i]:
                position = self.reward_locations[i - 1] * (-1)
            elif not self.reward_results[i - 1] and not self.reward_results[i]:
                position = self.reward_locations[i - 1]
            elif self.reward_results[i - 1] and not self.reward_results[i]:
                position = self.reward_locations[i] * (-1)
            else:
                position = self.reward_locations[i]

        if len(self.reward_locations) > len(self.reward_results):
            self.reward_locations[len(self.reward_locations) - 1] = position
        else:
            self.reward_locations.append(position)

        path_flat = self.paths['path_flat']
        path_stripes = self.paths['path_stripes']

        if path_stripes.position[0] * position < 0:  # need to flip
            path_stripes.position = np.array(path_stripes.position) * (-1., 1., 1.)
            path_flat.position = np.array(path_flat.position) * (-1., 1., 1.)

    def process_result(self, timestamp, spot, x, y):
        i = len(self.reward_locations) - 1

        # cross product (http://stackoverflow.com/questions/22668659/calculate-on-which-side-of-a-line-a-point-is)
        value = np.cos(self.yaw) * x - np.sin(self.yaw) * y

        if self.reward_locations[i] * value > 0:  # FIXME
            result = True
            self.io_ctrl.give_reward()  # FIXME communicate between controllers via message bus (shared state?)

        else:
            result = False

        if len(self.reward_locations) > len(self.reward_results):
            self.reward_results.append(result)
        else:
            self.reward_results[(len(self.reward_results) - 1)] = result

        self.logging_ctrl.write_result(time.time(), self.reward_locations[i], result)  # FIXME


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        print("WARNING! Using default settings..")
        path = os.path.join(os.path.dirname(cinema.__file__), "default.json")

    Dispatcher.from_file(Arena, path).launch()
