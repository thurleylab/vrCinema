from __future__ import absolute_import

import sys
import os
import json
import datetime, time
import ratcave as rc

ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(ROOT)

from threading import Timer
from cinema.dispatch import DispatcherMini
from cinema.controllers.acquisition import AcquisitionSocketSyncController
from cinema.controllers.video import VideoController
from cinema.controllers.position import MotivePositionController
from cinema.controllers.log import RatcaveLoggingController
from cinema.controllers.scene import RatcaveSceneController


def vr_scene_builder(scene_cfg):
    reader = rc.WavefrontReader('arena_uvgrid.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}
    root = rc.EmptyEntity()
    root.rotation = root.rotation.to_quaternion()

    texture = rc.Texture.from_image('uvgrid.png')
    for name, mesh in meshes.items():
        if name == 'floor_Floor':
            mesh.textures.append(texture)  # FIXME find out why the floor is not shown
        root.add_child(mesh)

    vr_scene = rc.Scene(meshes=root, bgColor=(0., 0., 0.))
    vr_scene.camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=90, aspect=1., z_near=.005, z_far=20.))
    vr_scene.root = root
    vr_scene.meshes_dict = meshes

    return vr_scene


class Ratcave180SceneController(RatcaveSceneController):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, *args, **kwargs):
        self.timers = []

        # schedule dark / arena rotation
        t1 = Timer(scene_cfg['timers'][0], self.make_all_invisible)
        t2 = Timer(scene_cfg['timers'][1], self.make_south_visible)
        t3 = Timer(scene_cfg['timers'][2], self.stop_experiment)

        self.timers = [t1, t2, t3]
        for timer in self.timers:
            timer.start()

        self.arena_y_comp = -0.055

        super(Ratcave180SceneController, self).__init__(scene_builder, scene_cfg, screen_cfg, *args, **kwargs)

    def run(self):
        # self.rot = self.vr_scene.root.rotation.to_euler()
        #
        # self.rot.y = 31.5  # what is this?
        # x1, y1, z1, w1, = self.rot.to_quaternion().xyzw
        # self.vr_scene.root.rotation.xyzw = (x1, y1, z1, w1)

        # for mesh in self.vr_scene.root.children:
        #     mesh.position.y -= 0.17
        #     mesh.rotation.y += 6.0

        self.make_south_invisible()

        # FIXME compensating virtual coordinates to Arena orientation
        self.scene.camera.position.x += .01

        # FIXME error with OpenGL rendering - requires position data first
        time.sleep(2)
        super(Ratcave180SceneController, self).run()

    def update(self, dt):
        super(Ratcave180SceneController, self).update(dt)

        x1, y1, z1, w1, = self.arena.rotation.xyzw
        self.vr_scene.root.rotation.xyzw = (x1, y1 + self.arena_y_comp, z1, w1)

        self.vr_scene.root.position.z += 0.0 # moving the arena back and forth by .1=10cm

    # events

    def make_all_invisible(self):
        for mesh in self.vr_scene.root.children:
            mesh.visible = False

    def make_south_invisible(self):
        the_meshes = [mesh for key, mesh in self.vr_scene.meshes_dict.items() if key.lower().find('south') > -1]

        for mesh in the_meshes:
            mesh.visible = False

    def make_south_visible(self):
        the_meshes = [mesh for key, mesh in self.vr_scene.meshes_dict.items() if key.lower().find('south') > -1]

        for mesh in the_meshes:
            mesh.visible = True

    def stop_experiment(self):
        self.on_close()


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        raise ValueError("Please provide settings file as a first argument")

    with open(path) as f:
        config = json.load(f)

    # create common directory for tracking / logs
    if config["logging"]["enabled"]:
        subject = config["vr"]["subjects"]["animal"]
        session = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        where = os.path.join(config['save_to'], subject, session)

        if not os.path.exists(where):
            os.makedirs(where)

        config["videotracking"]["record"]["where"] = where
        config["logging"]["where"] = where
        config['ephys']["rec_dir"] = os.path.join(config['ephys']["rec_dir"], subject)

    controllers = {
        "scene": (Ratcave180SceneController, (vr_scene_builder, config['vr']["scene"], config['vr']["screen"])),
        "motive": (MotivePositionController, (config['vr']["scene"],)),
    }

    cfg = config["logging"]
    if cfg["enabled"]:
        args = (cfg["where"], ROOT, "180", path)
        controllers["log"] = (RatcaveLoggingController, args)

    cfg = config["ephys"]
    if cfg["connect"]:
        args = (cfg["hostname"], cfg["port"], cfg["frequency"], cfg["rec_dir"])
        controllers["ephys"] = (AcquisitionSocketSyncController, args)

    cfg = config["videotracking"]
    if cfg["enabled"]:
        controllers["videotracking"] = (VideoController, (cfg,))

    DispatcherMini.start_ratcave(controllers, config['event_bus'])




