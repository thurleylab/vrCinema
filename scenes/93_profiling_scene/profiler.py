from __future__ import absolute_import

import pyglet
pyglet.options['debug_gl'] = False

import hotshot
import itertools
import ratcave as rc

import datetime
import numpy as np

from pyglet.window import key
from cinema.cinema import SphericalScene

"""
Run the script
 python profiler.py

Convert results into .kgrind

 sudo apt-get install kcachegrind-converters
 hotshot2calltree /tmp/prof.prof > /tmp/prof.out

To view use KCachegrind
https://kcachegrind.github.io

 sudo apt-get install kcachegrind
 kcachegrind

Refs:
https://habrahabr.ru/post/110537/
https://habrahabr.ru/company/mailru/blog/202832/
"""


reader = rc.WavefrontReader(rc.resources.obj_primitives)

meshes = []
size = 4
idx_to_check = list(itertools.product([- size, 0, size], [- size, 0, size]))

for x1, z1 in idx_to_check:
    pos = np.array((x1, 0., z1))

    # set the stage
    stage = reader.get_mesh('Plane', position=pos + np.array((0., -0.05, 0.)), rotation=(-90, 0., 0.), scale=2)
    stage.texture = rc.Texture.from_image(rc.resources.img_colorgrid)

    # create reward place
    item = reader.get_mesh('Cylinder', position=pos + np.array((1., 0.2, -1.)), scale=0.2)

    meshes += [stage, item]


#-------------------------------------------------------

display = pyglet.window.get_platform().get_default_display()
screens = display.get_screens()
window = pyglet.window.Window(vsync=False, resizable=True, fullscreen=False, screen=screens[1])


scene = SphericalScene(meshes=meshes, bgColor=(215./255., 215./255., 215./255.))
scene.light.position = (0., 0.5, 0.)


#-------------------------------------------------------------

prof = hotshot.Profile("/tmp/prof.prof")
prof.start()

ct = datetime.datetime.now()

for i in range(250):
    pyglet.clock.tick()

    nt = datetime.datetime.now()
    dt = nt - ct
    print(str(float(10**3)/((dt.microseconds + 1)/float(10**3))))
    ct = datetime.datetime.now()

    for window in pyglet.app.windows:
        window.switch_to()
        window.dispatch_events()
        scene.draw_360()
        window.flip()

prof.stop()
prof.close()
