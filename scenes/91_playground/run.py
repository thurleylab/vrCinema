import ratcave as rc
import pyglet
import os

from pyglet.window import key
from cinema.cinema import SphericalScene

path = os.path.abspath(os.path.dirname(__file__))


def create_scene():
    reader = rc.WavefrontReader(rc.resources.obj_primitives)

    stage = reader.get_mesh('Plane', position=(0, 0., 0.), rotation=(0., 0., 0.), scale=2.)
    stage.texture = rc.Texture.from_image(rc.resources.img_colorgrid)

    root = rc.EmptyEntity()
    root.add_child(stage)

    scene = SphericalScene(meshes=root, bgColor=(1. / 255., 1. / 255., 1. / 255.))
    scene.camera.position.xyz = (0., 0., 0.2)
    scene.camera.rotation.xyz = (90., 0., 90.)

    return scene


window = pyglet.window.Window(resizable=True, fullscreen=False)
scene = create_scene()


@window.event
def on_draw():
    scene.draw_360()


# movements in the maze
actions = {
    key.UP: lambda camera, step: setattr(camera.position, 'y', camera.position.y + step),
    key.DOWN: lambda camera, step: setattr(camera.position, 'y', camera.position.y - step),
    key.LEFT: lambda camera, step: setattr(camera.position, 'x', camera.position.x + step),
    key.RIGHT: lambda camera, step: setattr(camera.position, 'x', camera.position.x - step)
}


@window.event
def on_key_press(symbol, modifiers):
    # movements in space
    if symbol in actions.keys():
        actions[symbol](scene.camera, .2)


pyglet.app.run()
