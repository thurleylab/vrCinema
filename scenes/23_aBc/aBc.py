from __future__ import absolute_import

import sys
import os
import json
import datetime, time
import numpy as np
import ratcave as rc

ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(ROOT)

from threading import Timer
from cinema.dispatch import DispatcherMini
from cinema.controllers.acquisition import AcquisitionSocketSyncController
from cinema.controllers.video import VideoController
from cinema.controllers.position import MotivePositionController
from cinema.controllers.log import RatcaveLoggingController
from cinema.controllers.scene import RatcaveSceneController


def vr_scene_builder(scene_cfg):
    reader = rc.WavefrontReader('aBc.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}
    root = rc.EmptyEntity()
    root.rotation = root.rotation.to_quaternion()

    for name, mesh in meshes.items():
        root.add_child(mesh)

    vr_scene = rc.Scene(meshes=root, bgColor=(0., 0., 0.))
    vr_scene.camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=90, aspect=1., z_near=.005, z_far=20.))
    vr_scene.root = root
    vr_scene.meshes_dict = meshes

    return vr_scene


class RatcaveVFGSceneController(RatcaveSceneController):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, *args, **kwargs):
        self.timers = []

        t1 = Timer(scene_cfg['timers'][0], self.stop_experiment)

        self.timers = [t1]
        for timer in self.timers:
            timer.start()

        self.scene_cfg = scene_cfg
        self.arena_y_comp = -0.055

        super(RatcaveVFGSceneController, self).__init__(scene_builder, scene_cfg, screen_cfg, *args, **kwargs)

    def run(self):
        # FIXME compensating virtual coordinates to Arena orientation
        self.scene.camera.position.x += .01

        # FIXME error with OpenGL rendering - requires position data first
        time.sleep(2)
        super(RatcaveVFGSceneController, self).run()

    def update(self, dt):
        #super(RatcaveVFGSceneController, self).update(dt)

        self.check_messages()

        self.arena.position.xyz = self.arena_body_position
        self.arena.rotation.xyzw = self.arena_body_quaternion
        self.vr_scene.root.position.xyz = self.arena.position.xyz

        # limit the boundaries - in VR coordinates
        b_x = [-0.4, 0.21]
        b_y = [-0.59, 1.0]

        # bring subject position from OptiTrack coordinates to VR coordinates
        s_pos = self.subject_position

        phi = np.pi * 4.3 / 180.0

        x = s_pos[0] * np.cos(phi) - s_pos[2] * np.sin(phi)
        z = s_pos[0] * np.sin(phi) + s_pos[2] * np.cos(phi)
        y = s_pos[1]

        new_pos = [x, y, z]  # subject position in VR coordinates

        # cut position change at the boundaries in the VR coordinate system
        if x < b_x[0]:
            new_pos[0] = b_x[0]
        if x > b_x[1]:
            new_pos[0] = b_x[1]
        if z < b_y[0]:
            new_pos[2] = b_y[0]
        if z > b_y[1]:
            new_pos[2] = b_y[1]

        # move back (possibly limited) subject position to the OptiTrack coordinate system
        x1 = new_pos[0] * np.cos(-phi) - new_pos[2] * np.sin(-phi)
        z1 = new_pos[0] * np.sin(-phi) + new_pos[2] * np.cos(-phi)
        y1 = new_pos[1]

        # assign the (possibly limited) subject position for the projection
        self.vr_scene.camera.position.xyz = [x1, y1, z1]
        self.scene.camera.uniforms['playerPos'] = [x1, y1, z1]
        self.vr_scene.light.position.xyz = (x1, self.vr_scene.light.position.xyz[1], z1)

        # some strange offset value
        off_x = -0.02
        off_y = -0.25

        if self.scene_cfg['mode'] == 'middle':  # linear gain 0 on Z axis in the middle
            limits = [-0.008, 0.548]

            if z < limits[0]:
                VR_pos = [0, y, limits[0]]
            elif z > limits[1]:
                VR_pos = [0, y, limits[1]]
            else:
                VR_pos = [0, y, z]

        else:  # linear gain 1/3 on Z axis everywhere
            limits = [-0.55, 1.08]

            if z < limits[0]:
                VR_pos = [0, y, (limits[0] + 0.55) / 3]
            elif z > limits[1]:
                VR_pos = [0, y, (limits[1] + 0.55) / 3]
            else:
                VR_pos = [0, y, (z + 0.55) / 3]

        # move back subject X-null position to the OptiTrack coordinate system
        x2 = VR_pos[0] * np.cos(-phi) - VR_pos[2] * np.sin(-phi)
        z2 = VR_pos[0] * np.sin(-phi) + VR_pos[2] * np.cos(-phi)

        r_pos = self.vr_scene.root.position.xyz
        root_pos = (r_pos[0] + x2 + off_x, r_pos[1], r_pos[2] + z2 + off_y)
        self.vr_scene.root.position.xyz = root_pos
        self.vr_scene.light.position.xyz = (root_pos[0], self.vr_scene.light.position.xyz[1], root_pos[2])

        x1, y1, z1, w1, = self.arena.rotation.xyzw
        self.vr_scene.root.rotation.xyzw = (x1, y1 + self.arena_y_comp, z1, w1)

    # events

    def stop_experiment(self):
        self.on_close()


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        raise ValueError("Please provide settings file as a first argument")

    with open(path) as f:
        config = json.load(f)

    # create common directory for tracking / logs
    if config["logging"]["enabled"]:
        subject = config["vr"]["subjects"]["animal"]
        session = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        where = os.path.join(config['save_to'], subject, session)

        if not os.path.exists(where):
            os.makedirs(where)

        config["videotracking"]["record"]["where"] = where
        config["logging"]["where"] = where
        config['ephys']["rec_dir"] = os.path.join(config['ephys']["rec_dir"], subject)

    controllers = {
        "scene": (RatcaveVFGSceneController, (vr_scene_builder, config['vr']["scene"], config['vr']["screen"])),
        "motive": (MotivePositionController, (config['vr']["scene"],)),
    }

    cfg = config["logging"]
    if cfg["enabled"]:
        args = (cfg["where"], ROOT, "aBc")
        controllers["log"] = (RatcaveLoggingController, args)

    cfg = config["ephys"]
    if cfg["connect"]:
        args = (cfg["hostname"], cfg["port"], cfg["frequency"], cfg["rec_dir"])
        controllers["ephys"] = (AcquisitionSocketSyncController, args)

    cfg = config["videotracking"]
    if cfg["enabled"]:
        controllers["videotracking"] = (VideoController, (cfg,))

    DispatcherMini.start_ratcave(controllers, config['event_bus'])




