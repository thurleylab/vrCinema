from __future__ import absolute_import

import sys
import os
import json
import datetime, time
import pyglet
import numpy as np
import ratcave as rc


ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(ROOT)

from threading import Timer
from cinema.dispatch import DispatcherMini
from cinema.controllers.acquisition import AcquisitionSocketSyncController
from cinema.controllers.video import VideoController
from cinema.controllers.position import MotivePositionController, MotivePositionControllerWithSync
from cinema.controllers.log import RatcaveLoggingController
from cinema.controllers.scene import RatcaveSceneController
from cinema.controllers.serial import FeederController
from cinema.controllers.trajectory import InfoWindowController
from cinema.controllers.propixx import ProPixxController

from cinema.iserial import ArduinoDevice

# TODO refactor scene_builder to controllers


def vr_scene_builder(scene_cfg):
    reader = rc.WavefrontReader('VFG5.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}
    root = rc.EmptyEntity()
    root.rotation = root.rotation.to_quaternion()

    for name, mesh in meshes.items():
        root.add_child(mesh)

    vr_scene = rc.Scene(meshes=root, bgColor=(0., 0., 0.))
    vr_scene.camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=90, aspect=1., z_near=.005, z_far=20.))
    vr_scene.root = root
    vr_scene.meshes_dict = meshes

    return vr_scene


class RatcaveVFGSceneController(RatcaveSceneController):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, *args, **kwargs):
        self.scene_cfg = scene_cfg
        self.feed_interval = scene_cfg['feed_interval']
        self.timestamp = time.time()
        self.arena_y_comp = 0 # -0.055
        self.gain_progress = 0
        self.offset_progress = 0
        self.transition_direction = 0  # 0 - off, 1 - goes up to 1, -1 - goes down to 0
        self.transition_start = time.time()
        self.is_gain_active = scene_cfg['gain_initially_active']
        self.gain = scene_cfg['gain']
        self.off_x = scene_cfg['off_x']
        self.off_y = scene_cfg['off_y']
        self.pause = scene_cfg['pause']
        self.gain_switch_interval = scene_cfg["gain_switch_interval"]
        self.transition_duration = scene_cfg["transition_duration"]
        self.timers = []

        self.reward_delay = scene_cfg["reward_delay"]           # time an animal waits in the reward zone to get reward
        self.refractory_delay = scene_cfg["refractory_delay"]   # time in sec to let an animal eat the reward
        self.reward_radius = scene_cfg["reward_radius"]         # reward zone radius from the beacon center
        self.in_hotspot_since = 0
        self.on_refractory_since = 0

        self.last_frame_time = time.time()

        # schedule dark / arena rotation
        t1 = Timer(scene_cfg['timers']['stop_experiment'], self.stop_experiment)
        t2 = Timer(scene_cfg['timers']['turn_projection_off'], self.turn_projection_off)
        t3 = Timer(self.feed_interval, self.do_feed)

        self.timers = [t1, t2, t3]
        for timer in self.timers:
            timer.start()

        # -------------- integrate device controllers here ----------

        sys.path.append('c:\\users\\sirotalab\\miniconda3\\envs\\py35\\lib\\site-packages\\pypixxlib')
        from pypixxlib.propixx import PROPixx

        self.propixx_device = PROPixx()
        self.feeder = ArduinoDevice('COM12', 9600)

        super(RatcaveVFGSceneController, self).__init__(scene_builder, scene_cfg, screen_cfg, *args, **kwargs)

    def run(self):
        beacon_substr = self.scene_cfg['beacon_substr']
        self.active_beacon = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find(beacon_substr) > -1][0]

        # FIXME compensating virtual coordinates to Arena orientation
        self.scene.camera.position.x += .02

        # FIXME error with OpenGL rendering - requires position data first
        time.sleep(2)
        super(RatcaveVFGSceneController, self).run()

    def update(self, dt):
        self.check_messages()

        # transformation of "Arena" rigid body center point in the OptiTrack coordinate system to the virtual arena
        # (where the image is projected) center point defined in arena.obj coordinate system
        self.arena.position.xyz = np.array(self.arena_body_position) + np.array((0.12, 0, 0))
        self.arena.rotation.xyzw = np.array(self.arena_body_quaternion) #+ np.array((0, 90.012, 0, 0))

        # Align the VR scene coordinate system with the virtual arena (arena.obj) coordinate system
        self.vr_scene.root.position.xyz = self.arena.position.xyz
        x1, y1, z1, w1, = self.arena.rotation.xyzw
        self.vr_scene.root.rotation.xyzw = (x1, y1 + self.arena_y_comp, z1, w1)

        # limit the boundaries - in VR coordinates
        # b_x = [-0.37, 0.22]
        # b_y = [-0.59, 1.02]
        b_x = [-2.0, 2.0]  # basically no boundary
        b_y = [-3.0, 3.0] # basically no boundary

        # bring subject position from OptiTrack coordinates to VR coordinates
        s_pos = self.subject_position

        phi = np.pi * 0.0 / 180.0

        x = s_pos[0] * np.cos(phi) - s_pos[2] * np.sin(phi)
        z = s_pos[0] * np.sin(phi) + s_pos[2] * np.cos(phi)
        y = s_pos[1]

        new_pos = [x, y, z]  # subject position in VR coordinates

        # cut position change at the boundaries in the VR coordinate system
        if x < b_x[0]:
            new_pos[0] = b_x[0]
        if x > b_x[1]:
            new_pos[0] = b_x[1]
        if z < b_y[0]:
            new_pos[2] = b_y[0]
        if z > b_y[1]:
            new_pos[2] = b_y[1]

        # move back (possibly limited) subject position to the OptiTrack coordinate system
        x1 = new_pos[0] * np.cos(-phi) - new_pos[2] * np.sin(-phi)
        z1 = new_pos[0] * np.sin(-phi) + new_pos[2] * np.cos(-phi)
        y1 = new_pos[1]

        # assign the (possibly limited) subject position for the projection
        self.vr_scene.camera.position.xyz = [x1, y1, z1]
        self.scene.camera.uniforms['playerPos'] = [x1, y1, z1]
        self.vr_scene.light.position.xyz = (x1, self.vr_scene.light.position.xyz[1], z1)  # light moves with the animal

        # if self.is_gain_active:
        s_pos = self.vr_scene.camera.position.xyz
        r_pos = self.vr_scene.root.position.xyz

        koeff = self.gain
        self.vr_scene.root.position.x = r_pos[0] - self.gain_progress * 0.02 * s_pos[2] + self.offset_progress * self.off_x
        self.vr_scene.root.position.z = r_pos[2] - self.gain_progress * koeff * s_pos[2] + self.offset_progress * self.off_y + self.gain_progress * 0.03

        if self.transition_direction > 0:
            actual = time.time()
            progress = (actual - self.transition_start) / self.transition_duration

            self.gain_progress = progress
            self.offset_progress = progress

            self.timestamp = time.time()  # avoid calling start_transition while moving

            if progress > 1:
                self.transition_direction = 0  # stop transition

        elif self.transition_direction < 0:
            actual = time.time()
            progress = (actual - self.transition_start) / self.transition_duration

            self.gain_progress = 1 - progress
            self.offset_progress = 1 + progress

            self.timestamp = time.time()  # avoid calling start_transition while moving

            if progress > 1:
                self.transition_direction = 0  # stop transition

        # arena move
        time_since_last = time.time() - self.timestamp
        if self.gain_switch_interval < time_since_last:
            self.start_transition()

            print('started transition')

            self.timestamp = time.time()

        # # check hotspot
        # x_b = self.active_beacon.position.x + self.vr_scene.root.position.x
        # z_b = self.active_beacon.position.z + self.vr_scene.root.position.z
        # reward_distance = np.sqrt(np.square(new_pos[0] - x_b) + np.square(new_pos[2] - z_b))
        #
        # if reward_distance < self.reward_radius and not self.on_refractory_since:  # entered the hotspot
        #     self.record_hotspot()
        # elif self.in_hotspot_since and not self.on_refractory_since:  # exit the hotspot
        #     self.in_hotspot_since = 0
        #
        fps_update_diff = time.time() - self.last_frame_time
        #print(1.0/fps_update_diff)
        self.last_frame_time = time.time()

    # events

    def record_hotspot(self):
        # activate a hotspot
        if not self.in_hotspot_since:
            self.in_hotspot_since = time.time()
            # do something when animal entered reward zone

        # give reward and activate refractory # make a separate function
        if time.time() - self.in_hotspot_since > self.reward_delay:
            self.do_feed()

            # refractory period
            self.on_refractory_since = time.time()
            self.active_beacon.visible = False

            Timer(self.refractory_delay, self.cancel_refractory).start()

    def cancel_refractory(self):
        self.active_beacon.visible = True
        self.on_refractory_since = 0

    def start_transition(self):
        self.is_gain_active = not self.is_gain_active

        self.transition_direction = 1 if self.is_gain_active else -1
        self.transition_start = time.time()

    def do_feed(self):
        x, y = self.vr_scene.camera.position.x, self.vr_scene.camera.position.y
        self.event_bus.send({"feed": (str(time.time()), 'A', float(x), float(y))})

        self.feeder.write('f'.encode())

        # schedule next feed if feeding by time
        Timer(self.feed_interval, self.do_feed).start()

    def switch_gain(self):
        self.make_all_invisible()
        self.is_gain_active = not self.is_gain_active

        t1 = Timer(self.pause, self.make_all_visible)
        t1.start()

    def turn_projection_off(self):
        #self.event_bus.send({"turn_projection_on_off": (False,)})

        self.propixx_device.setLampLED(False)

    def make_all_invisible(self):
        for mesh in self.vr_scene.root.children:
            mesh.visible = False

    def make_all_visible(self):
        for mesh in self.vr_scene.root.children:
            mesh.visible = True

    def stop_experiment(self):
        # turn back projector LEDs ON
        self.propixx_device.setLampLED(True)

        # close feeder
        self.feeder.close()

        self.on_close()


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        raise ValueError("Please provide settings file as a first argument")

    with open(path) as f:
        config = json.load(f)

    # create common directory for tracking / logs
    subject = config["vr"]["subjects"]["animal"]
    config['ephys']["rec_dir"] = os.path.join(config['ephys']["rec_dir"], subject)
    config['vr']["scene"]["rec_dir"] = config['ephys']["rec_dir"]

    if config["logging"]["enabled"]:
        session = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        where = os.path.join(config['save_to'], subject, session)

        if not os.path.exists(where):
            os.makedirs(where)

        config["videotracking"]["record"]["where"] = where
        config["logging"]["where"] = where

    controllers = {
        "scene": (RatcaveVFGSceneController, (vr_scene_builder, config['vr']["scene"], config['vr']["screen"])),
        "motive": (MotivePositionControllerWithSync, (config['vr']["scene"],)),
    }

    cfg = config["logging"]
    if cfg["enabled"]:
        args = (cfg["where"], ROOT, "VFG", path)
        controllers["log"] = (RatcaveLoggingController, args)

    cfg = config["ephys"]
    if cfg["connect"]:
        args = (cfg["hostname"], cfg["port"], cfg["frequency"], cfg["rec_dir"])
        controllers["ephys"] = (AcquisitionSocketSyncController, args)

    cfg = config["videotracking"]
    if cfg["enabled"]:
        controllers["videotracking"] = (VideoController, (cfg,))

    cfg = config["infowindow"]
    if cfg["enabled"]:
        controllers["infowindow"] = (InfoWindowController, (cfg,))

    cfg = config["feeder"]
    if cfg["connect"]:
        args = (cfg["port"], cfg["baud"], cfg["verbose"])
        controllers["feeder"] = (FeederController, args)

    cfg = config["vr"]["projector"]
    if cfg["enabled"]:
        controllers["propixx"] = (ProPixxController, ())

    DispatcherMini.start_ratcave(controllers, config['event_bus'])
