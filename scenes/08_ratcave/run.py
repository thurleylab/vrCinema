from __future__ import absolute_import

import sys
import os
import time

from threading import Timer
from cinema.dispatch import DispatcherMini
from cinema.utils import Beacon
import cinema
import numpy as np
import random

from cinema.controllers.sensor import SensorController
from cinema.controllers.log import LoggingController
from cinema.controllers.position import PositionController
from cinema.controllers.scene import SceneController
from cinema.controllers.serial import SerialController
from cinema.controllers.video import VideoController
from cinema.controllers.acquisition import AcquisitionSyncController
from cinema.controllers.trajectory import TrajectoryWindowController
from cinema.controllers.sound import SoundController

ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(ROOT)


class LocalPositionController(PositionController):

    def run(self):
        self.event_bus.add_event("teleport")
        self.event_bus.register("teleport", self.schedule_teleport)

        super(LocalPositionController, self).run()

    def schedule_teleport(self, msg_data):
        no_teleport_zone_radius = 0.4
        beacon_x, beacon_y = msg_data

        x_set = [-0.4, -0.2, 0, 0.2, 0.4]
        y_set = [-0.2, 0, 0.2]

        x = 0.
        y = 0.

        while True:
            x = random.choice(x_set)
            y = random.choice(y_set)

            if np.sqrt((x - beacon_x) ** 2 + (y - beacon_y) ** 2) > no_teleport_zone_radius and self.is_inside(x, y):
                break

        """
        no_teleport_zone_radius = 0.4
        beacon_x, beacon_y = msg_data
        x = 0.
        y = 0.

        square = self.boundaries.values()[0]
        length_x = np.abs(square[1][0] - square[0][0])
        length_y = np.abs(square[1][1] - square[0][1])

        while True:
            val_x = np.random.rand(1)[0] * length_x - length_x / 2.
            val_y = np.random.rand(1)[0] * length_y - length_y / 2.
            x, y = random.choice([
                (square[0][0] + 0.05, val_y),
                (square[1][0] - 0.05, val_y),
                (val_x, square[0][1] + 0.05),
                (val_x, square[1][1] - 0.05)
            ])

            if np.sqrt((x - beacon_x) ** 2 + (y - beacon_y) ** 2) > no_teleport_zone_radius and self.is_inside(x, y):
                break
        """

        t = Timer(4.0, self.teleport, args=(x, y))
        t.start()

    def teleport(self, x, y):
        timestamp = time.time()

        delta_x = self.x_virt.value - x
        delta_y = self.y_virt.value - y
        self.x_virt.value = x
        self.y_virt.value = y

        move_args = (timestamp, self.x_virt.value, self.y_virt.value, self.yaw_virt.value, (delta_x, delta_y))
        self.event_bus.send({"move": move_args})


class LocalSceneController(SceneController):

    def run(self):
        self.event_bus.add_event("hotspot")
        self.event_bus.register("hotspot", self.do_teleport)

        super(LocalSceneController, self).run()

    def do_teleport(self, *args):
        t_off = Timer(1.0, self.hide_beacon)
        t_off.start()

        t_off = Timer(3.0, self.turn_light_off)
        t_off.start()

        t_on = Timer(5.0, self.turn_light_on)
        t_on.start()

        spot = self.scene.hotspots[0]
        self.event_bus.send({"teleport": (float(spot.x), float(spot.y))})

    def hide_beacon(self, *args):
        spot = self.scene.hotspots[0]
        spot.beacon.visible = False

    def turn_light_off(self, *args):
        for mesh in self.scene.meshes:
            mesh.visible = False

    def turn_light_on(self, *args):
        for mesh in self.scene.meshes:
            if not mesh.name == 'bar':
                mesh.visible = True

        spot = self.scene.hotspots[0]
        spot.reset()


class TargetBeacon(Beacon):

    def check_in(self):
        super(TargetBeacon, self).check_in()

        #t = Timer(5.0, self.reset)
        #t.start()

    def reset(self):
        super(TargetBeacon, self).reset()


def scene_builder(scene_config):
    import pyglet
    pyglet.options['debug_gl'] = False

    import ratcave as rc
    from cinema.cinema import SphericalScene

    reader = rc.WavefrontReader('arena.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}

    beacon = [mesh for key, mesh in meshes.items() if key.lower().find('beacon') > -1][0]
    beacon.name = 'beacon'
    beacon.position.xyz = (scene_config["beacon_x"], scene_config["beacon_y"], 0.28)
    hotspots = [TargetBeacon(1, 0.1, beacon)]

    # hide horizontal Cylinders
    bars = [mesh for key, mesh in meshes.items() if key.lower().find('bar') > -1]
    for bar in bars:
        bar.visible = False
        bar.name = 'bar'

    # TODO add moving objects
    #torus = [mesh for key, mesh in meshes.items() if key.find('Torus') > -1][0]

    #def animate(dt):
    #    torus.rotation.xyz = np.array(torus.rotation.xyz) + np.array((0., 2., 0.))

    #pyglet.clock.schedule(animate)

    root = rc.EmptyEntity()
    for mesh in meshes.values():
        root.add_child(mesh)

    scene = SphericalScene(sides=scene_config["sides"], meshes=root, bgColor=(1. / 255., 1. / 255., 1. / 255.))

    scene.meshes.rotation.xyz = (0., 0., scene_config["yaw"])
    scene.camera.position.xyz = (scene_config['x0'], scene_config['y0'], 0.1)
    scene.camera.rotation.xyz = (90., 0., 90.)
    scene.camera.projection.z_far = 10.0
    scene.hotspots = hotspots

    return scene


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        print("WARNING! Using default settings..")
        path = os.path.join(os.path.dirname(cinema.__file__), "default.json")

    settings = DispatcherMini.read_settings(path)

    controllers = {
        "sensor": SensorController,
        "log": LoggingController,
        "position": LocalPositionController,
        "scene": LocalSceneController,
        "serial": SerialController,
        "video": VideoController,
        "acquisition": AcquisitionSyncController,
        "trajectory": TrajectoryWindowController,
        #"sound": SoundController
        "sound": None
    }

    DispatcherMini.start(scene_builder, controllers, ROOT, settings)
