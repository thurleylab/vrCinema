from __future__ import absolute_import

import sys
import os

from threading import Timer
from cinema.dispatch import DispatcherMini
from cinema.utils import Beacon
import cinema


from cinema.controllers.scene import RatcaveSceneController
from pypixxlib import propixx

ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(ROOT)


class TargetBeacon(Beacon):

    def check_in(self):
        super(Beacon, self).check_in()

        t = Timer(5.0, self.reset)
        t.start()


def scene_builder(scene_config):
    import pyglet
    pyglet.options['debug_gl'] = False
    pyglet.options['debug_gl_trace'] = False

    import ratcave as rc

    """
    # Create Virtual Scenes
    vr_lighting = {
        'diffuse': cfg.VR_WALL_LIGHTING_DIFFUSE,
        'specular': cfg.VR_WALL_LIGHTING_SPECULAR,
        'ambient': cfg.VR_WALL_LIGHTING_AMBIENT,
        'flat_shading': cfg.VR_WALL_LIGHTING_FLAT_SHADING,
    }

    vr_arena = rc.WavefrontReader(cfg.ARENA_FILENAME).get_mesh('Arena')
    vr_arena.texture = cfg.ARENA_LIGHTING_TEXTURE
    for key, value in vr_lighting.iteritems():
        vr_arena.uniforms[key] = value #.update(vr_lighting)

    vr_wall = rc.WavefrontReader(cfg.VR_WALL_FILENAME).get_mesh(cfg.VR_WALL_MESHNAME)
    vr_wall.position.xyz = cfg.VR_WALL_X_OFFSET, cfg.VR_WALL_Y_OFFSET, 0.
    for key, value in vr_lighting.iteritems():
        vr_wall.uniforms[key] = value
    vr_wall.texture = cfg.VR_WALL_LIGHTING_TEXTURE
    """
    reader = rc.WavefrontReader('arena.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}

    beacon = [mesh for key, mesh in meshes.items() if key.lower().find('beacon') > -1][0]
    beacon.position.xyz = (scene_config["beacon_x"], scene_config["beacon_y"], 0.44)
    hotspots = [TargetBeacon(1, 0.1, beacon)]

    root = rc.EmptyEntity()
    for mesh in meshes.values():
        root.add_child(mesh)

    scene = rc.Scene(meshes=root, name="Arena")
    scene.hotspots = hotspots

    # Configure Ratcave App and register the virtual Scenes.
    #app.set_mouse_visible(cfg.MOUSE_CURSOR_VISIBLE)
    #app.arena.uniforms['flat_shading'] = cfg.ARENA_LIGHTING_FLAT_SHADING
    # app.arena.uniforms['diffuse'] = 0., 0., 0.
    #app.register_vr_scene(vr_scene_with_wall)
    #app.register_vr_scene(vr_scene_without_wall)

    return scene


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        print("WARNING! Using default settings..")
        path = os.path.join(os.path.dirname(cinema.__file__), "default.json")

    settings = DispatcherMini.read_settings(path)
    controllers = {
        "scene": RatcaveSceneController,
    }

    projector = propixx.PROPixx()
    projector.setSleepMode(not settings["projector"]["turned_on"])
    projector.setLampLED(settings["projector"]["led_on"])
    proj_brightness = settings["projector"]["led_intensity"]  # else "100.0"
    projector.setLedIntensity(proj_brightness)

    DispatcherMini.start(scene_builder, controllers, ROOT, settings)
