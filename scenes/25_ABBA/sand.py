import numpy as np
import time
#import matplotlib

#matplotlib.use('GTKAgg')
from matplotlib import pyplot as plt


def run(niter=1000, doblit=True):
    """
    Display the simulation using matplotlib, optionally using blit for speed
    """

    fig, ax = plt.subplots(1, 1)
    ax.set_aspect('equal')
    ax.set_xlim(0, 1)
    ax.set_ylim(0, 1)
    #ax.hold(True)

    x, y = np.random.rand(), np.random.rand()

    plt.show(False)
    plt.draw()

    # if doblit:
    #     # cache the background
    background = fig.canvas.copy_from_bbox(ax.bbox)

    points = ax.plot(x, y, 'o')[0]
    tic = time.time()

    fig.canvas.draw()

    for ii in range(niter):

        # update the xy data
        x, y = np.random.rand(), np.random.rand()
        points.set_data(x, y)

        # if doblit:
        # restore background
        fig.canvas.restore_region(background)

        # redraw just the points
        ax.draw_artist(points)

        # fill in the axes rectangle
        fig.canvas.blit(ax.bbox)

        # else:
        #     # redraw everything
        #     fig.canvas.draw()

    plt.close(fig)
    print("Blit = %s, average FPS: %.2f" % (str(doblit), niter / (time.time() - tic)))


def run_v2():
    fig = plt.figure()
    ax = fig.add_subplot(111)

    # some X and Y data
    x = np.arange(10000)
    y = np.random.randn(10000)

    li, = ax.plot(x, y)

    # draw and show it
    ax.relim()
    #ax.autoscale_view(True, True, True)
    fig.canvas.draw()
    plt.show(block=False)

    # loop to update the data
    while True:
        try:
            y[:-10] = y[10:]
            y[-10:] = np.random.randn(10)

            # set the new data
            li.set_ydata(y)

            fig.canvas.draw()

            time.sleep(0.1)
        except KeyboardInterrupt:
            break




if __name__ == '__main__':
    #run(doblit=False)
    run(doblit=True)
    # run_v2()