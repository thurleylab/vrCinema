from __future__ import absolute_import

import sys
import os
import json
import datetime, time
import numpy as np
import ratcave as rc

ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(ROOT)

from threading import Timer
from cinema.dispatch import DispatcherMini
from cinema.controllers.acquisition import AcquisitionSocketSyncController
from cinema.controllers.video import VideoController
from cinema.controllers.position import MotivePositionController, FakePositionController
from cinema.controllers.log import RatcaveLoggingController
from cinema.controllers.scene import RatcaveSceneController
from cinema.controllers.sound import DoubleSoundController
from cinema.controllers.serial import FeederController


def vr_scene_builder(scene_cfg):
    reader = rc.WavefrontReader('ABBA.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}
    root = rc.EmptyEntity()
    root.rotation = root.rotation.to_quaternion()

    for name, mesh in meshes.items():
        root.add_child(mesh)

    vr_scene = rc.Scene(meshes=root, bgColor=(0., 0., 0.))
    vr_scene.camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=90, aspect=1., z_near=.005, z_far=20.))
    vr_scene.root = root
    vr_scene.meshes_dict = meshes

    return vr_scene


# TODO refactor all mutable data to DTOs ?

# room_A
# room_B
# beacon_A
# beacon_B
# AmbientSoundPlayer


class RatcaveABSceneController(RatcaveSceneController):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, sound_cfg, *args, **kwargs):
        self.timers = []

        # schedule experiment end
        t1 = Timer(scene_cfg['timers'][0], self.switch_A_and_B)
        t2 = Timer(scene_cfg['timers'][1], self.stop_experiment)

        self.timers = [t1, t2]
        for timer in self.timers:
            timer.start()

        self.distance_since_sound = 0.0
        self.time_last_sound = time.time()
        self.timestamp = time.time()
        self.arena_y_comp = -0.055
        self.subject_last_position = (0, 0, 0)

        self.sound_cfg = sound_cfg

        self.click_delay = scene_cfg["click_delay"]             # time in sec between clicks when animal in reward zone
        self.reward_delay = scene_cfg["reward_delay"]           # time an animal waits in the reward zone to get reward
        self.refractory_delay = scene_cfg["refractory_delay"]   # time in sec to let an animal eat the reward
        self.reward_radius = scene_cfg["reward_radius"]         # reward zone radius from the beacon center
        self.AB_switched = scene_cfg["AB_switched"]             # whether A / B are switched initially

        self.is_south_active = False
        self.sound_on = False
        self.shift_required = self.AB_switched
        self.in_hotspot_since = 0
        self.on_refractory_since = 0
        self.time_last_reward_sound = time.time()

        super(RatcaveABSceneController, self).__init__(scene_builder, scene_cfg, screen_cfg, *args, **kwargs)

    def run(self):
        self.A_meshes = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('A_') > -1 and x.find('beacon') < 0]
        self.B_meshes = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('B_') > -1 and x.find('beacon') < 0]

        self.A_beacon = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('A_') > -1 and x.find('beacon') > -1][0]
        self.B_beacon = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('B_') > -1 and x.find('beacon') > -1][0]
        self.active_beacon = self.A_beacon
        self.B_beacon.visible = False
        self.refractory_meshes = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('white_floor') > -1]

        # FIXME compensating virtual coordinates to Arena orientation
        self.scene.camera.position.x += .02

        self.make_south_visible()

        # FIXME error with OpenGL rendering - requires position data first
        time.sleep(2)
        super(RatcaveABSceneController, self).run()

    def update(self, dt):
        #super(RatcaveABSceneController, self).update(dt)

        self.check_messages()

        # transformation of "Arena" rigid body center point in the OptiTrack coordinate system to the virtual arena
        # (where the image is projected) center point defined in arena.obj coordinate system
        self.arena.position.xyz = np.array(self.arena_body_position) + np.array((-0.005, -0.36, 0.09))
        self.arena.rotation.xyzw = np.array(self.arena_body_quaternion) + np.array((0, 0.012, 0, 0))

        self.vr_scene.root.position.xyz = self.arena.position.xyz

        # limit the boundaries - in VR coordinates
        # b_x = [-0.37, 0.22]
        # b_y = [-0.59, 1.02]
        b_x = [-1., 1.]
        b_y = [-1., 2.]

        # bring subject position from OptiTrack coordinates to VR coordinates
        s_pos = self.subject_position

        phi = np.pi * 4.3 / 180.0

        x = s_pos[0] * np.cos(phi) - s_pos[2] * np.sin(phi)
        z = s_pos[0] * np.sin(phi) + s_pos[2] * np.cos(phi)
        y = s_pos[1]

        new_pos = [x, y, z]  # subject position in VR coordinates

        # cut position change at the boundaries in the VR coordinate system
        if x < b_x[0]:
            new_pos[0] = b_x[0]
        if x > b_x[1]:
            new_pos[0] = b_x[1]
        if z < b_y[0]:
            new_pos[2] = b_y[0]
        if z > b_y[1]:
            new_pos[2] = b_y[1]

        if z < self.arena.position.z and self.is_south_active:
            self.make_north_visible()
        if z > self.arena.position.z and not self.is_south_active:
            self.make_south_visible()

        # move back (possibly limited) subject position to the OptiTrack coordinate system
        x1 = new_pos[0] * np.cos(-phi) - new_pos[2] * np.sin(-phi)
        z1 = new_pos[0] * np.sin(-phi) + new_pos[2] * np.cos(-phi)
        y1 = new_pos[1]

        # assign the (possibly limited) subject position for the projection
        self.vr_scene.camera.position.xyz = [x1, y1, z1]
        self.scene.camera.uniforms['playerPos'] = [x1, y1, z1]
        self.vr_scene.light.position.xyz = (x1, self.vr_scene.light.position.xyz[1], z1)

        x1, y1, z1, w1, = self.arena.rotation.xyzw
        self.vr_scene.root.rotation.xyzw = (x1, y1 + self.arena_y_comp, z1, w1)

        # update sound
        if self.sound_cfg["mode"] == 'position':  # distance dependent clicks-like sounds
            self.distance_since_sound += np.sqrt((self.subject_last_position[0] - new_pos[0])**2 + (self.subject_last_position[2] - new_pos[2])**2)
            if self.distance_since_sound > 0.03 and self.sound_on:
                self.distance_since_sound = 0.0

                # event to elicit sound
                self.event_bus.send({"do_play_sound": ()})

            self.subject_last_position = new_pos

        if self.sound_cfg["mode"] == 'time':  # clicks-like sounds at frequency
            current = time.time()
            if current - self.time_last_sound > 0.2 and self.sound_on:
                self.time_last_sound = current

                # event to elicit sound
                self.event_bus.send({"do_play_sound": ()})

        # check hotspot
        x_b = self.active_beacon.position.x + self.vr_scene.root.position.x
        z_b = self.active_beacon.position.z + self.vr_scene.root.position.z
        reward_distance = np.sqrt(np.square(new_pos[0] - x_b) + np.square(new_pos[2] - z_b))

        if reward_distance < self.reward_radius and self.active_beacon.visible == True and not self.on_refractory_since:
            self.record_hotspot()
        else:
            self.in_hotspot_since = 0

    # events

    def record_hotspot(self):
        # activate a hotspot
        if not self.in_hotspot_since:
            self.in_hotspot_since = time.time()

        # clicks while in a hotspot
        current = time.time()
        #if current - self.time_last_reward_sound > self.click_delay:
        if False:  # deactivate reward clicks for now
            self.time_last_reward_sound = current

            # event to elicit rewarding clicks
            spot_id = "A" if self.active_beacon == self.A_beacon else "B"
            x, y = self.vr_scene.camera.position.x, self.vr_scene.camera.position.y
            self.event_bus.send({"hotspot": (str(time.time()), spot_id, str(x), str(y))})

        # give reward and activate refractory # make a separate function
        if time.time() - self.in_hotspot_since > self.reward_delay:
            spot_id = "A" if self.active_beacon == self.A_beacon else "B"
            x, y = self.vr_scene.camera.position.x, self.vr_scene.camera.position.y
            self.event_bus.send({"feed": (str(time.time()), spot_id, str(x), str(y))})

            # refractory period
            self.on_refractory_since = time.time()
            self.in_hotspot_since = 0
            #self.make_refractory_visible()
            self.active_beacon.visible = False

            t1 = Timer(self.refractory_delay, self.switch_beacons)
            t1.start()

    def switch_beacons(self):
        # remove refractory
        self.on_refractory_since = 0

        # change beacons
        self.active_beacon = self.A_beacon if self.active_beacon == self.B_beacon else self.B_beacon

    def make_refractory_visible(self):
        for mesh in self.vr_scene.root.children:
            mesh.visible = False

        for mesh in self.refractory_meshes:
            mesh.visible = True

    def make_south_visible(self):
        self.is_south_active = True  # TODO use actual position of the A / B room to define is it south or north
        self.sound_on = self.is_south_active ^ self.AB_switched
        the_meshes = self.A_meshes if self.AB_switched else self.B_meshes

        if self.shift_required:
            self.shift_meshes()

        for mesh in self.vr_scene.root.children:
            mesh.visible = False

        for mesh in the_meshes:
            mesh.visible = True

        self.active_beacon.visible = (self.active_beacon == self.A_beacon if self.AB_switched else self.active_beacon == self.B_beacon) and not self.on_refractory_since

    def make_north_visible(self):
        self.is_south_active = False
        self.sound_on = self.is_south_active ^ self.AB_switched
        the_meshes = self.B_meshes if self.AB_switched else self.A_meshes

        if self.shift_required:
            self.shift_meshes()

        for mesh in self.vr_scene.root.children:
            mesh.visible = False

        for mesh in the_meshes:
            mesh.visible = True

        self.active_beacon.visible = (self.active_beacon == self.B_beacon if self.AB_switched else self.active_beacon == self.A_beacon) and not self.on_refractory_since

    def switch_A_and_B(self):
        self.AB_switched = not self.AB_switched
        self.shift_required = True

    def shift_meshes(self):
        move_direction = 1 if self.AB_switched else -1

        for mesh in self.A_meshes:
            mesh.position.z += move_direction * 0.8
        for mesh in self.B_meshes:
            mesh.position.z -= move_direction * 0.8

        self.A_beacon.position.z += move_direction * 0.8
        self.B_beacon.position.z -= move_direction * 0.8

        self.shift_required = False

    def stop_experiment(self):
        self.on_close()


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        raise ValueError("Please provide settings file as a first argument")

    with open(path) as f:
        config = json.load(f)

    # create common directory for tracking / logs
    if config["logging"]["enabled"]:
        subject = config["vr"]["subjects"]["animal"]
        session = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        where = os.path.join(config['save_to'], subject, session)

        if not os.path.exists(where):
            os.makedirs(where)

        config["videotracking"]["record"]["where"] = where
        config["logging"]["where"] = where
        config['ephys']["rec_dir"] = os.path.join(config['ephys']["rec_dir"], subject)

    controllers = {
        "scene": (RatcaveABSceneController, (vr_scene_builder, config['vr']["scene"], config['vr']["screen"], config["sound"])),
        "motive": (MotivePositionController, (config['vr']["scene"],)),
    }

    cfg = config["logging"]
    if cfg["enabled"]:
        args = (cfg["where"], ROOT, "ABBA", path)
        controllers["log"] = (RatcaveLoggingController, args)

    cfg = config["ephys"]
    if cfg["connect"]:
        args = (cfg["hostname"], cfg["port"], cfg["frequency"], cfg["rec_dir"])
        controllers["ephys"] = (AcquisitionSocketSyncController, args)

    cfg = config["videotracking"]
    if cfg["enabled"]:
        controllers["videotracking"] = (VideoController, (cfg,))

    cfg = config["sound"]
    if cfg["enabled"]:
        controllers["sound"] = (DoubleSoundController, ())

    cfg = config["feeder"]
    if cfg["connect"]:
        args = (cfg["port"], cfg["baud"], cfg["verbose"])
        controllers["feeder"] = (FeederController, args)

    DispatcherMini.start_ratcave(controllers, config['event_bus'])
