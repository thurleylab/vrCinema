from __future__ import absolute_import

import sys
import os
import json
import datetime
import time
#import pyglet
#pyglet.options['debug_gl'] = False
#pyglet.options['graphics_vbo'] = True

import numpy as np

ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(ROOT)

from threading import Timer
from cinema.dispatch import DispatcherMini
from cinema.controllers.acquisition import AcquisitionSocketSyncController
from cinema.controllers.position import MotivePositionController, FakePositionController
from cinema.controllers.log import RatcaveLoggingController
from cinema.controllers.scene import RatcaveSceneController
from cinema.controllers.serial import FeederController
from cinema.controllers.video import VideoController


def vr_scene_builder(scene_cfg):
    import ratcave as rc

    reader = rc.WavefrontReader('sphere.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}
    root = rc.EmptyEntity()
    root.rotation = root.rotation.to_quaternion()

    for name, mesh in meshes.items():
        root.add_child(mesh)

    vr_scene = rc.Scene(meshes=root, bgColor=(0., 0., 0.))
    vr_scene.camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=90, aspect=1., z_near=.005, z_far=20.))
    vr_scene.root = root
    vr_scene.meshes_dict = meshes

    return vr_scene


class RatcaveSphereSceneController(RatcaveSceneController):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, *args, **kwargs):
        self.feed_interval = scene_cfg['feed_interval']
        self.rot_gain = 0.0
        self.rotation_subj_before = 0.0
        self.rotation_subj_current = None
        self.rotation_world_current = 0.
        self.number_of_turns = 0

        self.rot = None

        self.diff_x = None
        self.diff_z = None

        self.gain = scene_cfg["gain"]
        self.transition_start = 0
        self.transition_from = 0
        self.transition_to = 0
        self.transition_duration = scene_cfg["transition_duration"]

        # schedule dark / arena rotation
        cfg = scene_cfg["gain"]["first"]
        t1 = Timer(cfg["after"], self.increase_gain, (cfg["from"], cfg["to"]))

        cfg = scene_cfg["gain"]["second"]
        t2 = Timer(cfg["after"], self.increase_gain, (cfg["from"], cfg["to"]))

        t3 = Timer(scene_cfg["stop_experiment"], self.stop_experiment)
        t4 = Timer(self.feed_interval, self.do_feed)

        self.timers = [t1, t2, t3, t4]
        for timer in self.timers:
            timer.start()

        super(RatcaveSphereSceneController, self).__init__(scene_builder, scene_cfg, screen_cfg, *args, **kwargs)

    def run(self):
        self.event_bus.register("position_subject", self.rotate_meshes)

        time.sleep(2)  # FIXME error with OpenGL rendering - requires position data first
        super(RatcaveSphereSceneController, self).run()

    def update(self, dt):
        self.check_messages()

        # original distance between arena and subject
        if self.diff_x is None:
            self.diff_x = self.arena_body_position[0] - self.subject_position[0]

        if self.diff_z is None:
            self.diff_z = self.arena_body_position[2] - self.subject_position[2]

        # lock projection to the physical arena
        # transformation of "Arena" rigid body center point in the OptiTrack coordinate system to the virtual arena
        # (where the image is projected) center point defined in arena.obj coordinate system
        self.arena.position.xyz = np.array(self.arena_body_position) + np.array((-0.005, -0.36, 0.09))
        self.arena.rotation.xyzw = np.array(self.arena_body_quaternion) + np.array((0, 0.012, 0, 0))

        # move VR together with the animal
        self.vr_scene.root.position.y = self.arena.position.y
        self.vr_scene.root.position.x = self.subject_position[0] #+ self.diff_x + 0.05
        self.vr_scene.root.position.z = self.subject_position[2] #+ self.diff_z + 0.1

        s_pos = self.subject_position
        self.vr_scene.camera.position.xyz = s_pos
        self.scene.camera.uniforms['playerPos'] = s_pos
        self.vr_scene.light.position.xyz = [s_pos[0], self.vr_scene.light.position[1], s_pos[2]]

        if self.transition_start > 0:
            actual = time.time()
            progress = (actual - self.transition_start) / self.transition_duration   # from 0 to 1

            self.rot_gain = self.transition_from + (self.transition_to - self.transition_from) * progress

            if progress > 1:
                self.transition_start = 0  # stop transition

    def rotate_meshes(self, msg_data):
        if self.rotation_subj_current is None:  # set the starting rotation angle
            self.rotation_subj_current = msg_data[5]

        if self.rotation_subj_current < -150 and msg_data[5] > 150:
            self.number_of_turns -= 1

        if self.rotation_subj_current > 150 and msg_data[5] < -150:
            self.number_of_turns += 1

        self.rotation_subj_before = self.rotation_subj_current
        self.rotation_subj_current = msg_data[5]

        # new_angle = (self.rot_gain * (self.rotation_subj_current + self.number_of_turns * 360)) % 360

        delta = (self.rotation_subj_current - self.rotation_subj_before)
        if abs(delta) > 100:
            delta += (1 if delta < 0 else -1) * 360

        self.rotation_world_current += self.rot_gain * delta
        angle = np.deg2rad(self.rotation_world_current)

        if self.rot is None:
            self.rot = self.vr_scene.root.rotation.to_euler()

        self.rot.y = angle - np.pi
        x1, y1, z1, w1, = self.rot.to_quaternion().xyzw
        self.vr_scene.root.rotation.xyzw = (x1, y1, z1, w1)

        # move meshes move meshes individually with the subject
        # delta_x = self.vr_scene.camera.position.x - self.arena.position.x
        # delta_z = self.vr_scene.camera.position.z - self.arena.position.z
        #
        # for msh in self.vr_scene.meshes.children:
        #     rotation_m = np.array([[np.cos(angle), np.sin(angle)], [-np.sin(angle), np.cos(angle)]])
        #     position_m = np.array([msh.original_position[0], msh.original_position[2]])
        #     rotated = np.dot(rotation_m, position_m)
        #
        #     msh.position.x = rotated[0] + delta_x
        #     msh.position.z = rotated[1] + delta_z

    # events

    def increase_gain(self, i_from, i_to):
        self.transition_start = time.time()
        self.transition_from = i_from
        self.transition_to = i_to
        self.number_of_turns = 0

        # self.rotation_subj_current = 0
        # self.rotation_world_current = 0
        # self.rot_gain = self.rot_gain_to_set
        # print("Rotational gain set to %10.2f" % self.rot_gain)

    def do_feed(self):
        x, y = self.vr_scene.camera.position.x, self.vr_scene.camera.position.y
        self.event_bus.send({"feed": (str(time.time()), 'A', str(x), str(y))})

        t1 = Timer(self.feed_interval, self.do_feed)
        t1.start()

    def stop_experiment(self):
        self.on_close()


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        raise ValueError("Please provide settings file as a first argument")

    with open(path) as f:
        config = json.load(f)

    # create common directory for tracking / logs
    subject = config["vr"]["subjects"]["animal"]
    config['ephys']["rec_dir"] = os.path.join(config['ephys']["rec_dir"], subject)

    # create common directory for tracking / logs
    if config["logging"]["enabled"]:
        session = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        where = os.path.join(config['save_to'], subject, session)

        if not os.path.exists(where):
            os.makedirs(where)

        config["logging"]["where"] = where
        config["videotracking"]["record"]["where"] = where

    controllers = {
        "scene": (RatcaveSphereSceneController, (vr_scene_builder, config['vr']["scene"], config['vr']["screen"])),
        "motive": (MotivePositionController, (config['vr']["scene"],)),
    }

    cfg = config["logging"]
    if cfg["enabled"]:
        args = (cfg["where"], ROOT, "sphere", path)
        controllers["log"] = (RatcaveLoggingController, args)

    cfg = config["ephys"]
    if cfg["connect"]:
        args = (cfg["hostname"], cfg["port"], cfg["frequency"], cfg["rec_dir"])
        controllers["ephys"] = (AcquisitionSocketSyncController, args)

    cfg = config["videotracking"]
    if cfg["enabled"]:
        controllers["videotracking"] = (VideoController, (cfg,))

    cfg = config["feeder"]
    if cfg["connect"]:
        args = (cfg["port"], cfg["baud"], cfg["verbose"])
        controllers["feeder"] = (FeederController, args)

    DispatcherMini.start_ratcave(controllers, config['event_bus'])
