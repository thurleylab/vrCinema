import numpy as np


# RatCAVE big scanned arena vertices: (t - top, m - middle; left-top, clockwise)
Am = np.array((-0.42215, 0.75110, -0.26307))
Bm = np.array((0.28282, 0.82723, -0.25226))
Cm = np.array((0.45875, -0.78484, -0.24964))
Dm = np.array((-0.25008, -0.85444, -0.26050))

#------

At = np.array((-0.69597, 0.97115, 0.38343))
Bt = np.array((0.50939, 1.09424, 0.38343))
Ct = np.array((0.73608, -0.99549, 0.38343))
Dt = np.array((-0.47045, -1.12095, 0.38343))

# top dimensions

ABt_phys = 1.18
CDt_phys = 1.18
BCt_phys = 2.09
DAt_phys = 2.09

ABt_scaling = ABt_phys / np.linalg.norm(At - Bt)
CDt_scaling = CDt_phys / np.linalg.norm(Ct - Dt)
BCt_scaling = BCt_phys / np.linalg.norm(Bt - Ct)
DAt_scaling = DAt_phys / np.linalg.norm(At - Dt)

ABm_phys = 0.718
CDm_phys = 0.718
BCm_phys = 1.63
DAm_phys = 1.63

ABm_scaling = ABm_phys / np.linalg.norm(Am - Bm)
CDm_scaling = CDm_phys / np.linalg.norm(Cm - Dm)
BCm_scaling = BCm_phys / np.linalg.norm(Bm - Cm)
DAm_scaling = DAm_phys / np.linalg.norm(Am - Dm)
