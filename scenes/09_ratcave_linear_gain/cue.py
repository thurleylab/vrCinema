from PIL import Image
import itertools


color_A = (0, 0, 255)
color_B = (0, 255, 0)

# checkerboard pattern: 3 (width) x 8 (height) squares, each square 200x200 px

sq_number_x = 3
sq_number_y = 8

sq_width = 200   # px
sq_height = 200  # px

start_color = color_B
current_color = start_color

img = Image.new('RGB', (3 * sq_width, 8 * sq_height), color_A)


def paint_square(n, m, color):
    coords = [(x, y) for x, y in itertools.product(range(sq_width), range(sq_height))]
    for x, y in coords:
        img.putpixel((n * sq_width + x, m * sq_height + y), color)


for sq_x in range(sq_number_x):
    start_color = color_B if start_color == color_A else color_A
    current_color = start_color

    for sq_y in range(sq_number_y):
        paint_square(sq_x, sq_y, current_color)
        current_color = color_B if current_color == color_A else color_A

img.save('checkerboard.png')
