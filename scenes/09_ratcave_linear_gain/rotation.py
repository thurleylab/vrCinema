from __future__ import absolute_import

import sys
import os
import json
import datetime
import time
import pyglet
pyglet.options['debug_gl'] = False
#pyglet.options['graphics_vbo'] = True

import ratcave as rc
import numpy as np

ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(ROOT)

from threading import Timer
from cinema.dispatch import DispatcherMini
from cinema.controllers.acquisition import AcquisitionSocketSyncController
from controllers import MotivePositionController, LocalLoggingController, RatcaveSceneController


def vr_scene_builder(scene_cfg):
    reader = rc.WavefrontReader('rotation.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}
    root = rc.EmptyEntity()
    root.rotation = root.rotation.to_quaternion()

    #texture = rc.Texture.from_image('uvgrid.png')
    for name, mesh in meshes.items():
        #if name == 'floor_Floor':
        #    mesh.textures.append(texture)  # FIXME find out why the floor is not shown
        root.add_child(mesh)

    vr_scene = rc.Scene(meshes=root, bgColor=(0., 0., 0.))
    vr_scene.camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=90, aspect=1., z_near=.005, z_far=20.))
    vr_scene.root = root

    return vr_scene


class RatcaveRotationSceneController(RatcaveSceneController):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, *args, **kwargs):
        self.rot_gain_to_set = scene_cfg["rot_gain"]
        self.rot_gain = 0.0
        self.rotation_subj_current = None
        self.rotation_world_current = 0.
        self.number_of_turns = 0

        self.rot = None

        self.diff_x = None
        self.diff_z = None

        # schedule dark / arena rotation
        t1 = Timer(scene_cfg["apply_gain_after"], self.increase_gain)
        t2 = Timer(scene_cfg["duration"], self.stop_experiment)

        self.timers = [t1, t2]
        for timer in self.timers:
            timer.start()

        super(RatcaveRotationSceneController, self).__init__(scene_builder, scene_cfg, screen_cfg, *args, **kwargs)

    def run(self):
        self.event_bus.register("position_subject", self.rotate_meshes)

        time.sleep(2)  # FIXME error with OpenGL rendering - requires position data first
        super(RatcaveRotationSceneController, self).run()

    def update(self, dt):
        #super(RatcaveRotationSceneController, self).update(dt)

        self.check_messages()

        # original distance between arena and subject
        if self.diff_x is None:
            self.diff_x = self.arena_body_position[0] - self.subject_position[0]

        if self.diff_z is None:
            self.diff_z = self.arena_body_position[2] - self.subject_position[2]

        self.arena.position.xyz = self.arena_body_position
        self.arena.rotation.xyzw = self.arena_body_quaternion

        self.vr_scene.root.position.y = self.arena.position.y
        self.vr_scene.root.position.x = self.subject_position[0] + self.diff_x
        self.vr_scene.root.position.z = self.subject_position[2] + self.diff_z

        self.vr_scene.camera.position.xyz = self.subject_position
        self.scene.camera.uniforms['playerPos'] = self.subject_position

        # to match current scanned arena better
        self.arena.position.y -= 0.15
        self.arena.rotation.y += 0.013

    def rotate_meshes(self, msg_data):
        if self.rotation_subj_current is None:  # set the starting rotation angle
            self.rotation_subj_current = msg_data[5]

        if self.rotation_subj_current < -150 and msg_data[5] > 150:
            self.number_of_turns -= 1

        if self.rotation_subj_current > 150 and msg_data[5] < -150:
            self.number_of_turns += 1

        self.rotation_subj_current = msg_data[5]
        self.rotation_world_current = (self.rot_gain * (self.rotation_subj_current + self.number_of_turns * 360)) % 360
        angle = np.deg2rad(self.rotation_world_current)

        if self.rot is None:
            self.rot = self.vr_scene.root.rotation.to_euler()

        self.rot.y = angle - np.pi
        x1, y1, z1, w1, = self.rot.to_quaternion().xyzw
        self.vr_scene.root.rotation.xyzw = (x1, y1, z1, w1)

        # move meshes move meshes individually with the subject
        # delta_x = self.vr_scene.camera.position.x - self.arena.position.x
        # delta_z = self.vr_scene.camera.position.z - self.arena.position.z
        #
        # for msh in self.vr_scene.meshes.children:
        #     rotation_m = np.array([[np.cos(angle), np.sin(angle)], [-np.sin(angle), np.cos(angle)]])
        #     position_m = np.array([msh.original_position[0], msh.original_position[2]])
        #     rotated = np.dot(rotation_m, position_m)
        #
        #     msh.position.x = rotated[0] + delta_x
        #     msh.position.z = rotated[1] + delta_z

    # events

    def increase_gain(self):
        self.number_of_turns = 0
        self.rotation_subj_current = 0
        self.rotation_world_current = 0
        self.rot_gain = self.rot_gain_to_set
        print("Rotational gain set to %10.2f" % self.rot_gain)

    def stop_experiment(self):
        self.on_close()


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        raise ValueError("Please provide settings file as a first argument")

    with open(path) as f:
        config = json.load(f)

    # create common directory for tracking / logs
    if config["logging"]["enabled"]:
        subject = config["vr"]["subjects"]["animal"]
        session = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        where = os.path.join(config['save_to'], subject, session)

        if not os.path.exists(where):
            os.makedirs(where)

        config["logging"]["where"] = where
        config['ephys']["rec_dir"] = os.path.join(config['ephys']["rec_dir"], subject)

    controllers = {
        "scene": (RatcaveRotationSceneController, (vr_scene_builder, config['vr']["scene"], config['vr']["screen"])),
        "motive": (MotivePositionController, (config['vr']["scene"],)),
    }

    cfg = config["logging"]
    if cfg["enabled"]:
        args = (cfg["where"], ROOT, "rotation")
        controllers["log"] = (LocalLoggingController, args)

    cfg = config["ephys"]
    if cfg["connect"]:
        args = (cfg["hostname"], cfg["port"], cfg["frequency"], cfg["rec_dir"])
        controllers["ephys"] = (AcquisitionSocketSyncController, args)

    DispatcherMini.start_ratcave(controllers, config['event_bus'])
