from __future__ import absolute_import

import sys
import os
import json
import datetime, time
import ratcave as rc

ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(ROOT)

from pyglet.window import key
from cinema.dispatch import DispatcherMini
from cinema.controllers.acquisition import AcquisitionSocketSyncController
from controllers import FakePositionController, LocalLoggingController, RatcaveSceneController, MotivePositionController


def vr_scene_builder(scene_cfg):
    #reader = rc.WavefrontReader('primitive.obj')
    reader = rc.WavefrontReader('VFG.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}
    root = rc.EmptyEntity()
    root.rotation = root.rotation.to_quaternion()

    texture = rc.Texture.from_image('uvgrid.png')
    for name, mesh in meshes.items():
        if name == 'floor_Floor':
            mesh.textures.append(texture)  # FIXME find out why the floor is not shown
        root.add_child(mesh)

    vr_scene = rc.Scene(meshes=root, bgColor=(0., 0., 0.))
    vr_scene.camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=90, aspect=1., z_near=.005, z_far=20.))
    vr_scene.root = root

    return vr_scene


class PrimitiveSceneController(RatcaveSceneController):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, *args, **kwargs):
        self.arena_delta = [0, 0, 0, 0, 0, 0, 0]  # rotating arena to match rigid body
        self.world_delta = [0, 0, 0, 0, 0, 0, 0]

        self.current = 'world'  # 'arena' or 'world'
        self.step_trs = 0.01
        self.step_rot = 0.1

        super(PrimitiveSceneController, self).__init__(scene_builder, scene_cfg, screen_cfg, *args, **kwargs)

    def run(self):
        # FIXME compensating virtual coordinates to Arena orientation
        self.scene.camera.position.x += .01

        for mesh in self.vr_scene.root.children:
            mesh.original_position = mesh.position.xyz
            mesh.original_rotation = mesh.rotation.xyz

        self.window.event(self.on_key_press)

        time.sleep(2)  # FIXME error with OpenGL rendering - requires position data first
        super(PrimitiveSceneController, self).run()

    def update(self, dt):
        super(PrimitiveSceneController, self).update(dt)

        self.arena.position.xyz = [x + y for x, y in zip(self.arena_delta[:3], self.arena.position.xyz)]
        self.arena.rotation.xyzw = [x + y for x, y in zip(self.arena_delta[3:], self.arena.rotation.xyzw)]

        for mesh in self.vr_scene.root.children:
            mesh.position.xyz = [x + y for x, y in zip(self.world_delta[:3], mesh.original_position)]
            mesh.rotation.xyz = [x + y for x, y in zip(self.world_delta[3:6], mesh.original_rotation)]

        x1, y1, z1, w1, = self.arena.rotation.xyzw
        # FIXME 0.55 compensating virtual coordinates to Arena orientation
        self.vr_scene.root.rotation.xyzw = (x1, y1 - 0.055, z1, w1)

    # events

    def on_key_press(self, symbol, modifiers):
        self.event_bus.send({"key_press": [symbol]})

        print(symbol == key.NUM_4)

        if symbol == key.A:
            self.current = 'arena'
            print('Adjusting: ARENA')

        if symbol == key.W:
            self.current = 'world'
            print('Adjusting: WORLD')

        delta = [0, 0, 0, 0, 0, 0, 0]
        if symbol == key.LEFT:
            delta[0] += self.step_trs
        if symbol == key.RIGHT:
            delta[0] -= self.step_trs
        if symbol == key.UP:
            delta[2] += self.step_trs
        if symbol == key.DOWN:
            delta[2] -= self.step_trs
        if symbol == key.PAGEUP:
            delta[1] += self.step_trs
        if symbol == key.PAGEDOWN:
            delta[1] -= self.step_trs

        if symbol == key.NUM_4:
            delta[3] += self.step_rot
        if symbol == key.NUM_6:
            delta[3] -= self.step_rot
        if symbol == key.NUM_8:
            delta[4] += self.step_rot
        if symbol == key.NUM_2:
            delta[4] -= self.step_rot
        if symbol == key.NUM_7:
            delta[5] += self.step_rot
        if symbol == key.NUM_9:
            delta[5] -= self.step_rot

        self.world_delta = [x + y for x, y in zip(self.world_delta, delta)]
        self.arena_delta = [x + y for x, y in zip(self.arena_delta, delta)]

        # if self.current == 'world':
        #     self.world_delta = [x + y for x, y in zip(self.world_delta, delta)]
        #
        # if self.current == 'arena':
        #     self.arena_delta = [x + y for x, y in zip(self.arena_delta, delta)]

    def stop_experiment(self):
        self.on_close()


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        raise ValueError("Please provide settings file as a first argument")

    with open(path) as f:
        config = json.load(f)

    # create common directory for tracking / logs
    if config["logging"]["enabled"]:
        subject = config["vr"]["subjects"]["animal"]
        session = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        where = os.path.join(config['save_to'], subject, session)

        if not os.path.exists(where):
            os.makedirs(where)

        config["logging"]["where"] = where
        config['ephys']["rec_dir"] = os.path.join(config['ephys']["rec_dir"], subject)

    controllers = {
        "scene": (PrimitiveSceneController, (vr_scene_builder, config['vr']["scene"], config['vr']["screen"])),
        #"motive": (FakePositionController, ()),
        "motive": (MotivePositionController, (config['vr']["scene"],))
    }

    cfg = config["logging"]
    if cfg["enabled"]:
        args = (cfg["where"], ROOT, "180")
        controllers["log"] = (LocalLoggingController, args)

    cfg = config["ephys"]
    if cfg["connect"]:
        args = (cfg["hostname"], cfg["port"], cfg["frequency"], cfg["rec_dir"])
        controllers["ephys"] = (AcquisitionSocketSyncController, args)

    DispatcherMini.start_ratcave(controllers, config['event_bus'])




