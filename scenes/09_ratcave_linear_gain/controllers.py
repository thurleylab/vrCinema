import time
import os
import ratcave as rc
import pyglet.gl as gl
import pyglet

from cinema.controllers.log import LoggingController
from cinema.utils import EventBusClient


class MotivePositionController(object):

    update_frequency = 100  # Hz

    def __init__(self, scene_cfg):
        self.event_bus = EventBusClient(events=["shutdown"])
        self.event_bus.register("shutdown", self.on_shutdown)

        # import only for Motive controller
        from natnetclient import NatClient

        self.client = NatClient()
        self.arena_body = self.client.rigid_bodies['Arena']
        self.subject = self.client.rigid_bodies[scene_cfg['body']]

        self.keep_alive = True
        self.last_loop_time = time.time()

        print("OK (%s): motive position controller" % str(os.getpid()))

        self.run()

    def run(self):
        timestamp = time.time()

        while self.keep_alive:
            self.event_bus.check_messages()  # listening on events
            self.broadcast_position()        # broadcast motive position info

            time_since_last = time.time() - timestamp
            if (1. / MotivePositionController.update_frequency) > time_since_last:
                time.sleep((1. / MotivePositionController.update_frequency) - time_since_last)
            timestamp = time.time()

            # uncomment to print the real update frequency
            # print(1. / (timestamp - self.last_loop_time))
            self.last_loop_time = timestamp

    def broadcast_position(self):
        actual = time.time()

        x, y, z = self.subject.position
        xr, yr, zr = self.subject.rotation
        self.event_bus.send({"position_subject": (actual, x, y, z, xr, yr, zr)})

        x, y, z = self.arena_body.position
        xr, yr, zr, w = self.arena_body.quaternion
        self.event_bus.send({"position_arena": (actual, x, y, z, xr, yr, zr, w)})

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: motive position controller")
        self.keep_alive = False


class FakePositionController(object):

    update_frequency = 100  # Hz
    subject_default = [0.0, 0.45, 0.8, 0.0, 85.0, 2.0]
    arena_default = [0.0, 0.8, 0.16, 0.0, 0.0, 0.0, -1]

    def __init__(self, subject_default=subject_default, arena_default=arena_default):
        self.event_bus = EventBusClient(events=["shutdown"])
        self.event_bus.register("shutdown", self.on_shutdown)

        self.subject_actual = subject_default
        self.arena_actual = arena_default

        self.keep_alive = True
        self.last_loop_time = time.time()

        print("OK (%s): motive position controller" % str(os.getpid()))

        self.run()

    def run(self):
        timestamp = time.time()

        while self.keep_alive:
            self.event_bus.check_messages()  # listening on events
            self.broadcast_position()        # broadcast motive position info

            time_since_last = time.time() - timestamp
            if (1. / MotivePositionController.update_frequency) > time_since_last:
                time.sleep((1. / MotivePositionController.update_frequency) - time_since_last)
            timestamp = time.time()

            # uncomment to print the real update frequency
            # print(1. / (timestamp - self.last_loop_time))
            self.last_loop_time = timestamp

    def broadcast_position(self):
        actual = time.time()

        x, y, z, xr, yr, zr = self.subject_actual
        self.event_bus.send({"position_subject": (actual, x, y, z, xr, yr, zr)})

        x, y, z, xr, yr, zr, w = self.arena_actual
        self.event_bus.send({"position_arena": (actual, x, y, z, xr, yr, zr, w)})

    def on_shutdown(self, msg_data):
        print("SHUTDOWN: motive position controller")
        self.keep_alive = False


class LocalLoggingController(LoggingController):

    def run(self):
        self.event_bus.add_event("position_subject")
        self.event_bus.register("position_subject", self.log_position)
        self.event_bus.add_event("arena_move")
        self.event_bus.register("arena_move", self.log_arena_move)

        super(LocalLoggingController, self).run()

    def log_position(self, msg_data):
        timestamp, x, y, z, xr, yr, zr = msg_data
        self.writer.log("position", timestamp, x, y, z, xr, yr, zr)

    def log_arena_move(self, msg_data):
        self.writer.log("hotspot", time.time(), msg_data[0])


class RatcaveSceneController(object):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, *args, **kwargs):
        self.event_bus = EventBusClient(events=["position_subject", "position_arena", "shutdown"])
        self.event_bus.register("shutdown", self.on_shutdown)
        self.event_bus.register("position_subject", self.subject_pos_update)
        self.event_bus.register("position_arena", self.arena_pos_update)

        self.subject_position = [0, 0, 0]
        self.subject_rotation = [0, 0, 0]
        self.arena_body_position = [0, 0, 0]
        self.arena_body_quaternion = [0, 0, 0, 0]

        # Load projector's Camera object, created from the calib_projector ratcave_utils CLI tool.
        config = gl.Config(double_buffer=True, stereo=True)
        display = pyglet.window.get_platform().get_default_display()
        screen = display.get_screens()[screen_cfg['screen_index']]
        self.window = pyglet.window.Window(fullscreen=True, screen=screen, vsync=False, config=config)

        self.fbo = rc.FBO(rc.TextureCube(width=4096, height=4096))
        self.shader3d = rc.resources.cube_shader

        # setting up the projection scene (arena)
        arena = rc.WavefrontReader(scene_cfg['arena_filename'].encode()).get_mesh('Arena')
        arena.rotation = arena.rotation.to_quaternion()
        arena.textures.append(self.fbo.texture)
        self.arena = arena

        camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=40.974, aspect=1.777778, z_near=.005, z_far=20.))
        camera.position.xyz = screen_cfg['projector_position']
        camera.rotation.xyz = screen_cfg['projector_rotation']

        scene = rc.Scene(meshes=[self.arena], bgColor=(0., 0., 0.))
        scene.gl_states.states = scene.gl_states.states[:-1]
        scene.camera = camera
        scene.light.position.xyz = camera.position.xyz
        self.scene = scene

        # building VR scene
        vr_scene = scene_builder(scene_cfg)
        vr_scene.light.position.xyz = self.scene.camera.position.xyz
        vr_scene.light.position.y -= 2.
        self.vr_scene = vr_scene

        for msh in self.vr_scene.meshes.children:
            msh.original_position = msh.position.xyz

        # events
        self.window.event(self.on_close)
        self.window.event(self.on_draw)
        self.fps_label = pyglet.window.FPSDisplay(self.window)

        pyglet.clock.schedule(self.update)

        print("OK (%s): scene controller" % str(os.getpid()))

        self.run()

    def run(self):
        pyglet.app.run()

    # daemons

    def check_messages(self):
        self.event_bus.check_messages()

    def update(self, dt):
        self.check_messages()

        self.arena.position.xyz = self.arena_body_position
        self.arena.rotation.xyzw = self.arena_body_quaternion

        self.vr_scene.root.position.xyz = self.arena.position.xyz
        self.vr_scene.camera.position.xyz = self.subject_position
        self.scene.camera.uniforms['playerPos'] = self.subject_position

    def arena_pos_update(self, msg_data):
        self.arena_body_position = [msg_data[1], msg_data[2], msg_data[3]]
        self.arena_body_quaternion = [msg_data[4], msg_data[5], msg_data[6], msg_data[7]]

    def subject_pos_update(self, msg_data):
        self.subject_position = [msg_data[1], msg_data[2], msg_data[3]]
        self.subject_rotation = [msg_data[4], msg_data[5], msg_data[6]]

    # events

    def on_shutdown(self, msg_data):
        if hasattr(self, 'timers'):
            for t in self.timers:
                t.cancel()
                print('TIMER: %s cancelled' % str(t))

        pyglet.clock.schedule_once(self.do_safe_close, 0.1)

    def on_close(self):
        print("SHUTDOWN: scene controller")
        self.event_bus.send({"shutdown": []})
        self.on_shutdown("")

    def on_draw(self):
        with self.shader3d:
            with self.fbo:
                self.window.clear()
                self.vr_scene.camera.projection.match_aspect_to_viewport()
                self.vr_scene.draw360_to_texture(self.fbo.texture)

            self.scene.camera.projection.match_aspect_to_viewport()
            self.scene.draw()

    # actions

    def do_safe_close(self, dt):
        self.window.close()
