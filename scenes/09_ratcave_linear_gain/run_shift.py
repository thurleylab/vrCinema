import pyglet
from natnetclient import NatClient
import ratcave as rc
import pyglet.gl as gl


def vr_demo(scene_filename, arena_filename, body, screen):
    client = NatClient()
    arena_body = client.rigid_bodies['Arena']
    subject = client.rigid_bodies[body]

    # Load projector's Camera object, created from the calib_projector ratcave_utils CLI tool.
    config = gl.Config(double_buffer=True, stereo=True)
    display = pyglet.window.get_platform().get_default_display()
    screen = display.get_screens()[screen]
    window = pyglet.window.Window(fullscreen=True, screen=screen, vsync=False, config=config)

    fbo = rc.FBO(rc.TextureCube(width=4096, height=4096))
    shader3d = rc.resources.cube_shader

    # setting up the projection scene (arena)
    arena = rc.WavefrontReader(arena_filename.encode()).get_mesh('Arena')
    arena.rotation = arena.rotation.to_quaternion()
    arena.textures.append(fbo.texture)

    camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=40.974, aspect=1.777778, z_near=.005, z_far=20.))
    camera.position.xyz = [-0.677419,  2.6681666,  1.0313901]
    camera.rotation.xyz = [-90.01349,  -0.5733004,  90.91343]

    scene = rc.Scene(meshes=[arena], bgColor=(0., 1., 0.))
    scene.gl_states.states = scene.gl_states.states[:-1]
    scene.camera = camera
    scene.camera.uniforms['playerPos'] = subject.position
    scene.light.position.xyz = camera.position.xyz

    # setting up the VR scene
    reader = rc.WavefrontReader(scene_filename)
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}
    root = rc.EmptyEntity()
    for mesh in meshes.values():
        root.add_child(mesh)

    vr_scene = rc.Scene(meshes=root, bgColor=(0., 0., 0.))
    vr_scene.camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=90, aspect=1., z_near=.005, z_far=20.))
    vr_scene.camera.position.xyz = subject.position
    vr_scene.light.position.xyz = camera.position.xyz
    vr_scene.light.position.y -= 2.

    for msh in vr_scene.meshes:
        msh.position.x -= 0.33
        msh.position.z += 0.42

    start_position = vr_scene.camera.position.xyz
    for msh in vr_scene.meshes.children:
        msh.original_position = msh.position.xyz

    @window.event
    def on_draw():
        with shader3d:
            with fbo:
                window.clear()
                vr_scene.camera.projection.match_aspect_to_viewport()
                vr_scene.draw360_to_texture(fbo.texture)

            scene.camera.projection.match_aspect_to_viewport()
            scene.draw()

    def update_body(dt, body):
        arena.position.xyz = arena_body.position
        arena.rotation.xyzw = arena_body.quaternion

        vr_scene.camera.position.xyz = subject.position
        vr_scene.camera.position.y -= 0.25  # why is this?
        scene.camera.uniforms['playerPos'] = subject.position

    def update_meshes_positions_with_gain(dt, gain):
        delta_x = vr_scene.camera.position.x - arena.position.x
        delta_z = vr_scene.camera.position.z - arena.position.z

        for msh in vr_scene.meshes.children:
            msh.position.x = msh.original_position[0] - gain * delta_x
            msh.position.z = msh.original_position[2] - gain * delta_z

    def shift_meshes(x, y, z):
        for msh in vr_scene.meshes.children:
            msh.position.x += x
            msh.position.y += y
            msh.position.z += z

    shift_meshes(0, 0, 1)
    pyglet.clock.schedule(update_body, body)  # update arena / camera position
    #pyglet.clock.schedule(update_meshes_positions_with_gain, 4)  # update meshes due to gain

    pyglet.app.run()


if __name__ == '__main__':
    scene_filename = 'shift.obj'
    arena_filename = 'arena.obj'
    body = 'Gerbil'
    screen = 1

    vr_demo(scene_filename, arena_filename, body, screen)




