import ratcave as rc
import pyglet

from cinema.cinema import SphericalScene


def on_draw(dt):
    scene.draw_360()

reader = rc.WavefrontReader(rc.resources.obj_primitives)

sphere = reader.get_mesh('Sphere', position=(1, 0.5, -1), scale=.5)

scene = SphericalScene(meshes=[sphere], bgColor=(38. / 255., 78. / 255., 119. / 255.))

window = pyglet.window.Window(resizable=True, fullscreen=False)

# either like this
#window.event(on_draw)

# or like that
pyglet.clock.schedule_interval(on_draw, 1/30.0)

pyglet.app.run()
