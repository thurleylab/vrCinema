from PIL import Image
import numpy as np


color_A = (255, 255, 255)  # green?
color_B = (0, 0, 0)

# horizontal stripes pattern with <cycles> each cycle <2 * half_cycle_width> px

cycles = 20
half_cycle_width = 50


img = Image.new('RGB', (2 * cycles * half_cycle_width, 2 * cycles * half_cycle_width), color_A)
pixels = np.array(img)

for i in range(cycles):
    start_index = i * 2 * half_cycle_width
    pixels[start_index:start_index + half_cycle_width, :] = color_B

img2 = Image.fromarray(pixels)
img2.save('stripes.png')
