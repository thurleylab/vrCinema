from __future__ import absolute_import

import sys
import os
import json
import datetime, time
import numpy as np
import ratcave as rc

ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.append(ROOT)

from threading import Timer
from cinema.dispatch import DispatcherMini
from cinema.controllers.acquisition import AcquisitionSocketSyncController
from cinema.controllers.video import VideoController
from cinema.controllers.position import MotivePositionController, FakePositionController
from cinema.controllers.log import RatcaveLoggingController
from cinema.controllers.scene import RatcaveSceneController
from cinema.controllers.sound import DoubleSoundController
from cinema.controllers.serial import FeederController


def vr_scene_builder(scene_cfg):
    reader = rc.WavefrontReader('islands.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}
    root = rc.EmptyEntity()
    root.rotation = root.rotation.to_quaternion()

    for name, mesh in meshes.items():
        root.add_child(mesh)

    vr_scene = rc.Scene(meshes=root, bgColor=(0., 0., 0.))
    vr_scene.camera = rc.Camera(projection=rc.PerspectiveProjection(fov_y=90, aspect=1., z_near=.005, z_far=20.))
    vr_scene.root = root
    vr_scene.meshes_dict = meshes

    return vr_scene


class RatcaveIslandsController(RatcaveSceneController):

    def __init__(self, scene_builder, scene_cfg, screen_cfg, sound_cfg, *args, **kwargs):
        self.scene_cfg = scene_cfg
        self.timers = []

        # schedule experiment end
        t1 = Timer(scene_cfg['timers'][0], self.stop_experiment)

        self.timers = [t1]
        for timer in self.timers:
            timer.start()

        self.distance_since_sound = 0.0
        self.time_last_sound = time.time()
        self.timestamp = time.time()
        self.arena_y_comp = -0.055
        self.subject_last_position = (0, 0, 0)

        self.sound_cfg = sound_cfg

        self.click_delay = scene_cfg["click_delay"]             # time in sec between clicks when animal in reward zone
        self.reward_delay = scene_cfg["reward_delay"]           # time an animal waits in the reward zone to get reward
        self.refractory_delay = scene_cfg["refractory_delay"]   # time in sec to let an animal eat the reward
        self.reward_radius = scene_cfg["reward_radius"]         # reward zone radius from the beacon center

        self.sound_on = False
        self.in_hotspot_since = 0
        self.on_refractory_since = 0
        self.time_last_reward_sound = time.time()

        super(RatcaveIslandsController, self).__init__(scene_builder, scene_cfg, screen_cfg, *args, **kwargs)

    def run(self):
        self.horizontal_meshes = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('horizontal') > -1]
        self.vertical_meshes = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('vertical') > -1]
        self.distractor_meshes = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('45') > -1]

        # make flat shading for all objects
        for key, mesh in self.vr_scene.meshes_dict.items():
            mesh.uniforms['flat_shading'] = True
            mesh.uniforms['diffuse'] = 1., 1., 1.

        self.floor = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('floor') > -1][0]
        self.floor.width = 0.6
        self.floor.length = 1.6

        self.active_beacon = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('beacon') > -1][0]
        self.active_beacon.visible = self.scene_cfg['beacon_visible']
        self.distractor_beacon = [mesh for x, mesh in self.vr_scene.meshes_dict.items() if x.find('distractor') > -1][0]
        self.distractor_beacon.visible = self.scene_cfg['beacon_visible']

        self.make_vertical_visible()
        self.move_beacon()

        # FIXME compensating virtual coordinates to Arena orientation
        self.scene.camera.position.x += .02

        # FIXME error with OpenGL rendering - requires position data first
        time.sleep(2)
        super(RatcaveIslandsController, self).run()

    def update(self, dt):
        self.check_messages()

        # Arena virtual mesh matches physical arena in OptiTrack coordinates
        self.arena.position.xyz = np.array(self.arena_body_position) + np.array((0, -0.36, 0))  # virtual arena offset
        self.arena.rotation.xyzw = np.array(self.arena_body_quaternion)

        # transformation of VR world to the OptiTrack coordinate system
        rotmat = np.array([
            [ 1.08733909e-01,  1.62176600e-03,  9.94069568e-01],
            [-9.93952849e-01, -1.52329476e-02,  1.08745994e-01],
            [-1.53189702e-02,  9.99882657e-01,  4.43790033e-05]])
        trans = np.array([0.0173365, 0.383428 , 0.0152375])
        trans = np.array([0.0173365, 0 , 0.0152375])

        # ---------- translation ----------
        self.vr_scene.root.position.xyz = self.arena.position.xyz  # first move VR world to the "center" of the arena
        #self.vr_scene.root.position.xyz = (self.vr_scene.root.position.xyz - trans)
        self.vr_scene.root.position.xyz = (self.vr_scene.root.position.xyz + np.array((-0.005, 0, 0.09)))


        # ---------- rotation ------------
        rot_t = rc.RotationEulerRadians.from_matrix(rotmat).to_degrees()
        rot_o = self.vr_scene.root.rotation.to_euler(units='deg')

        rot_t.x += 90
        rot_t.y = 90 - rot_t.y

        print(rot_t.x, rot_t.y, rot_t.z, rot_o.x, rot_o.y, rot_o.z)
        #self.vr_scene.root.rotation = rot_t
        x1, y1, z1, w1, = self.arena.rotation.xyzw
        self.vr_scene.root.rotation.xyzw = (x1, y1 + self.arena_y_comp, z1, w1)


        #self.vr_scene.root.position.xyz = (self.vr_scene.root.position.xyz - trans) @ rotmat.T
        #self.vr_scene.root.position.xyz = self.vr_scene.root.position.xyz @ rotmat.T

        # new_zxy = (self.vr_scene.root.position.xyz - trans) @ rotmat.T
        # self.vr_scene.root.position.xyz = (new_zxy[1], new_zxy[2], new_zxy[0])

        #self.arena.position.xyz = np.array(self.arena_body_position) + np.array((-0.005, -0.36, 0.09))
        #self.arena.position.xyz = np.array(self.arena_body_position) - np.array((0.0173365, +0.36, 0.0152375))
        # self.arena.position.xyz = np.array(self.arena_body_position) - trans
        # self.arena.rotation.xyzw = np.array(self.arena_body_quaternion)
        # rot_m = self.arena.rotation.to_matrix()
        # rot_eu = self.arena.rotation.to_euler().xyz
        #
        # #self.arena.rotation.xyzw = np.array(self.arena_body_quaternion) #+ np.array((0, 0.012, 0, 0))
        #
        # rotated = np.dot(rotmat, rot_eu)
        #
        # new_zxy = self.arena.position.xyz @ rotmat.T
        # print(np.array(self.arena.position.xyz), new_zxy)
        #self.arena.position.xyz = (new_zxy[1], new_zxy[2], new_zxy[0])
        #self.arena.rotation.xyz = rotated
        #
        #
        # self.arena.rotation = rc.RotationEulerRadians.from_matrix(rotmat)
        # self.vr_scene.root.position.xyz = self.arena.position.xyz






        # bring subject position from OptiTrack coordinates to VR coordinates
        s_pos = self.subject_position

        phi = np.pi * 4.3 / 180.0

        x = s_pos[0] * np.cos(phi) - s_pos[2] * np.sin(phi)
        z = s_pos[0] * np.sin(phi) + s_pos[2] * np.cos(phi)
        y = s_pos[1]

        new_pos = [x, y, z]  # subject position in VR coordinates

        # assign the (possibly limited) subject position for the projection
        x1, y1, z1, = self.subject_position
        self.vr_scene.camera.position.xyz = [x1, y1, z1]
        self.scene.camera.uniforms['playerPos'] = [x1, y1, z1]
        self.vr_scene.light.position.xyz = (x1, self.vr_scene.light.position.xyz[1], z1)

        #x1, y1, z1, w1, = self.arena.rotation.xyzw
        #self.vr_scene.root.rotation.xyzw = (x1, y1 + self.arena_y_comp, z1, w1)
        #self.vr_scene.root.rotation.xyz = self.arena.rotation.xyz

        # check beacon
        x_b = self.active_beacon.position.x + self.vr_scene.root.position.x
        z_b = self.active_beacon.position.z + self.vr_scene.root.position.z
        b_distance = np.sqrt(np.square(new_pos[0] - x_b) + np.square(new_pos[2] - z_b))

        # check distractor
        if self.scene_cfg['distractor_enabled']:
            x_d = self.distractor_beacon.position.x + self.vr_scene.root.position.x
            z_d = self.distractor_beacon.position.z + self.vr_scene.root.position.z
            d_distance = np.sqrt(np.square(new_pos[0] - x_d) + np.square(new_pos[2] - z_d))
        else:
            d_distance = 10**3  # never enter the distractor zone

        if not self.on_refractory_since:  # entered the hotspot
            if b_distance < self.reward_radius:
                self.record_hotspot('beacon')
            elif d_distance < self.reward_radius:
                self.record_hotspot('distractor')
            elif self.in_hotspot_since:  # exit the hotspot
                self.in_hotspot_since = 0
                self.make_vertical_visible()

    # events

    def record_hotspot(self, spot_type):  # spot_type is 'beacon' or 'distractor'
        # activate a hotspot
        if not self.in_hotspot_since:
            self.in_hotspot_since = time.time()

            if spot_type == 'beacon':
                self.make_horizontal_visible()
            else:
                self.make_distractor_visible()

        # clicks while in a hotspot
        current = time.time()

        # deactivate reward clicks for now
        #if current - self.time_last_reward_sound > self.click_delay:
        if False:
            self.time_last_reward_sound = current

            # event to elicit rewarding clicks
            x, y = self.vr_scene.camera.position.x, self.vr_scene.camera.position.y
            self.event_bus.send({"hotspot": (str(time.time()), 'A', str(x), str(y))})

        # give reward and activate refractory # make a separate function
        if time.time() - self.in_hotspot_since > self.reward_delay and spot_type == 'beacon':
            x, y = self.vr_scene.camera.position.x, self.vr_scene.camera.position.y
            self.event_bus.send({"feed": (str(time.time()), 'A', str(x), str(y))})

            # refractory period
            self.on_refractory_since = time.time()
            self.active_beacon.visible = False
            self.distractor_beacon.visible = False

            Timer(self.refractory_delay, self.move_beacon).start()
            Timer(self.scene_cfg["reward_keep_for"], self.make_all_invisible).start()

    def make_horizontal_visible(self):
        for mesh in self.vertical_meshes + self.distractor_meshes:
            mesh.visible = False

        for mesh in self.horizontal_meshes:
            mesh.visible = True

    def make_vertical_visible(self):
        for mesh in self.horizontal_meshes + self.distractor_meshes:
            mesh.visible = False

        for mesh in self.vertical_meshes:
            mesh.visible = True

    def make_distractor_visible(self):
        for mesh in self.horizontal_meshes + self.vertical_meshes:
            mesh.visible = False

        for mesh in self.distractor_meshes:
            mesh.visible = True

    def make_all_invisible(self):
        for mesh in self.vertical_meshes + self.horizontal_meshes + self.distractor_meshes:
            mesh.visible = False

    def move_beacon(self):
        width = (self.floor.width - 2 * self.scene_cfg['reward_margin'])
        length = (self.floor.length - 2 * self.scene_cfg['reward_margin'])

        def get_random_pos():
            x = float(self.floor.position.x) + (np.random.rand() * width) - width / 2
            z = float(self.floor.position.z) + (np.random.rand() * length) - length / 2
            return x, z

        x_a, z_a = get_random_pos()
        self.active_beacon.position.x = x_a
        self.active_beacon.position.z = z_a

        x_d, z_d = 0, 0
        while True:
            x_d, z_d = get_random_pos()
            if np.sqrt( (x_d - x_a)**2 + (z_d - z_a)**2 ) > 2 * self.scene_cfg['reward_radius']:
                self.distractor_beacon.position.x = x_d
                self.distractor_beacon.position.z = z_d
                break

        self.active_beacon.visible = self.scene_cfg['beacon_visible']
        self.distractor_beacon.visible = self.scene_cfg['beacon_visible']

        self.event_bus.send({"hotspot": (time.time(), 'A', x_a, z_a)})
        self.event_bus.send({"hotspot": (time.time(), 'D', x_d, z_d)})

        self.on_refractory_since = 0

    def stop_experiment(self):
        self.on_close()


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        raise ValueError("Please provide settings file as a first argument")

    with open(path) as f:
        config = json.load(f)

    # create common directory for tracking / logs
    if config["logging"]["enabled"]:
        subject = config["vr"]["subjects"]["animal"]
        session = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        where = os.path.join(config['save_to'], subject, session)

        if not os.path.exists(where):
            os.makedirs(where)

        config["videotracking"]["record"]["where"] = where
        config["logging"]["where"] = where
        config['ephys']["rec_dir"] = os.path.join(config['ephys']["rec_dir"], subject)

    controllers = {
        "scene": (RatcaveIslandsController, (vr_scene_builder, config['vr']["scene"], config['vr']["screen"], config["sound"])),
        "motive": (MotivePositionController, (config['vr']["scene"],)),
    }

    cfg = config["logging"]
    if cfg["enabled"]:
        args = (cfg["where"], ROOT, "islands", path)
        controllers["log"] = (RatcaveLoggingController, args)

    cfg = config["ephys"]
    if cfg["connect"]:
        args = (cfg["hostname"], cfg["port"], cfg["frequency"], cfg["rec_dir"])
        controllers["ephys"] = (AcquisitionSocketSyncController, args)

    cfg = config["videotracking"]
    if cfg["enabled"]:
        controllers["videotracking"] = (VideoController, (cfg,))

    cfg = config["sound"]
    if cfg["enabled"]:
        controllers["sound"] = (DoubleSoundController, ())

    cfg = config["feeder"]
    if cfg["connect"]:
        args = (cfg["port"], cfg["baud"], cfg["verbose"])
        controllers["feeder"] = (FeederController, args)

    DispatcherMini.start_ratcave(controllers, config['event_bus'])
