from __future__ import absolute_import

import sys
import os

import cinema
from cinema.dispatch import DispatcherMini

from cinema.controllers.sensor import SensorController
from cinema.controllers.log import LoggingController
from cinema.controllers.position import PositionController
from cinema.controllers.scene import SceneController
from cinema.controllers.serial import SerialController
from cinema.controllers.video import VideoController
from cinema.controllers.acquisition import AcquisitionSyncController
from cinema.controllers.trajectory import TrajectoryWindowController
from cinema.controllers.sound import SoundController

_ROOT = os.path.abspath(os.path.dirname(__file__))


def scene_builder(config):
    import ratcave as rc
    from cinema.cinema import SphericalScene

    reader = rc.WavefrontReader('linear.obj')
    meshes = {name: reader.get_mesh(name) for name in reader.bodies.keys()}

    root = rc.EmptyEntity()
    for mesh in meshes.values():
        root.add_child(mesh)

    scene = SphericalScene(meshes=root, bgColor=(1. / 255., 1. / 255., 1. / 255.))

    # starting scene position
    scene.meshes.rotation.xyz = (0., 0., config['yaw'])
    scene.camera.position.xyz = (config['x0'], config['y0'], 0.1)
    scene.camera.rotation.xyz = (90., 0., 90.)
    scene.camera.projection.z_far = 10.0

    scene.hotspots = []

    return scene


def is_inside(x0, y0):
    bdr = 0.05
    return -4.0 + bdr < x0 < 4.0 - bdr and -0.5 + bdr < y0 < 0.5 - bdr


if __name__ == '__main__':
    try:
        path = sys.argv[1]
    except IndexError:
        print("WARNING! Using default settings..")
        path = os.path.join(os.path.dirname(cinema.__file__), "default.json")

    controllers = {
        "sensor": SensorController,
        "log": LoggingController,
        "position": PositionController,
        "scene": SceneController,
        "serial": SerialController,
        "video": VideoController,
        "acquisition": AcquisitionSyncController,
        "trajectory": TrajectoryWindowController,
        "sound": SoundController
    }

    settings = DispatcherMini.read_settings(path)
    DispatcherMini.start(scene_builder, is_inside, controllers, _ROOT, settings)

