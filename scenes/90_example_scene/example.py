import ratcave as rc
import pyglet
import os

from pyglet.window import key
from cinema.cinema import SphericalScene

path = os.path.abspath(os.path.dirname(__file__))
window = pyglet.window.Window(resizable=True, fullscreen=False)

reader = rc.WavefrontReader(rc.resources.obj_primitives)

# plane
plane = reader.get_mesh('Plane', position=(0.5, 1.2, 0.3), scale=0.6)
plane.texture = rc.Texture.from_image(os.path.join(path, "stripes.png"))

# floor
stage = reader.get_mesh('Plane', position=(0, 0., 0.), rotation=(0., 0., 0.), scale=2.)
stage.texture = rc.Texture.from_image(rc.resources.img_colorgrid)


root = rc.EmptyEntity()
for mesh in [plane, stage]:
    root.add_child(mesh)

scene = SphericalScene(meshes=root, bgColor=(1. / 255., 1. / 255., 1. / 255.))
scene.camera.position.xyz = (0., 0., 0.2)
scene.camera.rotation.xyz = (90., 0., 90.)
scene.camera.projection.z_far = 10.0


@window.event
def on_draw():
    scene.draw_360()


tt = 0.
def update(dt):
    global tt
    tt += dt
    plane.rotation.y += 50. * dt


# movements in the maze
actions = {
    key.UP: lambda camera, step: setattr(camera.position, 'y', camera.position.y + step),
    key.DOWN: lambda camera, step: setattr(camera.position, 'y', camera.position.y - step),
    key.LEFT: lambda camera, step: setattr(camera.position, 'x', camera.position.x + step),
    key.RIGHT: lambda camera, step: setattr(camera.position, 'x', camera.position.x - step)
}


@window.event
def on_key_press(symbol, modifiers):
    # movements in space
    if symbol in actions.keys():
        actions[symbol](scene.camera, .2)

    # add object on the go
    if symbol == key.SPACE:
        sphere2 = reader.get_mesh('Sphere', position=(-1., 0.5, -2), scale=.5)
        scene.root.add_children([sphere2])


pyglet.clock.schedule_interval(update, 1./30)
pyglet.app.run()
