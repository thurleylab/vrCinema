from setuptools import setup, find_packages
import os


__author__ = 'asobolev'


setup(
    name='cinema',
    version='0.1',
    description='Tools to render different scenes in the ball (body-fixed) Virtual Reality',
    author='Andrey Sobolev',
    author_email='sobolev@bio.lmu.de',
    packages=find_packages(),
    include_package_data=True,
    package_data={'': [os.path.join('shaders', '*' + ext) for ext in ['.vert', '.frag']] +
                      [os.path.join('blender', '*')] +
                      [os.path.join('sounds', '*')] +
                      ['devices.json', 'default.json']},

    # ratcave/pyglet    - VR rendering
    # numpy             - math
    # pyzmq             - network events to Open Ephys
    # pyusb             - communication with mouse sensors
    # pyserial          - serial communication with arduino's

    install_requires=['ratcave', 'pyglet', 'numpy', 'pyzmq', 'pyusb', 'pyserial']
)
